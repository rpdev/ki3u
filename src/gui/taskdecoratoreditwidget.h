/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKDECORATOREDITWIDGET_H
#define TASKDECORATOREDITWIDGET_H

#include "../models/tasksmodel.h"

#include <QAbstractItemView>
#include <QWidget>

class QAbstractButton;

namespace ki3u
{

namespace gui
{

class TaskDecoratorEditWidget : public QWidget
{
  
  Q_OBJECT
  
public:
  
  explicit TaskDecoratorEditWidget( models::TasksModel *model, 
                                    QAbstractItemView *view,
                                    QWidget* parent = 0, 
                                    Qt::WindowFlags f = 0);
  virtual ~TaskDecoratorEditWidget();
  
private:
  
  class TaskDecoratorEditWidgetPrivate;
  TaskDecoratorEditWidgetPrivate *d;
  
private slots:
  
  void itemActivated( const QModelIndex &index );
  
  void handleButtonClick( QAbstractButton *button );
  
  void assignTask();
  void unassignTask();
  
};

}

}

#endif // TASKDECORATOREDITWIDGET_H
