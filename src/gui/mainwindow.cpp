/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "mainwindow.h"

#include "taskdecoratoreditwidget.h"
#include "taskfactoryselectiondialog.h"

#include "../models/factoriesmodel.h"
#include "../models/tasklogmodel.h"
#include "../models/tasksmodel.h"

#include "../views/progressitemdelegate.h"

#include "../dbus/secretsstoreadapter.h"

#include <KAction>
#include <KActionCollection>
#include <KApplication>
#include <KDebug>
#include <KLocale>
#include <KMenu>
#include <KMenuBar>
#include <KMessageBox>
#include <KStatusNotifierItem>

#include <QDBusConnection>
#include <QDockWidget>
#include <QHeaderView>
#include <QTreeView>

namespace ki3u
{
  
namespace gui
{

/**
  * @private
  */ 
class MainWindow::MainWindowPrivate
{
  
public:
  
  static const QString TaskContainerConfigGroup;
  static const QString MainWindowGroup;
  
  static const QString MainWindowGeometry;
  static const QString MainWindowState;
  static const QString MainWindowVisible;
  static const QString TaskViewHeaderState;
  
  static const QString EditTaskDockWidgetName;
  static const QString EditTaskDockWidgetToggleViewActionName;
  
  static const QString TaskLogDockWidgetName;
  static const QString TaskLogDockWidgetToggleViewActionName;
  
  static const QString DBusSecretsStorePath;
  
  core::TaskContainer *taskContainer;
  
  models::FactoriesModel *factoriesModel;
  models::TasksModel *tasksModel;
  models::TaskLogModel *taskLogModel;
  
  views::ProgressItemDelegate *progressBarItemDelegate;
  
  KAction *newTaskAction;
  KAction *copyTaskAction;
  KAction *configureTaskAction;
  KAction *deleteTaskAction;
  KAction *runTaskAction;
  
  KStatusNotifierItem *statusNotifierItem;
  
  QTreeView *taskView;
  
  TaskDecoratorEditWidget *taskEditWidget;
  QDockWidget *taskEditDockWidget;
  
  QTreeView *taskLogView;
  QDockWidget *taskLogDockWidget;
  
  QList< core::TaskDecorator* > startedTasks;
  
  MainWindowPrivate( MainWindow *mainWindow ) :
    taskContainer( 0 ),
    factoriesModel( 0 ),
    tasksModel( 0 ),
    taskLogModel( 0 ),
    progressBarItemDelegate( 0 ),
    newTaskAction( 0 ),
    copyTaskAction( 0 ),
    configureTaskAction( 0 ),
    deleteTaskAction( 0 ),
    runTaskAction( 0 ),
    statusNotifierItem( 0 ),
    taskView( 0 ),
    taskEditWidget( 0 ),
    taskEditDockWidget( 0 ),
    taskLogView( 0 ),
    taskLogDockWidget( 0 ),
    startedTasks( QList< core::TaskDecorator* >() ),
    q( mainWindow )
  {
  }
  
  virtual ~MainWindowPrivate()
  {
  }
  
  void 
  initialize()
  {
    setupTaskContainer();
    setupModels();
    setupViewItemDelegates();
    setupActions();
    setupStatusNotifierItem();
    setupTaskView();
    setupTaskEditWidget();
    setupTaskLogWidget();
    setupSignals();
    setupDBusAdapters();
  }
  
  void 
  saveConfig()
  {
    KConfig config;
    KConfigGroup taskContainerGroup = config.group( TaskContainerConfigGroup );
    taskContainer->save( taskContainerGroup );
    
    KConfigGroup mainWindowGeometryGroup = config.group( MainWindowGroup );
    mainWindowGeometryGroup.writeEntry( MainWindowGeometry, q->saveGeometry() );
    mainWindowGeometryGroup.writeEntry( MainWindowState, q->saveState() );
    mainWindowGeometryGroup.writeEntry( MainWindowVisible, q->isVisible() );
    mainWindowGeometryGroup.writeEntry( TaskViewHeaderState, taskView->header()->saveState() );
  }
  
  void 
  readConfig()
  {
    KConfig config;
    KConfigGroup taskContainerGroup = config.group( TaskContainerConfigGroup );
    taskContainer->restore( taskContainerGroup );
    
    KConfigGroup mainWindowGeometryGroup = config.group( MainWindowGroup );
    q->restoreGeometry( mainWindowGeometryGroup.readEntry< QByteArray >( MainWindowGeometry, QByteArray() ) );
    q->restoreState( mainWindowGeometryGroup.readEntry< QByteArray >( MainWindowState, QByteArray() ) );
    q->setVisible( mainWindowGeometryGroup.readEntry< bool >( MainWindowVisible, true ) );
    taskView->header()->restoreState( mainWindowGeometryGroup.readEntry< QByteArray >( TaskViewHeaderState, QByteArray() ) );
  }
  
private:
  
  MainWindow *q;
  
  void 
  setupTaskContainer()
  {
    taskContainer = new core::TaskContainer( q );
  }
  
  void 
  setupModels()
  {
    factoriesModel = new models::FactoriesModel( taskContainer, q );
    tasksModel = new models::TasksModel( taskContainer, q );
    taskLogModel = new models::TaskLogModel( q );
  }
  
  void
  setupViewItemDelegates()
  {
    progressBarItemDelegate = new views::ProgressItemDelegate( q );
  }
  
  void 
  setupActions()
  {
    newTaskAction = new KAction( q );
    newTaskAction->setText( i18n( "New" ) );
    newTaskAction->setIcon( KIcon( "document-new" ) );
    newTaskAction->setShortcut( QKeySequence::New );
    q->actionCollection()->addAction( NewTaskActionName, newTaskAction );
    
    copyTaskAction = new KAction( q );
    copyTaskAction->setText( i18n( "Duplicate" ) );
    copyTaskAction->setIcon( KIcon ("edit-copy") );
    copyTaskAction->setShortcut( QKeySequence::Copy );
    q->actionCollection()->addAction( CopyTaskActionName, copyTaskAction );
    
    configureTaskAction = new KAction( q );
    configureTaskAction->setText( i18n( "Configure..." ) );
    configureTaskAction->setIcon( KIcon( "configure" ) );
    q->actionCollection()->addAction( ConfigureTaskActionName, configureTaskAction );
    
    deleteTaskAction = new KAction( q );
    deleteTaskAction->setText( i18n( "Delete" ) );
    deleteTaskAction->setIcon( KIcon( "trash-emtpy" ) );
    q->actionCollection()->addAction( DeleteTaskActionName, deleteTaskAction );
    
    runTaskAction = new KAction( q );
    runTaskAction->setText( i18n( "Run" ) );
    runTaskAction->setIcon( KIcon( "arrow-right" ) );
    q->actionCollection()->addAction( RunTaskActionName, runTaskAction );
    
    q->actionCollection()->addAction( KStandardAction::Quit, 
                                     KApplication::kApplication(), 
                                     SLOT(quit()) );
  }
  
  void 
  setupStatusNotifierItem()
  {
    statusNotifierItem = new KStatusNotifierItem( q );
    statusNotifierItem->setAssociatedWidget( q );
    statusNotifierItem->setCategory( KStatusNotifierItem::ApplicationStatus );
    statusNotifierItem->setIconByName( "ki3u" );
    
    // TODO: Maybe should be set depending on task statuses
    statusNotifierItem->setStatus( KStatusNotifierItem::Active );
  }
  
  void 
  setupTaskView()
  {
    taskView = new QTreeView( q );
    taskView->setModel( tasksModel );
    taskView->setItemDelegateForColumn( models::TasksModel::StatusColumn, 
                                        progressBarItemDelegate );
    q->setCentralWidget( taskView );
  }
  
  void 
  setupTaskEditWidget()
  {
    taskEditDockWidget = new QDockWidget( i18n( "Task Information" ), q );
    taskEditWidget = new TaskDecoratorEditWidget( tasksModel, taskView, 
                                                  taskEditDockWidget );
    taskEditDockWidget->setObjectName( EditTaskDockWidgetName );
    taskEditDockWidget->setWidget( taskEditWidget );
    q->addDockWidget( Qt::RightDockWidgetArea, taskEditDockWidget );
    q->actionCollection()->addAction( EditTaskDockWidgetToggleViewActionName, 
                                      taskEditDockWidget->toggleViewAction() );
  }
  
  void 
  setupTaskLogWidget()
  {
    taskLogDockWidget = new QDockWidget( i18n( "Log" ), q );
    taskLogView = new QTreeView( taskLogDockWidget );
    taskLogView->setModel( taskLogModel );
    taskLogDockWidget->setObjectName( TaskLogDockWidgetName );
    taskLogDockWidget->setWidget( taskLogView );
    q->addDockWidget( Qt::BottomDockWidgetArea, taskLogDockWidget );
    q->actionCollection()->addAction( TaskLogDockWidgetToggleViewActionName, 
                                      taskLogDockWidget->toggleViewAction() );
  }
  
  void 
  setupSignals()
  {
    QObject::connect( newTaskAction, SIGNAL(triggered()),
                      q, SLOT(createNewTask()) );
    QObject::connect( copyTaskAction, SIGNAL(triggered()), 
                      q, SLOT(copyTask()) );
    QObject::connect( configureTaskAction, SIGNAL(triggered()),
                      q, SLOT(configureSelectedTask()) );
    QObject::connect( deleteTaskAction, SIGNAL(triggered()),
                      q, SLOT(deleteSelectedTask()) );
    QObject::connect( runTaskAction, SIGNAL(triggered()),
                      q, SLOT(runTask()) );
    QObject::connect( taskView, SIGNAL(activated(QModelIndex)), 
                      q, SLOT(taskSelected(QModelIndex)) );
    QObject::connect( q, SIGNAL(taskActivated(core::TaskDecorator*)), 
                      taskLogModel, SLOT(setTask(core::TaskDecorator*)) );
    QObject::connect( taskContainer->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)), 
                      q, SLOT(taskFinishedRunning()) );
  }
  
  void
  setupDBusAdapters()
  {
    new dbus::SecretsStoreAdapter( taskContainer->secretsStore() );
    QDBusConnection::sessionBus().registerObject( MainWindow::DBusSecretsStorePath, 
                                                  taskContainer->secretsStore() );
  }
  
};

const QString MainWindow::MainWindowPrivate::TaskContainerConfigGroup = 
  "TaskContainer";
const QString MainWindow::MainWindowPrivate::MainWindowGroup = 
  "MainWindow";
  
const QString MainWindow::MainWindowPrivate::MainWindowGeometry = 
  "geometry";
const QString MainWindow::MainWindowPrivate::MainWindowState = 
  "state";
const QString MainWindow::MainWindowPrivate::MainWindowVisible =
  "visible";
const QString MainWindow::MainWindowPrivate::TaskViewHeaderState =
  "taskViewHeaderState";
  
const QString MainWindow::MainWindowPrivate::EditTaskDockWidgetName = 
  "editTaskDockWidget";
const QString MainWindow::MainWindowPrivate::EditTaskDockWidgetToggleViewActionName = 
  "toggleEditTaskDockWidgetVisibility";
  
const QString MainWindow::MainWindowPrivate::TaskLogDockWidgetName = 
  "taskLogDockWidget";
const QString MainWindow::MainWindowPrivate::TaskLogDockWidgetToggleViewActionName = 
  "toggleTaskLogDockWidgetVisibility";


/**
 * @class MainWindow
 * @brief Main window class for the KI3U application
 * 
 * The MainWindow class is the central widget for the KI3U class.
 * 
 * @ingroup ki3u_gui
 */

const QString MainWindow::NewTaskActionName = "newTask";
const QString MainWindow::CopyTaskActionName = "copyTask";
const QString MainWindow::ConfigureTaskActionName = "configureTask";
const QString MainWindow::DeleteTaskActionName = "deleteTask";
const QString MainWindow::RunTaskActionName = "runTask";

const QString MainWindow::DBusSecretsStorePath = "/SecretsStore";

/**
 * @brief Constructor
 * 
 * @param parent Parent object for the main window.
 * @param f Window flags to use.
 */
MainWindow::MainWindow(QWidget* parent, Qt::WindowFlags f) : 
  KXmlGuiWindow(parent, f),
  d( new MainWindowPrivate( this ) )
{
  d->initialize();
  setupGUI();
  d->readConfig();
  
  // This is somehow hacky... but changing the name in the XML failed,
  // and using a custom menu, the quit action will still stick at the file
  // menu...
  KMenu* fileMenu = this->findChild< KMenu* >( "file" );
  if ( fileMenu )
  {
    fileMenu->setTitle( i18n( "Task" ) );
  }
}

/**
 * @brief Destructor
 */
MainWindow::~MainWindow()
{
  delete d;
}

/**
 * @brief Returns the task container used by the main window.
 */
core::TaskContainer* 
MainWindow::taskContainer() const
{
  return d->taskContainer;
}

/**
 * @brief The task view component
 * 
 * Returns the main view for tasks.
 */
QTreeView* 
MainWindow::taskView() const
{
  return d->taskView;
}

bool MainWindow::queryClose()
{
  d->saveConfig();
  KMainWindow::queryClose();
  // Only hide the window and return
  // We'll keep running, which is indicated by the status notifier in the systray
  hide();
  return false;
}

bool MainWindow::queryExit()
{
  d->saveConfig();
  return KMainWindow::queryExit();
}

/**
 * @brief Creates a new task.
 * 
 * Shows a dialog to the user, where a task type can be selected and
 * - if a valid type is chosen - creates a new task of this type.
 */
void 
MainWindow::createNewTask()
{
  TaskFactorySelectionDialog dialog( d->taskContainer, this );
  if ( dialog.exec() && !( dialog.factoryId().isEmpty() ) )
  {
    core::TaskDecorator *task = 
      d->taskContainer->createTask( dialog.factoryId() );
    task->setName( i18n( "New Task" ) );
  }
}

/**
 * @brief Copy the current task
 * 
 * Creates a copy of the selected task (i.e. it is cloned) and inserted into
 * the task container.
 */
void 
MainWindow::copyTask()
{
  d->tasksModel->createTaskFrom( d->taskView->currentIndex() );
}

/**
 * @brief Deletes the currently selected task
 * 
 * Deletes the currently selected task from the container.
 */
void 
MainWindow::deleteSelectedTask()
{
  core::TaskDecorator *task = 
    d->tasksModel->task( d->taskView->currentIndex() );
  if ( task )
  {
    if ( KMessageBox::questionYesNo( 
          this,
          i18n( "Do you want to delete the task %1?" ).arg( task->name() ),
          i18n( "Delete Task" ) ) 
        == KMessageBox::Yes )
    {
      emit taskActivated( 0 );
      d->tasksModel->deleteTask( task );
    }
  }
}

/**
 * @brief Configure the selected task
 * 
 * Uses the selected task's Task::configure() method to let the
 * task configure itself.
 */
void 
MainWindow::configureSelectedTask()
{
  core::TaskDecorator *task = 
    d->tasksModel->task( d->taskView->currentIndex() );
  if ( task )
  {
    task->configure();
  }
}

/**
 * @brief Runs the currently selected task
 * 
 * Starts executing the currently selected task if it is not already running.
 */
void 
MainWindow::runTask()
{
  core::TaskDecorator *task = d->tasksModel->task( d->taskView->currentIndex() );
  if ( task )
  {
    if ( task->lock() )
    {
      if ( !( d->taskContainer->runner()->run( task ) ) )
      {
        task->unlock();
        KMessageBox::information( this, 
                                  i18n( "Failed to run task..." ), 
                                  d->taskContainer->runner()->lastError() + 
                                  "\n\n" + 
                                  d->taskContainer->runner()->lastErrorLog().join(
                                    "\n" ) );
      } else
      {
        d->startedTasks.append( task );
      }
    } else
    {
      KMessageBox::information( this, 
                                i18n( "The task %1 has already been started or "
                                      "will be started as a child task of "
                                      "another task." ).arg( task->name() ) );
    }
  }
}

/**
 * @brief Handle activation of tasks and propagate the selected task
 * 
 * This will emit the selected task as core::TaskDecorator (instead of 
 * a QModelIndex).
 */
void 
MainWindow::taskSelected(const QModelIndex& index)
{
  emit taskActivated( d->tasksModel->task( index ) );
}

void 
MainWindow::taskFinishedRunning()
{
  /* TODO
   * Note that what we do here is a bit hacky:
   * Qt's moc seems to have problems with namespaces:
   * The runner has the signal internally without the core namespace,
   * the main window (of course) uses core::TaskDecorator. Thus, on
   * startup we get complaints that the argument lists are incompatible.
   * 
   * As for now, we simply step through the list if tasks and unlock those 
   * which finished (state == Idle). This basically is the same (for now)
   * but should be replaced by a better way.
   */
  foreach ( core::TaskDecorator *task, d->startedTasks )
  {
    if ( task->state() == core::TaskDecorator::Idle )
    {
      task->unlock();
      QString icon( "emblem-important" );
      core::TaskFactoryDecorator *factory = 
        d->taskContainer->factory()->taskFactory( task->factoryId() );
      if ( factory )
      {
        icon = factory->icon().name();
      }
      d->statusNotifierItem->showMessage( 
        i18n( "Task finished" ), 
        i18n( "The task %1 has finished" ).arg( task->name() ), 
        icon );
    }
  }
}


}

}