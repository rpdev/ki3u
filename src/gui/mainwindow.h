/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ki3u_gui_config.h"

#include "../core/taskcontainer.h"

#include <KXmlGuiWindow>

#include <QTreeView>

namespace ki3u
{

namespace gui
{
  
class KI3U_GUI_EXPORT MainWindow : public KXmlGuiWindow
{
  
  Q_OBJECT
  
public:
  
  static const QString NewTaskActionName;
  static const QString CopyTaskActionName;
  static const QString ConfigureTaskActionName;
  static const QString DeleteTaskActionName;
  static const QString RunTaskActionName;
  
  static const QString DBusSecretsStorePath;
  
  MainWindow( QWidget *parent = 0, 
              Qt::WindowFlags f = KDE_DEFAULT_WINDOWFLAGS );
  virtual ~MainWindow();
  
  core::TaskContainer* taskContainer() const;
  
  QTreeView* taskView() const;
  
signals:
  
  void taskActivated( core::TaskDecorator *task );
  
protected:
  
  // Refined from KXmlGuiWindow
  virtual bool queryClose();
  virtual bool queryExit();
  
private:
  
  class MainWindowPrivate;
  MainWindowPrivate *d;
  
private slots:
  
  void createNewTask();
  void copyTask();
  void deleteSelectedTask();
  void configureSelectedTask();
  void runTask();
  
  void taskSelected( const QModelIndex& index );
  
  void taskFinishedRunning();
  
};

}

}

#endif // MAINWINDOW_H
