/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKFACTORYSELECTIONDIALOG_H
#define TASKFACTORYSELECTIONDIALOG_H

#include "../core/taskcontainer.h"
#include "../core/taskfactorydecorator.h"

#include <KDialog>

namespace ki3u
{

namespace gui
{
  
class TaskFactorySelectionDialog : public KDialog
{
  
  Q_OBJECT
  
public:
  
    explicit TaskFactorySelectionDialog( core::TaskContainer *container, 
                                         QWidget* parent = 0, 
                                         Qt::WFlags flags = 0);
    virtual ~TaskFactorySelectionDialog();
    
    core::TaskFactoryDecorator* factory() const;
    QString factoryId() const;
  
private:
  
  class TaskFactorySelectionDialogPrivate;
  TaskFactorySelectionDialogPrivate *d;
  
};

}

}

#endif // TASKFACTORYSELECTIONDIALOG_H
