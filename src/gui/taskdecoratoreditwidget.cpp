/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskdecoratoreditwidget.h"

#include "ui_taskdecoratoreditwidget.h"

namespace ki3u
{

namespace gui
{

/**
  * @private
  */
class TaskDecoratorEditWidget::TaskDecoratorEditWidgetPrivate
{
  
public:
  
  models::TasksModel    *model;
  QAbstractItemView     *view;
  
  Ui::TaskDecoratorEditWidget ui;
  
  
  TaskDecoratorEditWidgetPrivate( models::TasksModel *model, 
                                  QAbstractItemView *view,
                                  TaskDecoratorEditWidget *widget ) :
    model( model ),
    view( view ),
    ui(),
    q( widget )
  {
  }
    
  virtual ~TaskDecoratorEditWidgetPrivate()
  {
  }
  
  void
  initialize()
  {
    setupGui();
    setupSignals();
  }
  
  void
  updateToIndex( const QModelIndex &index )
  {
    core::TaskDecorator *task = model->task( index );
    if ( task ) 
    {
      ui.name->setText( task->name() );
      ui.comment->setPlainText( task->comment() );
      ui.assignedTasks->clear();
      ui.availableTasks->clear();
      ui.executeInParallel->setChecked( task->executeChildrenInParallel() );
      ui.ignoreErrors->setChecked( task->ignoreChildErrors() );
      foreach ( QString id, task->childTasks() )
      {
        core::TaskDecorator *childTask = model->task( id );
        if ( childTask )
        {
          QListWidgetItem *item = new QListWidgetItem( 
            childTask->name().isEmpty() ? childTask->id() : childTask->name(), 
            ui.assignedTasks );
          item->setData( Qt::UserRole, id );
        }
      }
      for ( int i = 0; i < model->rowCount(); i++ )
      {
        core::TaskDecorator *availableTask = model->task( model->index( i, 0 ) );
        if ( availableTask && 
             !( task->childTasks().contains( availableTask->id() ) ) &&
           ( availableTask != task ) )
        {
          QListWidgetItem *item = new QListWidgetItem( 
            availableTask->name().isEmpty() ? availableTask->id() : availableTask->name(), 
            ui.availableTasks );
          item->setData( Qt::UserRole, availableTask->id() );
        }
      }
    } else
    {
      ui.name->clear();
      ui.comment->clear();
      ui.assignedTasks->clear();
      ui.availableTasks->clear();
      ui.executeInParallel->setChecked( false );
      ui.ignoreErrors->setChecked( false );
    }
  }
  
  void
  applySettings()
  {
    core::TaskDecorator *task = model->task( view->currentIndex() );
    if ( task )
    {
      task->setName( ui.name->text() );
      task->setComment( ui.comment->toPlainText() );
      task->setExecuteChildrenInParallel( ui.executeInParallel->isChecked() );
      task->setIgnoreChildErrors( ui.ignoreErrors->isChecked() );
      
      QStringList childTasks;
      for ( int i = 0; i < ui.assignedTasks->count(); i++ )
      {
        QListWidgetItem *item = ui.assignedTasks->item( i );
        childTasks << item->data( Qt::UserRole ).toString();
      }
      task->setChildTasks( childTasks );
    }
  }
  
private:
  
  TaskDecoratorEditWidget *q;
  
  
  void 
  setupGui()
  {
    ui.setupUi( q );
  }
  
  void
  setupSignals()
  {
    QObject::connect( view, SIGNAL(activated(QModelIndex)),
                      q, SLOT(itemActivated(QModelIndex)) );
    QObject::connect( ui.buttons, SIGNAL(clicked(QAbstractButton*)),
                      q, SLOT(handleButtonClick(QAbstractButton*)) );
    QObject::connect( ui.assignButton, SIGNAL(clicked()), 
                      q, SLOT(assignTask()) );
    QObject::connect( ui.unassignButton, SIGNAL(clicked()), 
                      q, SLOT(unassignTask()) );
  }
  
  
};

/**
 * @class TaskDecoratorEditWidget
 * @brief Widget for editing meta information of a TaskDecorator
 * 
 * The TaskDecoratorEditWidget is a composite widget, that allows
 * editing the meta information of a TaskDecorator object. The widget
 * integrates with Qt's Interview classes:
 * On creation, the widget is passed a TasksModel and a view.
 * The widget will track updates in both and change the displayed information
 * accordingly.
 */

/**
 * @brief Constructor
 * 
 * @param model The model to use for retrieving task objects
 * @param view The view to track (e.g. for active selection)
 * @param parent A parent widget for this object
 * @param f Window flags to use
 */
TaskDecoratorEditWidget::TaskDecoratorEditWidget( models::TasksModel* model, 
                                                  QAbstractItemView *view,
                                                  QWidget* parent, 
                                                  Qt::WindowFlags f ) : 
  QWidget( parent, f),
  d( new TaskDecoratorEditWidgetPrivate( model, view, this ) )
{
  d->initialize();
}

/**
 * @brief Destructor
 */
TaskDecoratorEditWidget::~TaskDecoratorEditWidget()
{
  delete d;
}

/**
 * @brief Update to new selected item
 * 
 * Updates the widget to show the information that belong to the task
 * which is associated with the given @p index.
 */
void 
TaskDecoratorEditWidget::itemActivated( const QModelIndex& index )
{
  d->updateToIndex( index );
}

/**
 * @brief Handle button clicks in the widget
 */
void 
TaskDecoratorEditWidget::handleButtonClick( QAbstractButton* button )
{
  switch ( d->ui.buttons->buttonRole( button ) )
  {
    case QDialogButtonBox::ApplyRole:   
      d->applySettings(); break;
      
    case QDialogButtonBox::ResetRole: 
      d->updateToIndex( d->view->currentIndex() ); break;
      
    default: break;
  }
}

void 
TaskDecoratorEditWidget::assignTask()
{
  QListWidgetItem *item = d->ui.availableTasks->currentItem();
  if ( item )
  {
    d->ui.availableTasks->takeItem( d->ui.availableTasks->currentRow() );
    d->ui.assignedTasks->addItem( item );
  }
}

void 
TaskDecoratorEditWidget::unassignTask()
{
  QListWidgetItem *item = d->ui.assignedTasks->currentItem();
  if ( item )
  {
    d->ui.assignedTasks->takeItem( d->ui.assignedTasks->currentRow() );
    d->ui.availableTasks->addItem( item );
  }
}


}

}