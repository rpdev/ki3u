/*
   ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskfactoryselectiondialog.h"

#include "../models/factoriesmodel.h"

#include <QTreeView>

namespace ki3u
{

namespace gui
{

/**
 * @private
 */
class TaskFactorySelectionDialog::TaskFactorySelectionDialogPrivate
{
  
public:
  
  core::TaskContainer *container;
  models::FactoriesModel *model;
  
  QTreeView *view;
  
  TaskFactorySelectionDialogPrivate( core::TaskContainer *c,
                                     TaskFactorySelectionDialog *dialog ) :
    container( c ),
    model( 0 ),
    q( dialog )
  {
  }
  
  virtual ~TaskFactorySelectionDialogPrivate()
  {
  }
  
  void
  initialize()
  {
    initializeModel();
    initializeView();
  }
  
private:
  
  TaskFactorySelectionDialog *q;
  
  void
  initializeModel()
  {
    model = new models::FactoriesModel( container, q );
  }
  
  void
  initializeView()
  {
    view = new QTreeView( q );
    view->setModel( model );
    q->setMainWidget( view );
  }
  
};

TaskFactorySelectionDialog::TaskFactorySelectionDialog( 
  core::TaskContainer* container, 
  QWidget* parent, 
  Qt::WFlags flags ): 
  KDialog( parent, flags ),
  d( new TaskFactorySelectionDialogPrivate( container, this ) )
{
  d->initialize();
}

TaskFactorySelectionDialog::~TaskFactorySelectionDialog()
{
  delete d;
}

core::TaskFactoryDecorator* 
TaskFactorySelectionDialog::factory() const
{
  return d->model->factory( d->view->currentIndex() );
}

QString 
TaskFactorySelectionDialog::factoryId() const
{
  return d->model->factoryId( d->view->currentIndex() );
}

}

}