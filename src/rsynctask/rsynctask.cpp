/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "rsynctask.h"

#include "rsyncconfigdialog.h"

#include <KConfigGroup>
#include <KDebug>

#include <QTimer>

namespace Config
{
  static const QString RSyncProgram = "rsyncProgram";
  static const QString Source = "source";
  static const QString Destination = "destination";
  static const QString UseArchiveMode = "useArchiveMode";
  static const QString UseChecksumForSkipping = "useChecksumForSkipping";
  static const QString Recursive = "recursive";
  static const QString RelativePathNames = "relativePathNames";
  static const QString NoImpliedDirs = "noImpliedDirs";
  static const QString Backup = "backup";
  static const QString BackupDir = "backupDir";
  static const QString BackupSuffix = "backupSuffix";
  static const QString Update = "update";
  static const QString UpdateInPlace = "updateInPlace";
  static const QString AppendToFiles = "appendToFiles";
  static const QString AppendVerify = "appendVerify";
  static const QString TransferDirectoriesNonRecursive = "transferDirectoriesNonResurcive";
  static const QString PreserveSymLinks = "preserveSymLinks";
  static const QString CopySymLinks = "copySymLinks";
  static const QString CopyOnlyUnsafeSymLinks = "copyOnlyUnsafeSymLinks";
  static const QString SkipUnsafeLinks = "skipUnsafeLinks";
  static const QString CopyDirSymLinks = "copyDirSymLinks";
  static const QString KeepDirectorySymLinks = "keepDirectorySymLinks";
  static const QString PreserveHardLinks = "preserveHardLinks";
  static const QString PreservePermissions = "preservePermissions";
  static const QString PreserveExecutability = "preserveExecutablility";
  static const QString NewPermissions = "newPermissions";
  static const QString PreserveACLs = "preserveACLs";
  static const QString PreserveExtendedAttributes = "preserveExtendedAttributes";
  static const QString PreserveOwner = "preserveOwner";
  static const QString PreserveGroup = "preserveGroup";
  static const QString PreserveDevices = "preserveDevices";
  static const QString PreserveSpecialFiles = "preserveSpecialFiles";
  static const QString PreserveModificationTimes = "preserveModificationTimes";
  static const QString DoNotPreserveModificationTimesForDirs = "doNotPreserveModificationTimesForDirs";
  static const QString ReceiverAttemptsSuperUserActivities = "receiverAttemptsSuperUserActivities";
  static const QString RestorePrivilegedAttributes = "restorePrivilegedAttributes";
  static const QString EfficientSparseFileHandling = "efficientSparseFileHandling";
  static const QString RunDry = "runDry";
  static const QString NoDeltaTransferMode = "noDeltaTransferMode";
  static const QString DontCrossFileSystemBoundaries = "dontCrossFileSystemBoundaries";
  static const QString ChecksumBlockSize = "checksumBlockSize";
  static const QString RemoteShell = "remoteShell";
  static const QString RemoteRsyncPath = "remoteRsyncPath";
  static const QString DontCreateNewFilesOnReceiver = "dontCreateNewFilesOnReceiver";
  static const QString DontUpdateExistingFilesOnReceiver = "dontUpdateExistingFilesOnReceiver";
  static const QString RemoveSourceFilesAfterSync = "removeSourceFilesAfterSync";
  static const QString DeleteExtraneousFilesOnReceiver = "deleteExtraneousFilesOnReceiver";
  static const QString DeleteTime = "deleteTime";
  static const QString DeleteExcludedFiles = "deleteExcludedFiles";
  static const QString IgnoreIOErrorsOnDelete = "ignoreIOErrorsOnDelete";
  static const QString DeleteNonEmptyDirectories = "deleteNonEmptyDirectories";
  static const QString MaxNumDeleteFiles = "maxNumDeleteFiles";
  static const QString MaxFileSizeToTransfer = "maxFileSizeToTransfer";
  static const QString MinFileSizeToTransfer = "minFileSizeToTransfer";
  static const QString KeepPartiallyTransferredFiles = "keepPartiallyTransferredFiles";
  static const QString PartialFileTargetDirectory = "partialFileTargetDirectory";
  static const QString DelayUpdates = "delayUpdates";
  static const QString PruneEmptyDirectoriesOnReceiver = "pruneEmptyDirectoriesOnReceiver";
  static const QString UseNumericIDs = "useNumericIDs";
  static const QString IOTimeout = "IOTimeout";
  static const QString DaemonConnectionTimeout = "daemonConnectionTimeout";
  static const QString IgnoreTimeAndSizeForSkipping = "ignoreTimeAndSizeForSkipping";
  static const QString SkipFilesBasedOnSize = "skipFilesBasedOnSize";
  static const QString ModifyWindow = "modifyWindow";
  static const QString TemporaryDirectory = "temporaryDirectory";
  static const QString FindSimilarBaseFile = "findSimilarBaseFile";
  static const QString CompareReceivedFilesToThisDir = "compareReceivedFilesToThisDir";
  static const QString IncludeCopiesOfUnchangedFilesFromThisDir = "includeCopiesOfUnchangedFromThisDir";
  static const QString HardLinkToFilesWhenUnchangedinThisDir = "hardLinkToFilesWhenUnchangedInThisDir";
  static const QString CompressionLevel = "compressionLevel";
  static const QString OmitCompressionSuffixes = "omitCompressionSuffixes";
  static const QString UseCVSExcludeFilters = "useCVSExcludeFilters";
  static const QString FileFilterRules = "fileFilterRules";
  static const QString UseAutoMergeFilterRule = "useAutoMergeFilterRule";
  static const QString ExcludePatterns = "excludePatterns";
  static const QString ExcludePatternFile = "exludePatternFile";
  static const QString IncludePatterns = "includePatterns";
  static const QString IncludePatternFile = "includePatternFile";
  static const QString SourceFilesListFile = "sourceFilesListFile";
  static const QString UseZeraTerminatedFromFilterFiles = "useZeroTerminatedFromFilterFiles";
  static const QString protectArguments = "protectArguments";
  static const QString OutgoingDaemonSocketAddress = "outgoingDaemonSocketAddress";
  static const QString AlternatePortNumber = "alternatePortNumber";
  static const QString CustomTCPOptions = "customTCPOptions";
  static const QString UseRemoteShellBlockinIO = "useRemoteShellBlockingIO";
  static const QString  ShowChangeSummary = "showChangeSummaryForUpdates";
  static const QString LogFile = "logFile";
  static const QString LogFileFormat = "logFileFormat";
  static const QString DaemonPasswordFile = "daemonPasswordFile";
  static const QString ListFilesOnly = "listFilesOnly";
  static const QString BandwidthLimit = "bandwidthLimit";
  static const QString WriteBatchedUpdateToFile = "writeBatchedUpdateToFile";
  static const QString OnlyWriteBatchedUpdate = "onlyWriteBatchedUpdate";
  static const QString ReadBatchedUpdateFromFile = "readBatchedUpdateFromFile";
  static const QString ProtocolVersion = "protocolVersion";
  static const QString CharsetConversionSpec = "charsetConversionSpec";
  static const QString ChecksumSeed = "checksumSeed";
  static const QString IPProtocol = "ipProtocol";
  static const QString PrintVersion = "printVersion";
}

RSyncTask::RSyncTask( QObject* parent ) : 
  Task( parent )
{
  setDefaultValues();
  m_process = 0;
}

RSyncTask::~RSyncTask()
{
}

void 
RSyncTask::configure()
{
  RSyncConfigDialog d( this );
  d.exec();
}

void 
RSyncTask::restore( KConfigGroup& config )
{
  setDefaultValues();
  
  m_rsyncProgram = config.readEntry< QString >( Config::RSyncProgram, m_rsyncProgram );
  m_source = config.readEntry< QString >( Config::Source, m_source );
  m_destination = config.readEntry< QString >( Config::Destination, m_destination );
  m_useArchiveMode = config.readEntry< bool >( Config::UseArchiveMode, m_useArchiveMode );
  m_useChecksumForSkipping = config.readEntry< bool >( Config::UseChecksumForSkipping, m_useChecksumForSkipping );
  m_recursive = config.readEntry< bool >( Config::Recursive, m_recursive );
  m_relativePathNames = config.readEntry< bool >( Config::RelativePathNames, m_relativePathNames );
  m_noImpliedDirs = config.readEntry< bool >( Config::NoImpliedDirs, m_noImpliedDirs );
  m_backup = config.readEntry< bool >( Config::Backup, m_backup );
  m_backupDir = config.readEntry< QString >( Config::BackupDir, m_backupDir );
  m_backupSuffix = config.readEntry< QString >( Config::BackupSuffix, m_backupSuffix );
  m_update = config.readEntry< bool >( Config::Update, m_update );
  m_updateInPlace = config.readEntry< bool >( Config::UpdateInPlace, m_updateInPlace );
  m_appendToFiles = config.readEntry< bool >( Config::AppendToFiles, m_appendToFiles );
  m_appendVerify = config.readEntry< bool >( Config::AppendVerify, m_appendVerify );
  m_transferDirectoriesNonResurcive = config.readEntry< bool >( Config::TransferDirectoriesNonRecursive, m_transferDirectoriesNonResurcive );
  m_preserveSymLinks = config.readEntry< bool >( Config::PreserveSymLinks, m_preserveSymLinks );
  m_copySymLinks = config.readEntry< bool >( Config::CopySymLinks, m_copySymLinks );
  m_copyOnlyUnsafeSymLinks = config.readEntry< bool >( Config::CopyOnlyUnsafeSymLinks, m_copyOnlyUnsafeSymLinks );
  m_skipUnsafeLinks = config.readEntry< bool >( Config::SkipUnsafeLinks, m_skipUnsafeLinks );
  m_copyDirSymLinks = config.readEntry< bool >( Config::CopyDirSymLinks, m_copyDirSymLinks );
  m_keepDirectorySymLinks = config.readEntry< bool >( Config::KeepDirectorySymLinks, m_keepDirectorySymLinks );
  m_preserveHardLinks = config.readEntry< bool >( Config::PreserveHardLinks, m_preserveHardLinks );
  m_preservePermissions = config.readEntry< bool >( Config::PreservePermissions, m_preservePermissions );
  m_preserveExecutability = config.readEntry< bool >( Config::PreserveExecutability, m_preserveExecutability );
  m_newPermissions = config.readEntry< QString >( Config::NewPermissions, m_newPermissions );
  m_preserveACLs = config.readEntry< bool >( Config::PreserveACLs, m_preserveACLs );
  m_preserveExtendedAttributes = config.readEntry< bool >( Config::PreserveExtendedAttributes, m_preserveExtendedAttributes );
  m_preserveOwner = config.readEntry< bool >( Config::PreserveOwner, m_preserveOwner );
  m_preserveGroup = config.readEntry< bool >( Config::PreserveGroup, m_preserveGroup );
  m_preserveDevices = config.readEntry< bool >( Config::PreserveDevices, m_preserveDevices );
  m_preserveSpecialFiles = config.readEntry< bool >( Config::PreserveSpecialFiles, m_preserveSpecialFiles );
  m_preserveModificationTimes = config.readEntry< bool >( Config::PreserveModificationTimes, m_preserveModificationTimes );
  m_doNotPreserveModificationTimesForDirs = config.readEntry< bool >( Config::DoNotPreserveModificationTimesForDirs, m_doNotPreserveModificationTimesForDirs );
  m_receiverAttemptsSuperUserActivities = config.readEntry< bool >( Config::ReceiverAttemptsSuperUserActivities, m_receiverAttemptsSuperUserActivities );
  m_restorePrivilegedAttributes = config.readEntry< bool >( Config::RestorePrivilegedAttributes, m_restorePrivilegedAttributes );
  m_efficientSparseFileHandling = config.readEntry< bool >( Config::EfficientSparseFileHandling, m_efficientSparseFileHandling );
  m_runDry = config.readEntry< bool >( Config::RunDry, m_runDry );
  m_noDeltaTransferMode = config.readEntry< bool >( Config::NoDeltaTransferMode, m_noDeltaTransferMode );
  m_dontCrossFileSystemBoundaries = config.readEntry< bool >( Config::DontCrossFileSystemBoundaries, m_dontCrossFileSystemBoundaries );
  m_checksumBlockSize = config.readEntry< int >( Config::ChecksumBlockSize, m_checksumBlockSize );
  m_remoteShell = config.readEntry< QString >( Config::RemoteShell, m_remoteShell );
  m_remoteRsyncPath = config.readEntry< QString >( Config::RemoteRsyncPath, m_remoteRsyncPath );
  m_dontCreateNewFilesOnReceiver = config.readEntry< bool >( Config::DontCreateNewFilesOnReceiver, m_dontCreateNewFilesOnReceiver );
  m_dontUpdateExistingFilesOnReceiver = config.readEntry< bool >( Config::DontUpdateExistingFilesOnReceiver, m_dontUpdateExistingFilesOnReceiver );
  m_removeSourceFilesAfterSync = config.readEntry< bool >( Config::RemoveSourceFilesAfterSync, m_removeSourceFilesAfterSync );
  m_deleteExtraneousFilesOnReceiver = config.readEntry< bool >( Config::DeleteExtraneousFilesOnReceiver, m_deleteExtraneousFilesOnReceiver );
  m_deleteTime = ( DeleteTime ) config.readEntry< int >( Config::DeleteTime, ( int ) m_deleteTime );
  m_deleteExcludedFiles = config.readEntry< bool >( Config::DeleteExcludedFiles, m_deleteExcludedFiles );
  m_ignoreIOErrorsOnDelete = config.readEntry< bool >( Config::IgnoreIOErrorsOnDelete, m_ignoreIOErrorsOnDelete );
  m_deleteNonEmptyDirectories = config.readEntry< bool >( Config::DeleteNonEmptyDirectories, m_deleteNonEmptyDirectories );
  m_maxNumDeleteFiles = config.readEntry< int >( Config::MaxNumDeleteFiles, m_maxNumDeleteFiles );
  m_maxFileSizeToTransfer = config.readEntry< int >( Config::MaxFileSizeToTransfer, m_maxFileSizeToTransfer );
  m_minFileSizeToTransfer = config.readEntry< int >( Config::MinFileSizeToTransfer, m_minFileSizeToTransfer );
  m_keepPartiallyTransferredFiles = config.readEntry< bool >( Config::KeepPartiallyTransferredFiles, m_keepPartiallyTransferredFiles );
  m_partialFileTargetDirectory = config.readEntry< QString >( Config::PartialFileTargetDirectory, m_partialFileTargetDirectory );
  m_delayUpdates = config.readEntry< bool >( Config::DelayUpdates, m_delayUpdates );
  m_pruneEmptyDirectoriesOnReceiver = config.readEntry< bool >( Config::PruneEmptyDirectoriesOnReceiver, m_pruneEmptyDirectoriesOnReceiver );
  m_useNumericIDs = config.readEntry< bool >( Config::UseNumericIDs, m_useNumericIDs );
  m_IOTimeout = config.readEntry< int >( Config::IOTimeout, m_IOTimeout );
  m_daemonConnectionTimeout = config.readEntry< int >( Config::DaemonConnectionTimeout, m_daemonConnectionTimeout );
  m_ignoreTimeAndSizeForSkipping = config.readEntry< bool >( Config::IgnoreTimeAndSizeForSkipping, m_ignoreTimeAndSizeForSkipping );
  m_skipFilesBasedOnSize = config.readEntry< bool >( Config::SkipFilesBasedOnSize, m_skipFilesBasedOnSize );
  m_modifyWindow = config.readEntry< bool >( Config::ModifyWindow, m_modifyWindow );
  m_temporaryDirectory = config.readEntry< QString >( Config::TemporaryDirectory, m_temporaryDirectory );
  m_findSimilarBaseFile = config.readEntry< bool >( Config::FindSimilarBaseFile, m_findSimilarBaseFile );
  m_compareReceivedFilesToThisDir = config.readEntry< QString >( Config::CompareReceivedFilesToThisDir, m_compareReceivedFilesToThisDir );
  m_includeCopiesOfUnchangedFromThisDir = config.readEntry< QString >( Config::IncludeCopiesOfUnchangedFilesFromThisDir, m_includeCopiesOfUnchangedFromThisDir );
  m_hardLinkToFilesWhenUnchangedInThisDir = config.readEntry< QString >( Config::HardLinkToFilesWhenUnchangedinThisDir, m_hardLinkToFilesWhenUnchangedInThisDir );
  m_compressionLevel = config.readEntry< int >( Config::CompressionLevel, m_compressionLevel );
  m_omitCompressionSuffixes = config.readEntry< QStringList >( Config::OmitCompressionSuffixes, m_omitCompressionSuffixes );
  m_useCVSExcludeFilters = config.readEntry< bool >( Config::UseCVSExcludeFilters, m_useCVSExcludeFilters );
  m_fileFilterRules = config.readEntry< QStringList >( Config::FileFilterRules, m_fileFilterRules );
  m_useAutoMergeFilterRule = config.readEntry< bool >( Config::UseAutoMergeFilterRule, m_useAutoMergeFilterRule );
  m_excludePatterns = config.readEntry< QStringList >( Config::ExcludePatterns, m_excludePatterns );
  m_exludePatternFile = config.readEntry< QString >( Config::ExcludePatternFile, m_exludePatternFile );
  m_includePatterns = config.readEntry< QStringList >( Config::IncludePatterns, m_includePatterns );
  m_includePatternFile = config.readEntry< QString >( Config::IncludePatternFile, m_includePatternFile );
  m_sourceFilesListFile = config.readEntry< QString >( Config::SourceFilesListFile, m_sourceFilesListFile );
  m_useZeroTerminatedFromFilterFiles = config.readEntry< bool >( Config::UseZeraTerminatedFromFilterFiles, m_useZeroTerminatedFromFilterFiles );
  m_protectArguments = config.readEntry< bool >( Config::protectArguments, m_protectArguments );
  m_outgoingDaemonSocketAddress = config.readEntry< QString >( Config::OutgoingDaemonSocketAddress, m_outgoingDaemonSocketAddress );
  m_alternatePortNumber = config.readEntry< int >( Config::AlternatePortNumber, m_alternatePortNumber );
  m_customTCPOptions = config.readEntry< QString >( Config::CustomTCPOptions, m_customTCPOptions );
  m_useRemoteShellBlockingIO = config.readEntry< bool >( Config::UseRemoteShellBlockinIO, m_useRemoteShellBlockingIO );
  m_showChangeSummaryForUpdates = config.readEntry< bool >( Config:: ShowChangeSummary, m_showChangeSummaryForUpdates );
  m_logFile = config.readEntry< QString >( Config::LogFile, m_logFile );
  m_logFileFormat = config.readEntry< QString >( Config::LogFileFormat, m_logFileFormat );
  m_daemonPasswordFile = config.readEntry< QString >( Config::DaemonPasswordFile, m_daemonPasswordFile );
  m_listFilesOnly = config.readEntry< bool >( Config::ListFilesOnly, m_listFilesOnly );
  m_bandwidthLimit = config.readEntry< int >( Config::BandwidthLimit, m_bandwidthLimit );
  m_writeBatchedUpdateToFile = config.readEntry< QString >( Config::WriteBatchedUpdateToFile, m_writeBatchedUpdateToFile );
  m_onlyWriteBatchedUpdate = config.readEntry< bool >( Config::OnlyWriteBatchedUpdate, m_onlyWriteBatchedUpdate );
  m_readBatchedUpdateFromFile = config.readEntry< QString >( Config::ReadBatchedUpdateFromFile, m_readBatchedUpdateFromFile );
  m_protocolVersion = config.readEntry< int >( Config::ProtocolVersion, m_protocolVersion );
  m_charsetConversionSpec = config.readEntry< QString >( Config::CharsetConversionSpec, m_charsetConversionSpec );
  m_checksumSeed = config.readEntry< int >( Config::ChecksumSeed, m_checksumSeed );
  m_ipProtocol = ( IPProtocol ) config.readEntry< int >( Config::IPProtocol, ( IPProtocol ) m_ipProtocol );
  m_printVersion = config.readEntry< bool >( Config::PrintVersion, m_printVersion );
}

void  
RSyncTask::save( KConfigGroup& config )
{
  config.writeEntry( Config::RSyncProgram, m_rsyncProgram );
  config.writeEntry( Config::Source, m_source );
  config.writeEntry( Config::Destination, m_destination );
  config.writeEntry( Config::UseArchiveMode, m_useArchiveMode );
  config.writeEntry( Config::UseChecksumForSkipping, m_useChecksumForSkipping );
  config.writeEntry( Config::Recursive, m_recursive );
  config.writeEntry( Config::RelativePathNames, m_relativePathNames );
  config.writeEntry( Config::NoImpliedDirs, m_noImpliedDirs );
  config.writeEntry( Config::Backup, m_backup );
  config.writeEntry( Config::BackupDir, m_backupDir );
  config.writeEntry( Config::BackupSuffix, m_backupSuffix );
  config.writeEntry( Config::Update, m_update );
  config.writeEntry( Config::UpdateInPlace, m_updateInPlace );
  config.writeEntry( Config::AppendToFiles, m_appendToFiles );
  config.writeEntry( Config::AppendVerify, m_appendVerify );
  config.writeEntry( Config::TransferDirectoriesNonRecursive, m_transferDirectoriesNonResurcive );
  config.writeEntry( Config::PreserveSymLinks, m_preserveSymLinks );
  config.writeEntry( Config::CopySymLinks, m_copySymLinks );
  config.writeEntry( Config::CopyOnlyUnsafeSymLinks, m_copyOnlyUnsafeSymLinks );
  config.writeEntry( Config::SkipUnsafeLinks, m_skipUnsafeLinks );
  config.writeEntry( Config::CopyDirSymLinks, m_copyDirSymLinks );
  config.writeEntry( Config::KeepDirectorySymLinks, m_keepDirectorySymLinks );
  config.writeEntry( Config::PreserveHardLinks, m_preserveHardLinks );
  config.writeEntry( Config::PreservePermissions, m_preservePermissions );
  config.writeEntry( Config::PreserveExecutability, m_preserveExecutability );
  config.writeEntry( Config::NewPermissions, m_newPermissions );
  config.writeEntry( Config::PreserveACLs, m_preserveACLs );
  config.writeEntry( Config::PreserveExtendedAttributes, m_preserveExtendedAttributes );
  config.writeEntry( Config::PreserveOwner, m_preserveOwner );
  config.writeEntry( Config::PreserveGroup, m_preserveGroup );
  config.writeEntry( Config::PreserveDevices, m_preserveDevices );
  config.writeEntry( Config::PreserveSpecialFiles, m_preserveSpecialFiles );
  config.writeEntry( Config::PreserveModificationTimes, m_preserveModificationTimes );
  config.writeEntry( Config::DoNotPreserveModificationTimesForDirs, m_doNotPreserveModificationTimesForDirs );
  config.writeEntry( Config::ReceiverAttemptsSuperUserActivities, m_receiverAttemptsSuperUserActivities );
  config.writeEntry( Config::RestorePrivilegedAttributes, m_restorePrivilegedAttributes );
  config.writeEntry( Config::EfficientSparseFileHandling, m_efficientSparseFileHandling );
  config.writeEntry( Config::RunDry, m_runDry );
  config.writeEntry( Config::NoDeltaTransferMode, m_noDeltaTransferMode );
  config.writeEntry( Config::DontCrossFileSystemBoundaries, m_dontCrossFileSystemBoundaries );
  config.writeEntry( Config::ChecksumBlockSize, m_checksumBlockSize );
  config.writeEntry( Config::RemoteShell, m_remoteShell );
  config.writeEntry( Config::RemoteRsyncPath, m_remoteRsyncPath );
  config.writeEntry( Config::DontCreateNewFilesOnReceiver, m_dontCreateNewFilesOnReceiver );
  config.writeEntry( Config::DontUpdateExistingFilesOnReceiver, m_dontUpdateExistingFilesOnReceiver );
  config.writeEntry( Config::RemoveSourceFilesAfterSync, m_removeSourceFilesAfterSync );
  config.writeEntry( Config::DeleteExtraneousFilesOnReceiver, m_deleteExtraneousFilesOnReceiver );
  config.writeEntry( Config::DeleteTime, ( int ) m_deleteTime );
  config.writeEntry( Config::DeleteExcludedFiles, m_deleteExcludedFiles );
  config.writeEntry( Config::IgnoreIOErrorsOnDelete, m_ignoreIOErrorsOnDelete );
  config.writeEntry( Config::DeleteNonEmptyDirectories, m_deleteNonEmptyDirectories );
  config.writeEntry( Config::MaxNumDeleteFiles, m_maxNumDeleteFiles );
  config.writeEntry( Config::MaxFileSizeToTransfer, m_maxFileSizeToTransfer );
  config.writeEntry( Config::MinFileSizeToTransfer, m_minFileSizeToTransfer );
  config.writeEntry( Config::KeepPartiallyTransferredFiles, m_keepPartiallyTransferredFiles );
  config.writeEntry( Config::PartialFileTargetDirectory, m_partialFileTargetDirectory );
  config.writeEntry( Config::DelayUpdates, m_delayUpdates );
  config.writeEntry( Config::PruneEmptyDirectoriesOnReceiver, m_pruneEmptyDirectoriesOnReceiver );
  config.writeEntry( Config::UseNumericIDs, m_useNumericIDs );
  config.writeEntry( Config::IOTimeout, m_IOTimeout );
  config.writeEntry( Config::DaemonConnectionTimeout, m_daemonConnectionTimeout );
  config.writeEntry( Config::IgnoreTimeAndSizeForSkipping, m_ignoreTimeAndSizeForSkipping );
  config.writeEntry( Config::SkipFilesBasedOnSize, m_skipFilesBasedOnSize );
  config.writeEntry( Config::ModifyWindow, m_modifyWindow );
  config.writeEntry( Config::TemporaryDirectory, m_temporaryDirectory );
  config.writeEntry( Config::FindSimilarBaseFile, m_findSimilarBaseFile );
  config.writeEntry( Config::CompareReceivedFilesToThisDir, m_compareReceivedFilesToThisDir );
  config.writeEntry( Config::IncludeCopiesOfUnchangedFilesFromThisDir, m_includeCopiesOfUnchangedFromThisDir );
  config.writeEntry( Config::HardLinkToFilesWhenUnchangedinThisDir, m_hardLinkToFilesWhenUnchangedInThisDir );
  config.writeEntry( Config::CompressionLevel, m_compressionLevel );
  config.writeEntry( Config::OmitCompressionSuffixes, m_omitCompressionSuffixes );
  config.writeEntry( Config::UseCVSExcludeFilters, m_useCVSExcludeFilters );
  config.writeEntry( Config::FileFilterRules, m_fileFilterRules );
  config.writeEntry( Config::UseAutoMergeFilterRule, m_useAutoMergeFilterRule );
  config.writeEntry( Config::ExcludePatterns, m_excludePatterns );
  config.writeEntry( Config::ExcludePatternFile, m_exludePatternFile );
  config.writeEntry( Config::IncludePatterns, m_includePatterns );
  config.writeEntry( Config::IncludePatternFile, m_includePatternFile );
  config.writeEntry( Config::SourceFilesListFile, m_sourceFilesListFile );
  config.writeEntry( Config::UseZeraTerminatedFromFilterFiles, m_useZeroTerminatedFromFilterFiles );
  config.writeEntry( Config::protectArguments, m_protectArguments );
  config.writeEntry( Config::OutgoingDaemonSocketAddress, m_outgoingDaemonSocketAddress );
  config.writeEntry( Config::AlternatePortNumber, m_alternatePortNumber );
  config.writeEntry( Config::CustomTCPOptions, m_customTCPOptions );
  config.writeEntry( Config::UseRemoteShellBlockinIO, m_useRemoteShellBlockingIO );
  config.writeEntry( Config:: ShowChangeSummary, m_showChangeSummaryForUpdates );
  config.writeEntry( Config::LogFile, m_logFile );
  config.writeEntry( Config::LogFileFormat, m_logFileFormat );
  config.writeEntry( Config::DaemonPasswordFile, m_daemonPasswordFile );
  config.writeEntry( Config::ListFilesOnly, m_listFilesOnly );
  config.writeEntry( Config::BandwidthLimit, m_bandwidthLimit );
  config.writeEntry( Config::WriteBatchedUpdateToFile, m_writeBatchedUpdateToFile );
  config.writeEntry( Config::OnlyWriteBatchedUpdate, m_onlyWriteBatchedUpdate );
  config.writeEntry( Config::ReadBatchedUpdateFromFile, m_readBatchedUpdateFromFile );
  config.writeEntry( Config::ProtocolVersion, m_protocolVersion );
  config.writeEntry( Config::CharsetConversionSpec, m_charsetConversionSpec );
  config.writeEntry( Config::ChecksumSeed, m_checksumSeed );
  config.writeEntry( Config::IPProtocol, ( int ) m_ipProtocol );
  config.writeEntry( Config::PrintVersion, m_printVersion );
}

ki3u::core::Task* 
RSyncTask::clone()
{
  RSyncTask *result = new RSyncTask();
  
  // TODO: Copy own values (as far as needed)
  result->m_rsyncProgram = m_rsyncProgram;
  result->m_source = m_source;
  result->m_destination = m_destination;
  result->m_useArchiveMode = m_useArchiveMode;
  result->m_useChecksumForSkipping = m_useChecksumForSkipping;
  result->m_recursive = m_recursive;
  result->m_relativePathNames = m_relativePathNames;
  result->m_noImpliedDirs = m_noImpliedDirs;
  result->m_backup = m_backup;
  result->m_backupDir = m_backupDir;
  result->m_backupSuffix = m_backupSuffix;
  result->m_update = m_update;
  result->m_updateInPlace = m_updateInPlace;
  result->m_appendToFiles = m_appendToFiles;
  result->m_appendVerify = m_appendVerify;
  result->m_transferDirectoriesNonResurcive = m_transferDirectoriesNonResurcive;
  result->m_preserveSymLinks = m_preserveSymLinks;
  result->m_copySymLinks = m_copySymLinks;
  result->m_copyOnlyUnsafeSymLinks = m_copyOnlyUnsafeSymLinks;
  result->m_skipUnsafeLinks = m_skipUnsafeLinks;
  result->m_copyDirSymLinks = m_copyDirSymLinks;
  result->m_keepDirectorySymLinks = m_keepDirectorySymLinks;
  result->m_preserveHardLinks = m_preserveHardLinks;
  result->m_preservePermissions = m_preservePermissions;
  result->m_preserveExecutability = m_preserveExecutability;
  result->m_newPermissions = m_newPermissions;
  result->m_preserveACLs = m_preserveACLs;
  result->m_preserveExtendedAttributes = m_preserveExtendedAttributes;
  result->m_preserveOwner = m_preserveOwner;
  result->m_preserveGroup = m_preserveGroup;
  result->m_preserveDevices = m_preserveDevices;
  result->m_preserveSpecialFiles = m_preserveSpecialFiles;
  result->m_preserveModificationTimes = m_preserveModificationTimes;
  result->m_doNotPreserveModificationTimesForDirs = m_doNotPreserveModificationTimesForDirs;
  result->m_receiverAttemptsSuperUserActivities = m_receiverAttemptsSuperUserActivities;
  result->m_restorePrivilegedAttributes = m_restorePrivilegedAttributes;
  result->m_efficientSparseFileHandling = m_efficientSparseFileHandling;
  result->m_runDry = m_runDry;
  result->m_noDeltaTransferMode = m_noDeltaTransferMode;
  result->m_dontCrossFileSystemBoundaries = m_dontCrossFileSystemBoundaries;
  result->m_checksumBlockSize = m_checksumBlockSize;
  result->m_remoteShell = m_remoteShell;
  result->m_remoteRsyncPath = m_remoteRsyncPath;
  result->m_dontCreateNewFilesOnReceiver = m_dontCreateNewFilesOnReceiver;
  result->m_dontUpdateExistingFilesOnReceiver = m_dontUpdateExistingFilesOnReceiver;
  result->m_removeSourceFilesAfterSync = m_removeSourceFilesAfterSync;
  result->m_deleteExtraneousFilesOnReceiver = m_deleteExtraneousFilesOnReceiver;
  result->m_deleteTime = m_deleteTime;
  result->m_deleteExcludedFiles = m_deleteExcludedFiles;
  result->m_ignoreIOErrorsOnDelete = m_ignoreIOErrorsOnDelete;
  result->m_deleteNonEmptyDirectories = m_deleteNonEmptyDirectories;
  result->m_maxNumDeleteFiles = m_maxNumDeleteFiles;
  result->m_maxFileSizeToTransfer = m_maxFileSizeToTransfer;
  result->m_minFileSizeToTransfer = m_minFileSizeToTransfer;
  result->m_keepPartiallyTransferredFiles = m_keepPartiallyTransferredFiles;
  result->m_partialFileTargetDirectory = m_partialFileTargetDirectory;
  result->m_delayUpdates = m_delayUpdates;
  result->m_pruneEmptyDirectoriesOnReceiver = m_pruneEmptyDirectoriesOnReceiver;
  result->m_useNumericIDs = m_useNumericIDs;
  result->m_IOTimeout = m_IOTimeout;
  result->m_daemonConnectionTimeout = m_daemonConnectionTimeout;
  result->m_ignoreTimeAndSizeForSkipping = m_ignoreTimeAndSizeForSkipping;
  result->m_skipFilesBasedOnSize = m_skipFilesBasedOnSize;
  result->m_modifyWindow = m_modifyWindow;
  result->m_temporaryDirectory = m_temporaryDirectory;
  result->m_findSimilarBaseFile = m_findSimilarBaseFile;
  result->m_compareReceivedFilesToThisDir = m_compareReceivedFilesToThisDir;
  result->m_includeCopiesOfUnchangedFromThisDir = m_includeCopiesOfUnchangedFromThisDir;
  result->m_hardLinkToFilesWhenUnchangedInThisDir = m_hardLinkToFilesWhenUnchangedInThisDir;
  result->m_compressionLevel = m_compressionLevel;
  result->m_omitCompressionSuffixes = m_omitCompressionSuffixes;
  result->m_useCVSExcludeFilters = m_useCVSExcludeFilters;
  result->m_fileFilterRules = m_fileFilterRules;
  result->m_useAutoMergeFilterRule = m_useAutoMergeFilterRule;
  result->m_excludePatterns = m_excludePatterns;
  result->m_exludePatternFile = m_exludePatternFile;
  result->m_includePatterns = m_includePatterns;
  result->m_includePatternFile = m_includePatternFile;
  result->m_sourceFilesListFile = m_sourceFilesListFile;
  result->m_useZeroTerminatedFromFilterFiles = m_useZeroTerminatedFromFilterFiles;
  result->m_protectArguments = m_protectArguments;
  result->m_outgoingDaemonSocketAddress = m_outgoingDaemonSocketAddress;
  result->m_alternatePortNumber = m_alternatePortNumber;
  result->m_customTCPOptions = m_customTCPOptions;
  result->m_useRemoteShellBlockingIO = m_useRemoteShellBlockingIO;
  result->m_showChangeSummaryForUpdates = m_showChangeSummaryForUpdates;
  result->m_logFile = m_logFile;
  result->m_logFileFormat = m_logFileFormat;
  result->m_daemonPasswordFile = m_daemonPasswordFile;
  result->m_listFilesOnly = m_listFilesOnly;
  result->m_bandwidthLimit = m_bandwidthLimit;
  result->m_writeBatchedUpdateToFile = m_writeBatchedUpdateToFile;
  result->m_onlyWriteBatchedUpdate = m_onlyWriteBatchedUpdate;
  result->m_readBatchedUpdateFromFile = m_readBatchedUpdateFromFile;
  result->m_protocolVersion = m_protocolVersion;
  result->m_charsetConversionSpec = m_charsetConversionSpec;
  result->m_checksumSeed = m_checksumSeed;
  result->m_ipProtocol = m_ipProtocol;
  result->m_printVersion = m_printVersion; 
  
  return result;
}

bool 
RSyncTask::teardown()
{
  if ( m_process )
  {
    delete m_process;
  }
  
  // TODO: Check if we need to do s/th here
  return true;
}

bool 
RSyncTask::run()
{ 
  m_process->setProgram( m_rsyncProgram, createArgumentList() );
  QTimer::singleShot( 0, this, SLOT(startProcess()) );
  return true;
}

bool 
RSyncTask::setup()
{
   // TODO: Implement some sanity checks (rsync present? source directory empty? ...)
  if ( m_source.isEmpty() || 
       m_destination.isEmpty() ||
       m_rsyncProgram.isEmpty() )
  {
    return false;
  }
  
  m_process = new KProcess( this );
  
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  // check if SSH_ASKPASS is set and - if not - default to ksshaskpass for now
  // TODO: Should be configurable (at best in ki3u core) or
  // even be replaced by ki3u-ssh-askpass
  if ( !env.contains( "SSH_ASKPASS" ) && env.value( "SSH_ASKPASS" ).isEmpty() )
  {
    env.insert( "SSH_ASKPASS", "ksshaskpass" );
  }
  m_process->setProcessEnvironment( env );
  m_process->closeWriteChannel();
  
  m_process->setOutputChannelMode( KProcess::SeparateChannels );
  connect( m_process, SIGNAL(error(QProcess::ProcessError)), 
           this, SLOT(processError(QProcess::ProcessError)) );
  connect( m_process, SIGNAL(finished(int,QProcess::ExitStatus)), 
           this, SLOT(processFinished(int,QProcess::ExitStatus)) );
  connect( m_process, SIGNAL(readyReadStandardError()), 
           this, SLOT(processReadStandardError()) );
  connect( m_process, SIGNAL(readyReadStandardOutput()), 
           this, SLOT(processReadStandardOutput()) );
  connect( m_process, SIGNAL(started()), 
           this, SLOT(processStarted()) );
  
  return true;
}

QStringList 
RSyncTask::createArgumentList() const
{
  QStringList arguments;
  if ( m_useArchiveMode ) arguments << "--archive";
  if ( m_useChecksumForSkipping ) arguments << "--checksum";
  if ( m_recursive ) arguments << "--recursive";
  if ( m_relativePathNames ) arguments << "--relative";
  if ( m_noImpliedDirs ) arguments << "--no-implied-dirs";
  if ( m_backup ) arguments << "--backup";
  if ( !( m_backupDir.isEmpty() ) ) arguments << "--backup-dir=" + m_backupDir;
  if ( !( m_backupSuffix.isEmpty() ) ) arguments << "--suffix=" + m_backupSuffix;
  if ( m_update ) arguments << "--update";
  if ( m_updateInPlace ) arguments << "--inplace";
  if ( m_appendToFiles ) arguments << "--append";
  if ( m_appendVerify ) arguments << "--append-verify";
  if ( m_transferDirectoriesNonResurcive ) arguments << "--dirs";
  if ( m_preserveSymLinks ) arguments << "--links";
  if ( m_copySymLinks ) arguments << "--copy-links";
  if ( m_copyOnlyUnsafeSymLinks ) arguments << "--copy-unsafe-links";
  if ( m_skipUnsafeLinks ) arguments << "--safe-links";
  if ( m_copyDirSymLinks ) arguments << "--copy-dirlinks";
  if ( m_keepDirectorySymLinks ) arguments << "--keep-dirlinks";
  if ( m_preserveHardLinks ) arguments << "--hard-links";
  if ( m_preservePermissions ) arguments << "--perms";
  if ( m_preserveExecutability ) arguments << "--executability";
  if ( !( m_newPermissions.isEmpty() ) ) arguments << "--chmod=" + m_newPermissions;
  if ( m_preserveACLs ) arguments << "--acls";
  if ( m_preserveExtendedAttributes ) arguments << "--xattrs";
  if ( m_preserveOwner ) arguments << "--owner";
  if ( m_preserveGroup ) arguments << "--group";
  if ( m_preserveDevices ) arguments << "--devices";
  if ( m_preserveSpecialFiles ) arguments << "--specials";
  if ( m_preserveModificationTimes ) arguments << "--times";
  if ( m_doNotPreserveModificationTimesForDirs ) arguments << "--omit-dir-times";
  if ( m_receiverAttemptsSuperUserActivities ) arguments << "--super";
  if ( m_restorePrivilegedAttributes ) arguments << "--fake-super";
  if ( m_efficientSparseFileHandling ) arguments << "-sparse";
  if ( m_runDry ) arguments << "--dry-run";
  if ( m_noDeltaTransferMode ) arguments << "--whole-file";
  if ( m_dontCrossFileSystemBoundaries ) arguments << "--one-file-system";
  if ( m_checksumBlockSize > 0 ) arguments << QString( "--block-size=%1" ).arg( m_checksumBlockSize );
  if ( !( m_remoteShell.isEmpty() ) ) arguments << "-rsh=" + m_remoteShell;
  if ( !( m_remoteRsyncPath.isEmpty() ) ) arguments << "--rsync-path=" + m_remoteRsyncPath;
  if ( m_dontCreateNewFilesOnReceiver ) arguments << "--existing";
  if ( m_dontUpdateExistingFilesOnReceiver ) arguments << "--ignore-existing";
  if ( m_removeSourceFilesAfterSync ) arguments << "--remove-source-files";
  if ( m_deleteExtraneousFilesOnReceiver ) 
  {
    arguments << "--delete";
    switch ( m_deleteTime )
    {
      case DeleteBefore: arguments << "--delete-before"; break;
      case DeleteDuring: arguments << "--delete-during"; break;
      case DeleteDelay: arguments << "--delete-delay"; break;
      case DeleteAfter: arguments << "--delete-after"; break;
      default: break;
    }
    if ( m_deleteExcludedFiles ) arguments << "--delete-excluded";
    if ( m_ignoreIOErrorsOnDelete ) arguments << "--ignore-errors";
    if ( m_deleteNonEmptyDirectories ) arguments << "--force";
    if ( m_maxNumDeleteFiles > 0 ) arguments << QString( "--max-delete=%1" ).arg( m_maxNumDeleteFiles );
  }
  if ( m_maxFileSizeToTransfer > 0 ) arguments << QString( "--max-size=%1" ).arg( m_maxFileSizeToTransfer );
  if ( m_minFileSizeToTransfer > 0 ) arguments << QString( "--min-size=%1" ).arg( m_minFileSizeToTransfer );
  if ( m_keepPartiallyTransferredFiles ) arguments << "--partial";
  if ( !( m_partialFileTargetDirectory.isEmpty() ) ) arguments << "--partial-dir=" + m_partialFileTargetDirectory;
  if ( m_delayUpdates ) arguments << "--delay-updates";
  if ( m_pruneEmptyDirectoriesOnReceiver ) arguments << "--prune-empty-dirs";
  if ( m_useNumericIDs ) arguments << "--numeric-ids";
  if ( m_IOTimeout > 0 ) arguments << QString( "--timeout=%1" ).arg( m_IOTimeout );
  if ( m_daemonConnectionTimeout > 0 ) arguments << QString( "--contimeout=%1" ).arg( m_daemonConnectionTimeout );
  if ( m_ignoreTimeAndSizeForSkipping ) arguments << "--ignore-times";
  if ( m_skipFilesBasedOnSize ) arguments << "--size-only";
  if ( m_modifyWindow > 0 ) arguments << QString( "--modify-window=%1" ).arg( m_modifyWindow );
  if ( !( m_temporaryDirectory.isEmpty() ) ) arguments << "--temp-dir=" + m_temporaryDirectory;
  if ( m_findSimilarBaseFile ) arguments << "--fuzzy";
  if ( !( m_compareReceivedFilesToThisDir.isEmpty() ) ) arguments << "--compare-dest=" + m_compareReceivedFilesToThisDir;
  if ( !( m_includeCopiesOfUnchangedFromThisDir.isEmpty() ) ) arguments << "--copy-dest=" + m_includeCopiesOfUnchangedFromThisDir;
  if ( !( m_hardLinkToFilesWhenUnchangedInThisDir ).isEmpty() ) arguments << "--link-dest=" + m_hardLinkToFilesWhenUnchangedInThisDir;
  if ( m_compressionLevel > 0 )
  {
    arguments << "--compress";
    arguments << QString( "--compress-level=%1" ).arg( m_compressionLevel );
    if ( m_omitCompressionSuffixes.size() > 0 ) arguments << "--skip-compress=" + m_omitCompressionSuffixes.join( " " );
  }
  if ( m_useCVSExcludeFilters ) arguments << "--cvs-exclude";
  foreach ( QString filter, m_fileFilterRules ) arguments << "--filter=" + filter;
  if ( m_useAutoMergeFilterRule ) arguments << "-F";
  foreach ( QString pattern, m_excludePatterns ) arguments << "--exclude=" + pattern;
  if ( !( m_exludePatternFile.isEmpty() ) ) arguments << "--exclude-from=" + m_exludePatternFile;
  foreach ( QString pattern, m_includePatterns ) arguments << "--include=" + pattern;
  if ( !( m_includePatternFile.isEmpty() ) ) arguments << "--include-from=" + m_includePatternFile;
  if ( !( m_sourceFilesListFile.isEmpty() ) ) arguments << "--files-from=" + m_sourceFilesListFile;
  if ( m_useZeroTerminatedFromFilterFiles ) arguments << "--from0";
  if ( m_protectArguments ) arguments << "-s";
  if ( !( m_outgoingDaemonSocketAddress.isEmpty() ) ) arguments << "--address=" + m_outgoingDaemonSocketAddress;
  if ( m_alternatePortNumber >= 0 ) arguments << QString( "--port=%1" ).arg( m_alternatePortNumber );
  if ( !( m_customTCPOptions.isEmpty() ) ) arguments << "--sockopts=" + m_customTCPOptions;
  if ( m_useRemoteShellBlockingIO ) arguments << "--blocking-io";
  if ( m_showChangeSummaryForUpdates ) arguments << "--itemize-changes";
  if ( !( m_logFile.isEmpty() ) ) arguments << "--log-file=" + m_logFile;
  if ( !( m_logFileFormat.isEmpty() ) ) arguments << "--log-file-format=" + m_logFileFormat;
  if ( !( m_daemonPasswordFile.isEmpty() ) ) arguments << "--password-file=" + m_daemonPasswordFile;
  if ( m_listFilesOnly ) arguments << "--list-only";
  if ( m_bandwidthLimit > 0 ) arguments << QString( "--bwlimit=%1" ).arg( m_bandwidthLimit );
  if ( !( m_writeBatchedUpdateToFile.isEmpty() ) ) 
  {
    if ( m_onlyWriteBatchedUpdate )
    {
      arguments << "--only-write-batch=" + m_writeBatchedUpdateToFile;
    } else
    {
      arguments << "--write-batch=" + m_writeBatchedUpdateToFile;
    }
  }
  if ( !( m_readBatchedUpdateFromFile.isEmpty() ) ) arguments << "--read-batch=" + m_readBatchedUpdateFromFile;
  if ( m_protocolVersion > 0 ) arguments << QString( "--protocol=%1" ).arg( m_protocolVersion );
  if ( !( m_charsetConversionSpec.isEmpty() ) ) arguments << "--iconv=" + m_charsetConversionSpec;
  if ( m_checksumSeed > 0 ) arguments << QString( "--checksum-seed=%1" ).arg( m_checksumSeed );
  switch ( m_ipProtocol )
  {
    case IPv4: arguments << "--ipv4"; break;
    case IPv6: arguments << "--ipv6"; break;
    default: break;
  }
  if ( m_printVersion ) arguments << "--version";
  arguments << "--stats"
            << "--human-readable"
            << "--progress"
            << "--verbose"
            << m_source 
            << m_destination;
  
  return arguments;
}

void 
RSyncTask::setDefaultValues()
{
  m_rsyncProgram = "rsync";
  m_source = QString();
  m_destination = QString();
  m_useArchiveMode = true;
  m_useChecksumForSkipping = false;
  m_recursive = false;
  m_relativePathNames = false;
  m_noImpliedDirs = false;
  m_backup = false;
  m_backupDir = QString();
  m_backupSuffix = QString();
  m_update = true;
  m_updateInPlace = false;
  m_appendToFiles = false;
  m_appendVerify = false;
  m_transferDirectoriesNonResurcive = false;
  m_preserveSymLinks = false;
  m_copySymLinks = false;
  m_copyOnlyUnsafeSymLinks = false;
  m_skipUnsafeLinks = false;
  m_copyDirSymLinks = false;
  m_keepDirectorySymLinks = false;
  m_preserveHardLinks = false;
  m_preservePermissions = false;
  m_preserveExecutability = false;
  m_newPermissions = QString();
  m_preserveACLs = false;
  m_preserveExtendedAttributes = false;
  m_preserveOwner = false;
  m_preserveGroup = false;
  m_preserveDevices = false;
  m_preserveSpecialFiles = false;
  m_preserveModificationTimes = false;
  m_doNotPreserveModificationTimesForDirs = false;
  m_receiverAttemptsSuperUserActivities = false;
  m_restorePrivilegedAttributes = false;
  m_efficientSparseFileHandling = false;
  m_runDry = false;
  m_noDeltaTransferMode = false;
  m_dontCrossFileSystemBoundaries = false;
  m_checksumBlockSize = -1;
  m_remoteShell = QString();
  m_remoteRsyncPath = QString();
  m_dontCreateNewFilesOnReceiver = false;
  m_dontUpdateExistingFilesOnReceiver = false;
  m_removeSourceFilesAfterSync = false;
  m_deleteExtraneousFilesOnReceiver = false;
  m_deleteTime = DeleteDefault;
  m_deleteExcludedFiles = false;
  m_ignoreIOErrorsOnDelete = false;
  m_deleteNonEmptyDirectories = false;
  m_maxNumDeleteFiles = -1;
  m_maxFileSizeToTransfer = -1;
  m_minFileSizeToTransfer = -1;
  m_keepPartiallyTransferredFiles = false;
  m_partialFileTargetDirectory = QString();
  m_delayUpdates = false;
  m_pruneEmptyDirectoriesOnReceiver = false;
  m_useNumericIDs = false;
  m_IOTimeout = 30;
  m_daemonConnectionTimeout = -1;
  m_ignoreTimeAndSizeForSkipping = false;
  m_skipFilesBasedOnSize = false;
  m_modifyWindow = -1;
  m_temporaryDirectory = QString();
  m_findSimilarBaseFile = false;
  m_compareReceivedFilesToThisDir = QString();
  m_includeCopiesOfUnchangedFromThisDir = QString();
  m_hardLinkToFilesWhenUnchangedInThisDir = QString();
  m_compressionLevel = 0;
  m_omitCompressionSuffixes = QStringList();
  m_useCVSExcludeFilters = false;
  m_fileFilterRules = QStringList();
  m_useAutoMergeFilterRule = false;
  m_excludePatterns = QStringList();
  m_exludePatternFile = QString();
  m_includePatterns = QStringList();
  m_includePatternFile = QString();
  m_sourceFilesListFile = QString();
  m_useZeroTerminatedFromFilterFiles = false;
  m_protectArguments = false;
  m_outgoingDaemonSocketAddress = QString();
  m_alternatePortNumber = -1;
  m_customTCPOptions = QString();
  m_useRemoteShellBlockingIO = false;
  m_showChangeSummaryForUpdates = false;
  m_logFile = QString();
  m_logFileFormat = QString();
  m_daemonPasswordFile = QString();
  m_listFilesOnly = false;
  m_bandwidthLimit = -1;
  m_writeBatchedUpdateToFile = QString();
  m_onlyWriteBatchedUpdate = false;
  m_readBatchedUpdateFromFile = QString();
  m_protocolVersion = -1;
  m_charsetConversionSpec = QString();
  m_checksumSeed = -1;
  m_ipProtocol = DefaultIPVersion;
  m_printVersion = false;
}

void 
RSyncTask::startProcess()
{
  m_process->start();
}

void 
RSyncTask::processError( QProcess::ProcessError error )
{
  if ( error == QProcess::FailedToStart )
  {
    emit runStarted( false );
  }
}

void 
RSyncTask::processFinished( int exitCode, QProcess::ExitStatus exitStatus )
{
  Q_UNUSED( exitStatus );
  emit runFinished( exitCode == 0 );
}

void 
RSyncTask::processReadStandardError()
{
  // TODO: Read and propagate
  m_process->setReadChannel( QProcess::StandardError );
  while ( m_process->canReadLine() )
  {
    QString line = m_process->readLine();
    emit debugMessage( ki3u::core::Task::Error, line );
    kWarning() << line;
  }
}

void 
RSyncTask::processReadStandardOutput()
{
  m_process->setReadChannel( QProcess::StandardOutput );
  while ( m_process->canReadLine() )
  {
    QString line = m_process->readLine().trimmed();
    kWarning() << line;
    QRegExp exp( "to-check=([0-9]*)/([0-9]*)" );
    if ( exp.indexIn( line ) >= 0 )
    {
      int progress = -1;
      int toCheck = exp.capturedTexts()[ 1 ].toInt();
      int totalFiles = exp.capturedTexts()[ 2 ].toInt();
      if ( totalFiles != 0 )
      {
        progress = 100 - toCheck * 100 / totalFiles;
      }
      emit runProgress( progress );
    } else
    {
      emit debugMessage( ki3u::core::Task::Debug, line );
    }
  }
}

void 
RSyncTask::processStarted()
{
  emit runStarted( true );
}



