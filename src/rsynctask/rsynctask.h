/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RSYNCTASK_H
#define RSYNCTASK_H

#include "../core/task.h"

#include <KProcess>

class RSyncConfigDialog;

class RSyncTask : public ki3u::core::Task
{

  Q_OBJECT
  
public:
  
  typedef enum DeleteTime
  {
    DeleteDefault = 0,
    DeleteBefore,
    DeleteDuring,
    DeleteDelay,
    DeleteAfter
  } DeleteTime;
  
  typedef enum IPProtocol
  {
    DefaultIPVersion = 0,
    IPv4,
    IPv6
  } IPProtocol;
  
  RSyncTask(QObject* parent = 0);
  virtual ~RSyncTask();  
  
  virtual void configure();
  virtual void restore(KConfigGroup& config);
  virtual void save(KConfigGroup& config);
  virtual ki3u::core::Task* clone();
  virtual bool teardown();
  virtual bool run();
  virtual bool setup();
  
  friend class RSyncConfigDialog;
  
private:
  
  // "public" properties, copy on clone
  
  QString                               m_rsyncProgram;
  QString                               m_source;
  QString                               m_destination;
  bool                                  m_useArchiveMode; // -a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
  bool                                  m_useChecksumForSkipping; // -c, --checksum              skip based on checksum, not mod-time & size
  bool                                  m_recursive; //-r, --recursive             recurse into directories
  bool                                  m_relativePathNames; // -R, --relative              use relative path names
  bool                                  m_noImpliedDirs; //   --no-implied-dirs       don't send implied dirs with --relative
  bool                                  m_backup; //   -b, --backup                make backups (see --suffix & --backup-dir)
  QString                               m_backupDir; //      --backup-dir=DIR        make backups into hierarchy based in DIR
  QString                               m_backupSuffix; //      --suffix=SUFFIX         backup suffix (default ~ w/o --backup-dir)
  bool                                  m_update; //  -u, --update                skip files that are newer on the receiver
  bool                                  m_updateInPlace; //      --inplace               update destination files in-place
  bool                                  m_appendToFiles; //      --append                append data onto shorter files
  bool                                  m_appendVerify; //      --append-verify         --append w/old data in file checksum
  bool                                  m_transferDirectoriesNonResurcive; //  -d, --dirs                  transfer directories without recursing
  bool                                  m_preserveSymLinks; //  -l, --links                 copy symlinks as symlinks
  bool                                  m_copySymLinks; //  -L, --copy-links            transform symlink into referent file/dir
  bool                                  m_copyOnlyUnsafeSymLinks; //      --copy-unsafe-links     only "unsafe" symlinks are transformed
  bool                                  m_skipUnsafeLinks; //      --safe-links            ignore symlinks that point outside the tree
  bool                                  m_copyDirSymLinks; //  -k, --copy-dirlinks         transform symlink to dir into referent dir
  bool                                  m_keepDirectorySymLinks; //  -K, --keep-dirlinks         treat symlinked dir on receiver as dir
  bool                                  m_preserveHardLinks; //  -H, --hard-links            preserve hard links
  bool                                  m_preservePermissions; //  -p, --perms                 preserve permissions
  bool                                  m_preserveExecutability; //  -E, --executability         preserve executability
  QString                               m_newPermissions; //      --chmod=CHMOD           affect file and/or directory permissions
  bool                                  m_preserveACLs; //  -A, --acls                  preserve ACLs (implies -p)
  bool                                  m_preserveExtendedAttributes; //  -X, --xattrs                preserve extended attributes
  bool                                  m_preserveOwner; //  -o, --owner                 preserve owner (super-user only)
  bool                                  m_preserveGroup; //  -g, --group                 preserve group
  bool                                  m_preserveDevices; //      --devices               preserve device files (super-user only)
  bool                                  m_preserveSpecialFiles; //      --specials              preserve special files
  bool                                  m_preserveModificationTimes; //  -t, --times                 preserve modification times
  bool                                  m_doNotPreserveModificationTimesForDirs; //  -O, --omit-dir-times        omit directories from --times
  bool                                  m_receiverAttemptsSuperUserActivities; //      --super                 receiver attempts super-user activities
  bool                                  m_restorePrivilegedAttributes; //      --fake-super            store/recover privileged attrs using xattrs
  bool                                  m_efficientSparseFileHandling; //  -S, --sparse                handle sparse files efficiently
  bool                                  m_runDry; //  -n, --dry-run               perform a trial run with no changes made
  bool                                  m_noDeltaTransferMode; //  -W, --whole-file            copy files whole (w/o delta-xfer algorithm)
  bool                                  m_dontCrossFileSystemBoundaries; //  -x, --one-file-system       don't cross filesystem boundaries
  int                                   m_checksumBlockSize; //  -B, --block-size=SIZE       force a fixed checksum block-size
  QString                               m_remoteShell; //  -e, --rsh=COMMAND           specify the remote shell to use
  QString                               m_remoteRsyncPath; //      --rsync-path=PROGRAM    specify the rsync to run on remote machine
  bool                                  m_dontCreateNewFilesOnReceiver; //      --existing              skip creating new files on receiver
  bool                                  m_dontUpdateExistingFilesOnReceiver; //      --ignore-existing       skip updating files that exist on receiver
  bool                                  m_removeSourceFilesAfterSync; //      --remove-source-files   sender removes synchronized files (non-dir)
  bool                                  m_deleteExtraneousFilesOnReceiver; //      --delete                delete extraneous files from dest dirs
  DeleteTime                            m_deleteTime;
                                                                           //      --delete-before         receiver deletes before transfer (default)
                                                                           //      --delete-during         receiver deletes during xfer, not before
                                                                           //      --delete-delay          find deletions during, delete after
                                                                           //      --delete-after          receiver deletes after transfer, not before
  bool                                  m_deleteExcludedFiles; //      --delete-excluded       also delete excluded files from dest dirs
  bool                                  m_ignoreIOErrorsOnDelete; //      --ignore-errors         delete even if there are I/O errors
  bool                                  m_deleteNonEmptyDirectories; //      --force                 force deletion of dirs even if not empty
  int                                   m_maxNumDeleteFiles; //      --max-delete=NUM        don't delete more than NUM files
  int                                   m_maxFileSizeToTransfer; //      --max-size=SIZE         don't transfer any file larger than SIZE
  int                                   m_minFileSizeToTransfer; //      --min-size=SIZE         don't transfer any file smaller than SIZE
  bool                                  m_keepPartiallyTransferredFiles; //      --partial               keep partially transferred files
  QString                               m_partialFileTargetDirectory; //      --partial-dir=DIR       put a partially transferred file into DIR
  bool                                  m_delayUpdates; //      --delay-updates         put all updated files into place at end
  bool                                  m_pruneEmptyDirectoriesOnReceiver; //  -m, --prune-empty-dirs      prune empty directory chains from file-list
  bool                                  m_useNumericIDs; //      --numeric-ids           don't map uid/gid values by user/group name
  int                                   m_IOTimeout; //      --timeout=SECONDS       set I/O timeout in seconds
  int                                   m_daemonConnectionTimeout; //      --contimeout=SECONDS    set daemon connection timeout in seconds
  bool                                  m_ignoreTimeAndSizeForSkipping; //  -I, --ignore-times          don't skip files that match size and time
  bool                                  m_skipFilesBasedOnSize; //      --size-only             skip files that match in size
  int                                   m_modifyWindow; //      --modify-window=NUM     compare mod-times with reduced accuracy
  QString                               m_temporaryDirectory; //  -T, --temp-dir=DIR          create temporary files in directory DIR
  bool                                  m_findSimilarBaseFile; //  -y, --fuzzy                 find similar file for basis if no dest file
  QString                               m_compareReceivedFilesToThisDir; //      --compare-dest=DIR      also compare received files relative to DIR
  QString                               m_includeCopiesOfUnchangedFromThisDir; //      --copy-dest=DIR         ... and include copies of unchanged files
  QString                               m_hardLinkToFilesWhenUnchangedInThisDir; //      --link-dest=DIR         hardlink to files in DIR when unchanged
                                                            //  -z, --compress              compress file data during the transfer
  int                                   m_compressionLevel; //      --compress-level=NUM    explicitly set compression level
  QStringList                           m_omitCompressionSuffixes; //      --skip-compress=LIST    skip compressing files with suffix in LIST
  bool                                  m_useCVSExcludeFilters; //  -C, --cvs-exclude           auto-ignore files in the same way CVS does
  QStringList                           m_fileFilterRules; //  -f, --filter=RULE           add a file-filtering RULE
  bool                                  m_useAutoMergeFilterRule; //  -F                          same as --filter='dir-merge /.rsync-filter'
                                                                  //                              repeated: --filter='- .rsync-filter'
  QStringList                           m_excludePatterns; //      --exclude=PATTERN       exclude files matching PATTERN
  QString                               m_exludePatternFile; //      --exclude-from=FILE     read exclude patterns from FILE
  QStringList                           m_includePatterns; //      --include=PATTERN       don't exclude files matching PATTERN
  QString                               m_includePatternFile; //      --include-from=FILE     read include patterns from FILE
  QString                               m_sourceFilesListFile; //      --files-from=FILE       read list of source-file names from FILE
  bool                                  m_useZeroTerminatedFromFilterFiles; //  -0, --from0                 all *from/filter files are delimited by 0s
  bool                                  m_protectArguments; //  -s, --protect-args          no space-splitting; wildcard chars only
  QString                               m_outgoingDaemonSocketAddress; //      --address=ADDRESS       bind address for outgoing socket to daemon
  int                                   m_alternatePortNumber; //      --port=PORT             specify double-colon alternate port number
  QString                               m_customTCPOptions; //      --sockopts=OPTIONS      specify custom TCP options
  bool                                  m_useRemoteShellBlockingIO; //      --blocking-io           use blocking I/O for the remote shell
                                                                    //      --stats                 give some file-transfer stats
                                                                    //  -8, --8-bit-output          leave high-bit chars unescaped in output
                                                                    //  -h, --human-readable        output numbers in a human-readable format
                                                                    //      --progress              show progress during transfer
                                                                    //  -P                          same as --partial --progress
  bool                                  m_showChangeSummaryForUpdates; //  -i, --itemize-changes       output a change-summary for all updates
                                                                       //      --out-format=FORMAT     output updates using the specified FORMAT
  QString                               m_logFile; //      --log-file=FILE         log what we're doing to the specified FILE
  QString                               m_logFileFormat; //      --log-file-format=FMT   log updates using the specified FMT
  QString                               m_daemonPasswordFile; //      --password-file=FILE    read daemon-access password from FILE
  bool                                  m_listFilesOnly; //      --list-only             list the files instead of copying them
  int                                   m_bandwidthLimit; //      --bwlimit=KBPS          limit I/O bandwidth; KBytes per second
  QString                               m_writeBatchedUpdateToFile; //      --write-batch=FILE      write a batched update to FILE
  bool                                  m_onlyWriteBatchedUpdate; //      --only-write-batch=FILE like --write-batch but w/o updating dest
  QString                               m_readBatchedUpdateFromFile; //      --read-batch=FILE       read a batched update from FILE
  int                                   m_protocolVersion; //      --protocol=NUM          force an older protocol version to be used
  QString                               m_charsetConversionSpec; //      --iconv=CONVERT_SPEC    request charset conversion of filenames
  int                                   m_checksumSeed; //      --checksum-seed=NUM     set block/file checksum seed (advanced)
  IPProtocol                            m_ipProtocol; //  -4, --ipv4                  prefer IPv4
                                                      //  -6, --ipv6                  prefer IPv6
  bool                                  m_printVersion; //      --version               print version number
  
  // private properties, do not copy on clone
  
  KProcess                              *m_process;
  
  QStringList createArgumentList() const;
  void setDefaultValues();
  
private slots:
  
  void startProcess();
  
  void processError( QProcess::ProcessError error );
  void processFinished( int exitCode, QProcess::ExitStatus exitStatus );
  void processReadStandardError();
  void processReadStandardOutput();
  void processStarted();
  
  
  
  
};

#endif // RSYNCTASK_H
