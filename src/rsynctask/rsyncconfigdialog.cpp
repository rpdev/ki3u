/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "rsyncconfigdialog.h"

#include "rsynctask.h"

#include "ui_connectionpage.h"
#include "ui_deletepage.h"
#include "ui_fileattributespage.h"
#include "ui_filterpage.h"
#include "ui_generalpage.h"
#include "ui_linkspage.h"
#include "ui_updatepage.h"

#include <KLocale>

class RSyncConfigDialog::RSyncConfigDialogPrivate
{
  
public:
  
  RSyncTask *task;
  
  Ui::ConnectionPage      *connectionPage;
  Ui::DeletePage          *deletePage;
  Ui::FileAttributesPage  *fileAttributesPage;
  Ui::FilterPage          *filterPage;
  Ui::GeneralPage         *generalPage;
  Ui::LinksPage           *linksPage;
  Ui::UpdatePage          *updatePage;
  
  RSyncConfigDialogPrivate( RSyncConfigDialog *dialog ) :
    q( dialog )
  {
  }
  
  virtual ~RSyncConfigDialogPrivate()
  {
  }
  
  void 
  initialize()
  {
    setupPages();
  }
  
  void 
  setSettingsFromTask()
  {
    // General
    generalPage->rsyncProgram->setText( task->m_rsyncProgram );
    generalPage->source->setText( task->m_source );
    generalPage->destination->setText( task->m_destination );
    generalPage->useArchiveMode->setChecked( task->m_useArchiveMode );
    generalPage->updateExistingFiles->setChecked( task->m_update );
    generalPage->deleteExtraneousFiles->setChecked( task->m_deleteExtraneousFilesOnReceiver );
    generalPage->runDry->setChecked( task->m_runDry );
    generalPage->printVersion->setChecked( task->m_printVersion );
    
    // Connection
    connectionPage->remoteShell->setText( task->m_remoteShell );
    connectionPage->remoteRSyncPath->setText( task->m_remoteRsyncPath );
    connectionPage->remoteShellBlockingIO->setChecked( task->m_useRemoteShellBlockingIO );
    connectionPage->outgoingSockAddress->setText( task->m_outgoingDaemonSocketAddress );
    connectionPage->alternatePortNumber->setValue( task->m_alternatePortNumber );
    connectionPage->customTCPOptions->setText( task->m_customTCPOptions );
    connectionPage->daemonPasswordFile->setText( task->m_daemonPasswordFile );
    connectionPage->receiverAttemptsSuperUser->setChecked( task->m_receiverAttemptsSuperUserActivities );
    connectionPage->restorePrivilegedAttributes->setChecked( task->m_restorePrivilegedAttributes );
    connectionPage->noDeltaTransferMode->setChecked( task->m_noDeltaTransferMode );
    connectionPage->ioTimeout->setValue( task->m_IOTimeout );
    connectionPage->contimeout->setValue( task->m_daemonConnectionTimeout );
    connectionPage->compressionLevel->setValue( task->m_compressionLevel );
    connectionPage->omitCompressionSuffixes->setPlainText( task->m_omitCompressionSuffixes.join( "\n" ) );
    connectionPage->bandwidthLimit->setValue( task->m_bandwidthLimit );
    setComboBoxCurrentItem( connectionPage->ipProtocol, ( int ) task->m_ipProtocol );
    
    // Delete
    setComboBoxCurrentItem( deletePage->deleteTime, ( int ) task->m_deleteTime );
    deletePage->deleteExcluded->setChecked( task->m_deleteExcludedFiles );
    deletePage->ignoreIOErrors->setChecked( task->m_ignoreIOErrorsOnDelete );
    deletePage->deleteNonEmptyDirs->setChecked( task->m_deleteNonEmptyDirectories );
    deletePage->maxFilesToDelete->setValue( task->m_maxNumDeleteFiles );
    
    // File Attributes
    fileAttributesPage->preservePermissions->setChecked( task->m_preservePermissions );
    fileAttributesPage->preserveExecutability->setChecked( task->m_preserveExecutability );
    fileAttributesPage->preserveACLs->setChecked( task->m_preserveACLs );
    fileAttributesPage->preserveExtendedAttributes->setChecked( task->m_preserveExtendedAttributes );
    fileAttributesPage->preserveOwner->setChecked( task->m_preserveOwner );
    fileAttributesPage->preserveGroup->setChecked( task->m_preserveGroup );
    fileAttributesPage->preserveDevices->setChecked( task->m_preserveDevices );
    fileAttributesPage->preserveSpecialFiles->setChecked( task->m_preserveSpecialFiles );
    fileAttributesPage->preserveModificationTimes->setChecked( task->m_preserveModificationTimes );
    fileAttributesPage->dontPreserveDirectoryModificationTimes->setChecked( task->m_doNotPreserveModificationTimesForDirs );
    fileAttributesPage->newPermissions->setText( task->m_newPermissions );
    
    // Filters
    filterPage->exclude->setPlainText( task->m_excludePatterns.join( "\n" ) );
    filterPage->excludeFile->setText( task->m_exludePatternFile );
    filterPage->include->setPlainText( task->m_includePatterns.join( "\n" ) );
    filterPage->includeFile->setText( task->m_includePatternFile );
    filterPage->cvsLikePatterns->setChecked( task->m_useCVSExcludeFilters );
    filterPage->fileMerginRules->setPlainText( task->m_fileFilterRules.join( "\n" ) );
    filterPage->useAutomaticFileMergingRules->setChecked( task->m_useAutoMergeFilterRule );
    filterPage->zeroTerminatedFiles->setChecked( task->m_useZeroTerminatedFromFilterFiles );
    
    // Links
    linksPage->preserveSymLinks->setChecked( task->m_preserveSymLinks );
    linksPage->copySymLinks->setChecked( task->m_copySymLinks );
    linksPage->copyOnlyUnsafeSymLinks->setChecked( task->m_copyOnlyUnsafeSymLinks );
    linksPage->skipUnsafeSymLinks->setChecked( task->m_skipUnsafeLinks );
    linksPage->copySymLinksToDirectories->setChecked( task->m_copyDirSymLinks );
    linksPage->preserveSymLinksToDirectories->setChecked( task->m_keepDirectorySymLinks );
    linksPage->preserveHardLinks->setChecked( task->m_preserveHardLinks );
    
    // Updates
    updatePage->updatesInPlace->setChecked( task->m_updateInPlace );
    updatePage->appendFiles->setChecked( task->m_appendToFiles );
    updatePage->appendVerify->setChecked( task->m_appendVerify );
    updatePage->skipExistingFilesOnReceiver->setChecked( task->m_dontUpdateExistingFilesOnReceiver );
    updatePage->delayUpdates->setChecked( task->m_delayUpdates );
    updatePage->showChangeSummary->setChecked( task->m_showChangeSummaryForUpdates );
    updatePage->batchedUpdateFile->setText( task->m_writeBatchedUpdateToFile );
    updatePage->onlyWriteBatchedUpdateFile->setChecked( task->m_onlyWriteBatchedUpdate );
    updatePage->readBatchedUpdateFromFile->setText( task->m_readBatchedUpdateFromFile );
    
  }
  
  void
  applySettings()
  {
    // General
    task->m_rsyncProgram = generalPage->rsyncProgram->text();
    task->m_source = generalPage->source->text();
    task->m_destination = generalPage->destination->text();
    task->m_useArchiveMode = generalPage->useArchiveMode->isChecked();
    task->m_update = generalPage->updateExistingFiles->isChecked();
    task->m_deleteExtraneousFilesOnReceiver = generalPage->deleteExtraneousFiles->isChecked();
    task->m_runDry = generalPage->runDry->isChecked();
    task->m_printVersion = generalPage->printVersion->isChecked();
    
    // Connection
    task->m_remoteShell = connectionPage->remoteShell->text();
    task->m_remoteRsyncPath = connectionPage->remoteRSyncPath->text();
    task->m_useRemoteShellBlockingIO = connectionPage->remoteShellBlockingIO->isChecked();
    task->m_outgoingDaemonSocketAddress = connectionPage->outgoingSockAddress->text();
    task->m_alternatePortNumber = connectionPage->alternatePortNumber->value();
    task->m_customTCPOptions = connectionPage->customTCPOptions->text();
    task->m_daemonPasswordFile = connectionPage->daemonPasswordFile->text();
    task->m_receiverAttemptsSuperUserActivities = connectionPage->receiverAttemptsSuperUser->isChecked();
    task->m_restorePrivilegedAttributes = connectionPage->restorePrivilegedAttributes->isChecked();
    task->m_noDeltaTransferMode = connectionPage->noDeltaTransferMode->isChecked();
    task->m_IOTimeout = connectionPage->ioTimeout->value();
    task->m_daemonConnectionTimeout = connectionPage->contimeout->value();
    task->m_compressionLevel = connectionPage->compressionLevel->value();
    task->m_omitCompressionSuffixes = connectionPage->omitCompressionSuffixes->toPlainText().split( "\n" );
    task->m_bandwidthLimit = connectionPage->bandwidthLimit->value();
    task->m_ipProtocol = ( RSyncTask::IPProtocol ) connectionPage->ipProtocol->itemData( connectionPage->ipProtocol->currentIndex() ).toInt();
    
    // Delete
    task->m_deleteTime = ( RSyncTask::DeleteTime ) deletePage->deleteTime->itemData( deletePage->deleteTime->currentIndex() ).toInt();
    task->m_deleteExcludedFiles = deletePage->deleteExcluded->isChecked();
    task->m_ignoreIOErrorsOnDelete = deletePage->ignoreIOErrors->isChecked();
    task->m_deleteNonEmptyDirectories = deletePage->deleteNonEmptyDirs->isChecked();
    task->m_maxNumDeleteFiles = deletePage->maxFilesToDelete->value();
    
    // File Attributes
    task->m_preservePermissions = fileAttributesPage->preservePermissions->isChecked();
    task->m_preserveExecutability = fileAttributesPage->preserveExecutability->isChecked();
    task->m_preserveACLs = fileAttributesPage->preserveACLs->isChecked();
    task->m_preserveExtendedAttributes = fileAttributesPage->preserveExtendedAttributes->isChecked();
    task->m_preserveOwner = fileAttributesPage->preserveOwner->isChecked();
    task->m_preserveGroup = fileAttributesPage->preserveGroup->isChecked();
    task->m_preserveDevices = fileAttributesPage->preserveDevices->isChecked();
    task->m_preserveSpecialFiles = fileAttributesPage->preserveSpecialFiles->isChecked();
    task->m_preserveModificationTimes = fileAttributesPage->preserveModificationTimes->isChecked();
    task->m_doNotPreserveModificationTimesForDirs = fileAttributesPage->dontPreserveDirectoryModificationTimes->isChecked();
    task->m_newPermissions = fileAttributesPage->newPermissions->text();
    
    // Filters
    task->m_excludePatterns = filterPage->exclude->toPlainText().split( "\n" );
    task->m_exludePatternFile = filterPage->excludeFile->text();
    task->m_includePatterns = filterPage->include->toPlainText().split( "\n" );
    task->m_includePatternFile = filterPage->includeFile->text();
    task->m_useCVSExcludeFilters = filterPage->cvsLikePatterns->isChecked();
    task->m_fileFilterRules = filterPage->fileMerginRules->toPlainText().split( "\n" );
    task->m_useAutoMergeFilterRule = filterPage->useAutomaticFileMergingRules->isChecked();
    task->m_useZeroTerminatedFromFilterFiles = filterPage->zeroTerminatedFiles->isChecked();
    
    // Links
    task->m_preserveSymLinks = linksPage->preserveSymLinks->isChecked();
    task->m_copySymLinks = linksPage->copySymLinks->isChecked();
    task->m_copyOnlyUnsafeSymLinks = linksPage->copyOnlyUnsafeSymLinks->isChecked();
    task->m_skipUnsafeLinks = linksPage->skipUnsafeSymLinks->isChecked();
    task->m_copyDirSymLinks = linksPage->copySymLinksToDirectories->isChecked();
    task->m_keepDirectorySymLinks = linksPage->preserveSymLinksToDirectories->isChecked();
    task->m_preserveHardLinks = linksPage->preserveHardLinks->isChecked();
    
    // Updates
    task->m_updateInPlace = updatePage->updatesInPlace->isChecked();
    task->m_appendToFiles = updatePage->appendFiles->isChecked();
    task->m_appendVerify = updatePage->appendVerify->isChecked();
    task->m_dontUpdateExistingFilesOnReceiver = updatePage->skipExistingFilesOnReceiver->isChecked();
    task->m_delayUpdates = updatePage->delayUpdates->isChecked();
    task->m_showChangeSummaryForUpdates = updatePage->showChangeSummary->isChecked();
    task->m_writeBatchedUpdateToFile = updatePage->batchedUpdateFile->text();
    task->m_onlyWriteBatchedUpdate = updatePage->onlyWriteBatchedUpdateFile->isChecked();
    task->m_readBatchedUpdateFromFile = updatePage->readBatchedUpdateFromFile->text();
  }
  
private:
  
  void 
  setupPages()
  {
    generalPage = new Ui::GeneralPage;
    QWidget *generalWidget = new QWidget();
    generalPage->setupUi( generalWidget );
    KPageWidgetItem *generalPageItem = q->addPage( generalWidget, i18n( "General" ) );
    generalPageItem->setIcon( KIcon( "preferences-system" ) );
    
    connectionPage = new Ui::ConnectionPage;
    QWidget *connectionWidget = new QWidget( q );
    connectionPage->setupUi( connectionWidget );
    connectionPage->ipProtocol->addItem( i18n( "Default" ), ( int ) RSyncTask::DefaultIPVersion );
    connectionPage->ipProtocol->addItem( i18n( "IPv4" ), ( int ) RSyncTask::IPv4 );
    connectionPage->ipProtocol->addItem( i18n( "IPv6" ), ( int ) RSyncTask::IPv6 );
    KPageWidgetItem *connectionPageItem = q->addPage( connectionWidget, i18n( "Connection" ) );
    connectionPageItem->setIcon( KIcon( "network-disconnect") );
    
    deletePage = new Ui::DeletePage();
    QWidget *deleteWidget = new QWidget( q );
    deletePage->setupUi( deleteWidget );
    deletePage->deleteTime->addItem( i18n( "Default" ), ( int ) RSyncTask::DeleteDefault );
    deletePage->deleteTime->addItem( i18n( "After" ), ( int ) RSyncTask::DeleteAfter );
    deletePage->deleteTime->addItem( i18n( "Before" ), ( int ) RSyncTask::DeleteBefore );
    deletePage->deleteTime->addItem( i18n( "Delay" ), ( int ) RSyncTask::DeleteDelay );
    deletePage->deleteTime->addItem( i18n( "During" ), ( int ) RSyncTask::DeleteDuring );
    KPageWidgetItem *deletePageItem = q->addPage( deleteWidget, i18n( "Delete" ) );
    deletePageItem->setIcon( KIcon( "edit-delete" ) );
    deletePageItem->setEnabled( false );
    
    fileAttributesPage = new Ui::FileAttributesPage();
    QWidget *fileAttributesWidget = new QWidget( q );
    fileAttributesPage->setupUi( fileAttributesWidget );
    KPageWidgetItem *fileAttributesPageItem = q->addPage( fileAttributesWidget, i18n( "File Attributes" ) );
    fileAttributesPageItem->setIcon( KIcon( "document-properties" ) );
    
    filterPage = new Ui::FilterPage();
    QWidget *filterWidget = new QWidget( q );
    filterPage->setupUi( filterWidget );
    KPageWidgetItem *filterPageItem = q->addPage( filterWidget, i18n( "Filters" ) );
    filterPageItem->setIcon( KIcon( "view-filter" ) );
    
    linksPage = new Ui::LinksPage();
    QWidget *linksWidget = new QWidget( q );
    linksPage->setupUi( linksWidget );
    KPageWidgetItem *linksPageItem = q->addPage( linksWidget, i18n( "Links" ) );
    linksPageItem->setIcon( KIcon( "emblem-symbolic-link" ) );
    
    updatePage = new Ui::UpdatePage();
    QWidget *updateWidget = new QWidget( q );
    updatePage->setupUi( updateWidget );
    KPageWidgetItem *updatePageItem = q->addPage( updateWidget, i18n( "Update" ) );
    updatePageItem->setIcon( KIcon( "view-refresh" ) );
    
    QObject::connect( generalPage->deleteExtraneousFiles, SIGNAL(toggled(bool)), 
                      deletePageItem, SLOT(setEnabled(bool)) );
  }
  
  void
  setComboBoxCurrentItem( QComboBox *box, int value )
  {
    box->setCurrentIndex( 0 );
    for( int i = 0; i < box->count(); i++ )
    {
      if ( box->itemData( i ).toInt() == value )
      {
        box->setCurrentIndex( i );
        return;
      }
    }
  }
  
  RSyncConfigDialog *q;
  
};

RSyncConfigDialog::RSyncConfigDialog( RSyncTask* task, 
                                      QWidget* parent, Qt::WFlags flags ) : 
  KPageDialog(parent, flags),
  d( new RSyncConfigDialogPrivate( this ) )
{
  d->task = task;
  
  d->initialize();
}

RSyncConfigDialog::~RSyncConfigDialog()
{
  delete d;
}

int
RSyncConfigDialog::exec()
{
  d->setSettingsFromTask();
  int result = KPageDialog::exec();
  if ( result == KPageDialog::Accepted )
  {
    d->applySettings();
  }
  return result;
}

/*
 *   bool                                  m_useChecksumForSkipping; // -c, --checksum              skip based on checksum, not mod-time & size
  bool                                  m_recursive; //-r, --recursive             recurse into directories
  bool                                  m_relativePathNames; // -R, --relative              use relative path names
  bool                                  m_noImpliedDirs; //   --no-implied-dirs       don't send implied dirs with --relative
  bool                                  m_backup; //   -b, --backup                make backups (see --suffix & --backup-dir)
  QString                               m_backupDir; //      --backup-dir=DIR        make backups into hierarchy based in DIR
  QString                               m_backupSuffix; //      --suffix=SUFFIX         backup suffix (default ~ w/o --backup-dir)
  bool                                  m_transferDirectoriesNonResurcive; //  -d, --dirs                  transfer directories without recursing
  bool                                  m_efficientSparseFileHandling; //  -S, --sparse                handle sparse files efficiently
  bool                                  m_dontCrossFileSystemBoundaries; //  -x, --one-file-system       don't cross filesystem boundaries
  int                                   m_checksumBlockSize; //  -B, --block-size=SIZE       force a fixed checksum block-size
  bool                                  m_dontCreateNewFilesOnReceiver; //      --existing              skip creating new files on receiver
  bool                                  m_removeSourceFilesAfterSync; //      --remove-source-files   sender removes synchronized files (non-dir)
  int                                   m_maxFileSizeToTransfer; //      --max-size=SIZE         don't transfer any file larger than SIZE
  int                                   m_minFileSizeToTransfer; //      --min-size=SIZE         don't transfer any file smaller than SIZE
  bool                                  m_keepPartiallyTransferredFiles; //      --partial               keep partially transferred files
  QString                               m_partialFileTargetDirectory; //      --partial-dir=DIR       put a partially transferred file into DIR
  bool                                  m_pruneEmptyDirectoriesOnReceiver; //  -m, --prune-empty-dirs      prune empty directory chains from file-list
  bool                                  m_useNumericIDs; //      --numeric-ids           don't map uid/gid values by user/group name
  bool                                  m_ignoreTimeAndSizeForSkipping; //  -I, --ignore-times          don't skip files that match size and time
  bool                                  m_skipFilesBasedOnSize; //      --size-only             skip files that match in size
  int                                   m_modifyWindow; //      --modify-window=NUM     compare mod-times with reduced accuracy
  QString                               m_temporaryDirectory; //  -T, --temp-dir=DIR          create temporary files in directory DIR
  bool                                  m_findSimilarBaseFile; //  -y, --fuzzy                 find similar file for basis if no dest file
  QString                               m_compareReceivedFilesToThisDir; //      --compare-dest=DIR      also compare received files relative to DIR
  QString                               m_includeCopiesOfUnchangedFromThisDir; //      --copy-dest=DIR         ... and include copies of unchanged files
  QString                               m_hardLinkToFilesWhenUnchangedInThisDir; //      --link-dest=DIR         hardlink to files in DIR when unchanged
                                                            //  -z, --compress              compress file data during the transfer
  QString                               m_sourceFilesListFile; //      --files-from=FILE       read list of source-file names from FILE
  bool                                  m_protectArguments; //  -s, --protect-args          no space-splitting; wildcard chars only
  QString                               m_outgoingDaemonSocketAddress; //      --address=ADDRESS       bind address for outgoing socket to daemon
  int                                   m_alternatePortNumber; //      --port=PORT             specify double-colon alternate port number
  QString                               m_customTCPOptions; //      --sockopts=OPTIONS      specify custom TCP options
  bool                                  m_useRemoteShellBlockingIO; //      --blocking-io           use blocking I/O for the remote shell
                                                                    //      --stats                 give some file-transfer stats
                                                                    //  -8, --8-bit-output          leave high-bit chars unescaped in output
                                                                    //  -h, --human-readable        output numbers in a human-readable format
                                                                    //      --progress              show progress during transfer
                                                                    //  -P                          same as --partial --progress
  QString                               m_logFile; //      --log-file=FILE         log what we're doing to the specified FILE
  QString                               m_logFileFormat; //      --log-file-format=FMT   log updates using the specified FMT
  bool                                  m_listFilesOnly; //      --list-only             list the files instead of copying them
  int                                   m_protocolVersion; //      --protocol=NUM          force an older protocol version to be used
  QString                               m_charsetConversionSpec; //      --iconv=CONVERT_SPEC    request charset conversion of filenames
  int                                   m_checksumSeed; //      --checksum-seed=NUM     set block/file checksum seed (advanced)
  */