


# Some internally used information:
set(KI3U_DBUS_MAJOR_VERSION 0)
set(KI3U_DBUS_MINOR_VERSION 0)
set(KI3U_DBUS_RELEASE_VERSION 0)
set(KI3U_DBUS_VERSION_STRING "${KI3U_DBUS_MAJOR_VERSION}.${KI3U_DBUS_MINOR_VERSION}.${KI3U_DBUS_RELEASE_VERSION}")

# Add configure file:
configure_file( ki3u_dbus_config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/ki3u_dbus_config.h )

# Add include paths
include_directories( 
  ${CMAKE_CURRENT_BINARY_DIR}/../dbus
  ${CMAKE_CURRENT_BINARY_DIR}/../core
)

# Set "build" mode
add_definitions( -DKI3U_DBUS )

## Build the KI3U library:

# Source files to compile:
set(ki3u_dbus_SRCS 
  secretsstoreadapter.cpp
)

# Set headers that will be installed
set( ki3u_dbus_INSTHDRS
  ${CMAKE_CURRENT_BINARY_DIR}/ki3u_dbus_config.h
  secretsstoreadapter.h
)

# Add the library and link it. Also set properties.
kde4_add_library(ki3u_dbus SHARED ${ki3u_dbus_SRCS})
target_link_libraries(ki3u_dbus
  ${KDE4_KIO_LIBS}
  ki3u_core
  ki3u_models
)
set_target_properties( ki3u_dbus PROPERTIES VERSION ${KI3U_DBUS_VERSION_STRING} SOVERSION ${KI3U_DBUS_VERSION_STRING})



## Install
install( TARGETS ki3u_dbus ${INSTALL_TARGETS_DEFAULT_ARGS} )
install( FILES ${ki3u_dbus_INSTHDRS} COMPONENT development DESTINATION include/ki3u/dbus )
