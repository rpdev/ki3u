/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SECRETSSTOREADAPTER_H
#define SECRETSSTOREADAPTER_H

#include "ki3u_dbus_config.h"

#include "../core/secretsstore.h"

#include <QDBusAbstractAdaptor>

namespace ki3u
{

namespace dbus
{
  
class KI3U_DBUS_EXPORT SecretsStoreAdapter : public QDBusAbstractAdaptor
{
  
  Q_OBJECT
  Q_CLASSINFO("D-Bus Interface", "net.rpdev.ki3u.SecretsStore")
  
public:

  SecretsStoreAdapter( core::SecretsStore *parent );
  virtual ~SecretsStoreAdapter();
    
public slots:
  
  bool hasKey( const QString &key, 
               const QString &context = core::SecretsStore::DefaultContext );
  QString value( const QString &key, 
                 const QString &context = core::SecretsStore::DefaultContext );
  void setValue( const QString &key, 
                 const QString &value,
                 const QString &context = core::SecretsStore::DefaultContext );
  void removeValue( const QString &key,
                    const QString &context = core::SecretsStore::DefaultContext );
    
private:
  
  class SecretsStoreAdapterPrivate;
  SecretsStoreAdapterPrivate *d;
  
};

}

}

#endif // SECRETSSTOREADAPTER_H
