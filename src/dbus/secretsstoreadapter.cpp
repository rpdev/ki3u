/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "secretsstoreadapter.h"

namespace ki3u
{
  
namespace dbus
{

/**
 * @private
 */
  
class SecretsStoreAdapter::SecretsStoreAdapterPrivate
{
  
public:
  
  core::SecretsStore *store;
  
  SecretsStoreAdapterPrivate( SecretsStoreAdapter *adapter ) :
    q( adapter )
  {
  }
  
  virtual ~SecretsStoreAdapterPrivate()
  {
  }
  
private:
  
  SecretsStoreAdapter *q;
  
};

SecretsStoreAdapter::SecretsStoreAdapter( core::SecretsStore* parent ) : 
  QDBusAbstractAdaptor(parent),
  d( new SecretsStoreAdapterPrivate( this ) )
{
  d->store = parent;
}

SecretsStoreAdapter::~SecretsStoreAdapter()
{
 delete d;
}

bool 
SecretsStoreAdapter::hasKey( const QString& key, 
                             const QString& context )
{
  return d->store->hasKey( key, context );
}

QString 
SecretsStoreAdapter::value( const QString& key, 
                            const QString& context )
{
  return d->store->value( key, context );
}

void 
SecretsStoreAdapter::setValue( const QString& key, 
                               const QString& value, 
                               const QString& context )
{
  d->store->setValue( key, value, context );
}

void 
SecretsStoreAdapter::removeValue( const QString& key, 
                                  const QString& context )
{
  d->store->removeValue( key, context );
}

}

}