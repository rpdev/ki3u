/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FACTORIESMODEL_H
#define FACTORIESMODEL_H

#include "ki3u_models_config.h"

#include "../core/taskcontainer.h"

#include <QAbstractListModel>

namespace ki3u
{

namespace models
{

class KI3U_MODELS_EXPORT FactoriesModel : public QAbstractItemModel
{

  Q_OBJECT
  
public:
  
  typedef enum Columns
  {
    NameColumn = 0,
    DescriptionColumn,
    ColumnCount
  } Columns;
  
  FactoriesModel( core::TaskContainer *taskContainer, QObject *parent = 0 );
  virtual ~FactoriesModel();
  
  // Additional Data Access
  
  core::TaskFactoryDecorator* factory( const QModelIndex &index );
  QString factoryId( const QModelIndex &index );
  
  
  // Implementation of QAbstractItemModel interface
  
  virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  virtual QVariant headerData( int section, Qt::Orientation orientation, 
                               int role = Qt::DisplayRole) const;
  virtual int columnCount(const QModelIndex &index = QModelIndex()) const;
  virtual int rowCount(const QModelIndex &index = QModelIndex()) const;
  virtual QModelIndex parent(const QModelIndex&) const;
  virtual QModelIndex index(int row, int column, const QModelIndex& = QModelIndex()) const;
  
private:
  
  class FactoriesModelPrivate;
  FactoriesModelPrivate *d;
};

}

}

#endif // FACTORIESMODEL_H
