/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKLOGMODEL_H
#define TASKLOGMODEL_H

#include "../core/taskdecorator.h"

#include "ki3u_models_config.h"

#include <QAbstractListModel>

namespace ki3u
{

namespace models
{
  
class KI3U_MODELS_EXPORT TaskLogModel : public QAbstractItemModel
{

  Q_OBJECT 
  
public:
  
  typedef enum Columns
  {
    ColumnTime = 0,
    ColumnMessage,
    NumColumns
  } Columns;
  
  explicit TaskLogModel(QObject* parent = 0);
  virtual ~TaskLogModel();
  
  // QAbstractItemModel interface implementation
  virtual QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
  virtual int rowCount(const QModelIndex& parent = QModelIndex()) const;
  virtual QModelIndex parent(const QModelIndex&) const;
  virtual QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
  virtual QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;  
  
public slots:
  
  void setTask( core::TaskDecorator *task );
  
private:
  
  class TaskLogModelPrivate;
  TaskLogModelPrivate *d;
  
private slots:
  
  void newLogEntry();
  void logCleared();

};

}

}

#endif // TASKLOGMODEL_H
