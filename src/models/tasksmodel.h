/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKSMODEL_H
#define TASKSMODEL_H

#include "ki3u_models_config.h"

#include "../core/taskcontainer.h"

#include <QtCore/QAbstractItemModel>

namespace ki3u
{

namespace models
{

class KI3U_MODELS_EXPORT TasksModel : public QAbstractItemModel
{

  Q_OBJECT
  
public:
  
  typedef enum Columns
  {
    TypeColumn,
    NameColumn,
    StatusColumn,
    LastRunColumn,
    ColumnCount
  } Columns;
  
  explicit TasksModel( core::TaskContainer *taskContainer, 
                       QObject *parent = 0);
  virtual ~TasksModel();
  
  // Access to TaskContainer
  core::TaskDecorator* task( const QModelIndex &index ) const;
  core::TaskDecorator* task( const QString &id ) const;
  core::TaskDecorator* createTask( const QString &factoryId );
  core::TaskDecorator* createTaskFrom( const QModelIndex &index );
  void deleteTask( core::TaskDecorator *task );
  void deleteTask( const QString &taskId );
  void deleteTask( const QModelIndex &index );
    
  // Implementation of QAbstractItemModel interface
  virtual QVariant data( const QModelIndex& index, 
                         int role = Qt::DisplayRole ) const;
  virtual QVariant headerData( int section, Qt::Orientation orientation, 
                               int role = Qt::DisplayRole) const;
  virtual int columnCount( const QModelIndex &index = QModelIndex() ) const;
  virtual int rowCount( const QModelIndex &index = QModelIndex() ) const;
  virtual QModelIndex parent( const QModelIndex& ) const;
  virtual QModelIndex index( int row, int column, 
                             const QModelIndex& = QModelIndex() ) const;
  
private:
  
  class TasksModelPrivate;
  TasksModelPrivate *d;
  
private slots:
  
  void taskContainerCleared();
  void taskAdded( int index );
  void taskRemoved( int index );
  void taskDataChanged( int index );
  
};

}

}

#endif // TASKSMODEL_H
