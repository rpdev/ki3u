

# Some internally used information:
set(KI3U_MODELS_MAJOR_VERSION 0)
set(KI3U_MODELS_MINOR_VERSION 0)
set(KI3U_MODELS_RELEASE_VERSION 0)
set(KI3U_MODELS_VERSION_STRING "${KI3U_MODELS_MAJOR_VERSION}.${KI3U_MODELS_MINOR_VERSION}.${KI3U_MODELS_RELEASE_VERSION}")

# Add configure file:
configure_file( ki3u_models_config.h.cmake ${CMAKE_CURRENT_BINARY_DIR}/ki3u_models_config.h )

# Set "build" mode
add_definitions( -DKI3U_MODELS )

# Search for headers from Core module
include_directories( ${CMAKE_CURRENT_BINARY_DIR}/../core )

## Build the KI3U model library:

# Source files to compile:
set(ki3u_models_SRCS 
  factoriesmodel.cpp
  tasklogmodel.cpp
  tasksmodel.cpp
)

# Set headers that will be installed
set( ki3u_models_INSTHDRS
  ${CMAKE_CURRENT_BINARY_DIR}/ki3u_models_config.h
  factoriesmodel.h
  tasklogmodel.h
  tasksmodel.h
)

# Add the library and link it. Also set properties.
kde4_add_library(ki3u_models SHARED ${ki3u_models_SRCS})
target_link_libraries(ki3u_models
  ${KDE4_KIO_LIBS}
  ki3u_core
)
set_target_properties( ki3u_models PROPERTIES VERSION ${KI3U_MODELS_VERSION_STRING} SOVERSION ${KI3U_MODELS_VERSION_STRING})



## Install
install(TARGETS ki3u_models ${INSTALL_TARGETS_DEFAULT_ARGS})
install( FILES ${ki3u_models_INSTHDRS} COMPONENT development DESTINATION include/ki3u/models )