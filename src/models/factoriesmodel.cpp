/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "factoriesmodel.h"

namespace ki3u
{

namespace models
{

/**
 * @private
 */
class FactoriesModel::FactoriesModelPrivate
{
  
public:
  
  core::TaskContainer *taskContainer;
  
  FactoriesModelPrivate() :
    taskContainer( 0 )
  {
  }
  
  virtual ~FactoriesModelPrivate()
  {
  }
  
};

/**
 * @class FactoriesModel
 * @brief Give access to TaskFactoryDecorator objects in a TaskContainer
 * 
 * The FactoriesModel provides a model for factories that are stored
 * within a TaskContainer object.
 * 
 * @ingroup ki3u_models
 */

/**
 * @brief Constructor
 * 
 * @param taskContainer The task container which's factories to provide access
 *      to.
 * @param parent The parent object for the model
 */
FactoriesModel::FactoriesModel( core::TaskContainer* taskContainer, 
                                QObject* parent): 
  QAbstractItemModel( parent ),
  d( new FactoriesModelPrivate() )
{
  d->taskContainer = taskContainer;
}

/**
 * @brief Destructor
 */
FactoriesModel::~FactoriesModel()
{
  delete d;
}

/**
 * @brief Access task factories.
 * 
 * Returns the TaskFactoryDecorator object at the given @p index
 */
core::TaskFactoryDecorator* 
FactoriesModel::factory(const QModelIndex& index)
{
  if ( index.isValid() )
  {
    return d->taskContainer->factory()->concreteFactories().at( index.row() );
  }
  return 0;
}

/**
 * @brief Access task factory IDs
 * 
 * Returns the task factory ID of the factory at the given @p index.
 */
QString
FactoriesModel::factoryId(const QModelIndex& index)
{
  if ( index.isValid() )
  {
    return factory( index )->id();
  }
  return QString();
}

QVariant 
FactoriesModel::data(const QModelIndex& index, int role) const
{
  if ( !( index.isValid() ) )
  {
    return QVariant();
  }
  
  core::TaskFactoryDecorator *factory =
    d->taskContainer->factory()->concreteFactories().at( index.row() );
    
  switch ( role )
  {
    case Qt::DisplayRole:
      switch ( index.column() )
      {
        case NameColumn: return factory->name();
        case DescriptionColumn: return factory->description();
        default: return QString();
      }
      
    case Qt::ToolTipRole:
      return factory->comment();
    
    case Qt::DecorationRole:
      switch ( index.column() )
      {
        case NameColumn: return factory->icon();
        default: return QVariant();
      }
      
    default: return QVariant();
  }
}

QVariant 
FactoriesModel::headerData(int section, Qt::Orientation orientation, 
                           int role) const
{
  switch ( role )
  {
    case Qt::DisplayRole:
      if ( orientation == Qt::Horizontal )
      {
        switch ( section )
        {
          case NameColumn: return i18n( "Name" );
          case DescriptionColumn: return i18n( "Description" );
          default: return 
            QAbstractItemModel::headerData(section, orientation, role); 
        }
      }
    default: return QAbstractItemModel::headerData(section, orientation, role);
  }
}


int 
FactoriesModel::columnCount(const QModelIndex& index ) const
{
  Q_UNUSED( index );
  return ColumnCount;
}

int 
FactoriesModel::rowCount(const QModelIndex& index ) const
{
  if ( index.isValid() )
  {
    return 0;
  } else
  {
    return d->taskContainer->factory()->concreteFactories().count();
  }
}

QModelIndex 
FactoriesModel::parent(const QModelIndex& ) const
{
  return QModelIndex();
}

QModelIndex 
FactoriesModel::index(int row, int column, const QModelIndex& ) const
{
  return createIndex( row, column );
}

}

}
