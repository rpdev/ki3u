/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tasksmodel.h"

namespace ki3u
{
  
namespace models
{

/**
 * @private
 */
class TasksModel::TasksModelPrivate
{
  
public:
  
  core::TaskContainer *container;
  
  TasksModelPrivate()
  {
  }
  
  virtual ~TasksModelPrivate()
  {
  }
  
};

/**
 * @class TasksModel
 * @brief Model for tasks in a TaskContainer
 * 
 * The TasksModel takes a TaskContainer at creation time and displays
 * shows information about the TaskDecorator objects stored in the container.
 * 
 * @ingroup ki3u_models
 */

/**
 * @brief Constructor
 * 
 * @param taskContainer The container which's tasks to give access to
 * @param parent The parent object for the model
 */
TasksModel::TasksModel( core::TaskContainer* taskContainer, 
                        QObject* parent ) : 
  QAbstractItemModel( parent ),
  d( new TasksModelPrivate() )
{
  d->container = taskContainer;
  connect( taskContainer, SIGNAL(taskListEmptied()),
           this, SLOT(taskContainerCleared()) );
  connect( taskContainer, SIGNAL(taskRemoved(int)),
           this, SLOT(taskRemoved(int)) );
  connect( taskContainer, SIGNAL(taskAppended(int)),
           this, SLOT(taskAdded(int)) );
  connect( taskContainer, SIGNAL(taskDataChanged(int)),
           this, SLOT(taskDataChanged(int)) );
}

/**
 * @brief Destructor
 */
TasksModel::~TasksModel()
{
  delete d;
}

/**
 * @brief Access single task object
 * 
 * Returns the TaskDecorator object specified by the given @p index
 */
core::TaskDecorator* 
TasksModel::task(const QModelIndex& index) const
{
  if ( index.row() >= 0 && index.row() < d->container->tasks().size() )
  {
    return d->container->tasks().at( index.row() );
  }
  return 0;
}

/**
 * @brief Access single task object
 * 
 * Returns the task object with the given @p id or 0, if
 * no such task exists.
 */
core::TaskDecorator* 
TasksModel::task(const QString& id) const
{
  return d->container->task( id );
}

/**
 * @brief Create a new task.
 * 
 * Creates a new task object and adds it to the underlying TaskContainer object.
 * This will use the factory having the given @p factoryId.
 * 
 * @returns The created task object
 */
core::TaskDecorator *
TasksModel::createTask(const QString& factoryId)
{
  return d->container->createTask( factoryId );
}

/**
 * @brief Create new task from existing one.
 * 
 * Creates a new task object by cloning an existing one.
 */
core::TaskDecorator* 
TasksModel::createTaskFrom(const QModelIndex& index)
{
  return d->container->createTaskFrom( task( index ) );
}


/**
 * @brief Delete a task
 * 
 * Deletes the given @p task object from the underlying TaskContainer
 * object.
 * 
 * @note This will have effect if and only if the task is indeed stored
 *      inside the container.
 */
void 
TasksModel::deleteTask(core::TaskDecorator* task)
{
  d->container->removeTask( task );
}

/**
 * @brief Delete a task
 * 
 * Deletes the task with the given @p taskId from the TaskContainer
 * object.
 */
void 
TasksModel::deleteTask(const QString& taskId)
{
  d->container->removeTask( taskId );
}

/**
 * @brief Delete a task
 * 
 * Deletes the task at the specified @p index from the TaskContainer
 * object.
 */
void 
TasksModel::deleteTask(const QModelIndex& index)
{
  d->container->removeTask( task( index ) );
}

QVariant 
TasksModel::data(const QModelIndex& index, int role) const
{
  core::TaskDecorator *task = d->container->tasks().at( index.row() );
  core::TaskFactoryDecorator *factory = 
    d->container->factory()->taskFactory( task->factoryId() );
  
  switch( role )
  {
    case Qt::DisplayRole:
      switch ( index.column() )
      {
        case TypeColumn: return QVariant();
        case NameColumn: return task->name();
        case StatusColumn:
          switch ( task->state() )
          {
            case core::TaskDecorator::Idle: return "Idle";
            case core::TaskDecorator::Initialized: return "Initialized";
            case core::TaskDecorator::Running: return "Running";
            case core::TaskDecorator::Finishing: return "Finishing";
            default: return QVariant();
          }
        case LastRunColumn: return task->lastSuccessfulRunDateTime().toString();
        default: return QVariant();
      }
      
    case Qt::ToolTipRole:
      switch ( index.column() )
      {
        case TypeColumn: return ( factory == 0 ? QString() : factory->name() );
        default: return task->comment();
      }
      
    case Qt::DecorationRole:
      switch ( index.column() )
      {
        case TypeColumn: 
          if ( factory )
          {
            return factory->icon();
          } else
          {
            return KIcon ("emblem-important");
          }
          
        default:
          return QIcon();
      }
      
    case Qt::UserRole:
      switch ( index.column() )
      {
        case StatusColumn:
          if ( task->state() != core::TaskDecorator::Idle )
          {
            return task->progress();
          } else
          {
            return QVariant();
          }
          
        default:
          return QVariant();
      }
        
    
    default: return QVariant();
  }
}

QVariant 
TasksModel::headerData( int section, Qt::Orientation orientation, 
                        int role ) const
{
  switch( role )
  {
    case Qt::DisplayRole:
      if ( orientation == Qt::Horizontal ) 
      {
        switch( section )
        {
          case TypeColumn: return i18n( "Type" );
          case NameColumn: return i18n( "Name" );
          case StatusColumn: return i18n( "Status" );
          case LastRunColumn: return i18n( "Last Run" );
          default: 
            return QAbstractItemModel::headerData( section, orientation, role );
        }
      }
    default: return QAbstractItemModel::headerData(section, orientation, role);
  }
}


int 
TasksModel::columnCount( const QModelIndex& index ) const
{
  Q_UNUSED( index );
  return ColumnCount;
}

int 
TasksModel::rowCount( const QModelIndex& index ) const
{
  if ( index.isValid() )
  {
    return 0;
  } else
  {
    return d->container->tasks().count();
  }
}

QModelIndex 
TasksModel::parent( const QModelIndex& ) const
{
  return QModelIndex();
}

QModelIndex 
TasksModel::index( int row, int column, const QModelIndex& ) const
{
  return createIndex( row, column );
}

void 
TasksModel::taskContainerCleared()
{
  beginResetModel();
  endResetModel();
}

void 
TasksModel::taskAdded(int index)
{
  beginInsertRows( QModelIndex(), index, index );
  endInsertRows();
}

void 
TasksModel::taskRemoved(int index)
{
  beginRemoveRows( QModelIndex(), index, index );
  endRemoveRows();
}

void 
TasksModel::taskDataChanged( int index )
{
  dataChanged( this->index( index, 0 ), 
               this->index( index, this->columnCount() ) );
}



}

}

