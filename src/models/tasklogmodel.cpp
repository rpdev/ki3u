/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "tasklogmodel.h"

#include <KIcon>
#include <KLocale>

namespace ki3u
{

namespace models
{

/**
 * @private
 */

class TaskLogModel::TaskLogModelPrivate
{
public:
  
  core::TaskDecorator *task;
  
  TaskLogModelPrivate( TaskLogModel *model ) :
    task( 0 ),
    q( model )
  {
  }
  
  virtual ~TaskLogModelPrivate()
  {
  }
  
private:
  
  TaskLogModel *q;
  
};

TaskLogModel::TaskLogModel( QObject* parent ) : 
  QAbstractItemModel( parent ),
  d( new TaskLogModelPrivate( this ) )
  {
}

TaskLogModel::~TaskLogModel()
{
  delete d;
}

QVariant 
TaskLogModel::data(const QModelIndex& index, int role) const
{
  if ( !( d->task ) )
  {
    return QVariant();
  }
  
  core::TaskDecorator::LogEntry entry = d->task->log().at( index.row() );
  
  switch ( role )
  {
    case Qt::DisplayRole:
      switch ( index.column() )
      {
        case ColumnTime: return entry.time.toString();
        case ColumnMessage: return entry.message;
        default: return QVariant();
      }
      
    case Qt::DecorationRole:
      switch ( index.column() )
      {
        case ColumnTime:
          switch ( entry.type )
          {
            case core::Task::Error: return KIcon( "emblem-important");
            default: return QVariant();
          }
          
        default: return QVariant();
      }
      
    default: QVariant();
  }
  return QVariant();
}

int 
TaskLogModel::columnCount( const QModelIndex& parent ) const
{
  if ( parent.isValid() )
  {
    return 0;
  } else
  {
    return NumColumns;
  }
}

int TaskLogModel::rowCount(const QModelIndex& parent) const
{
  if ( parent.isValid() || d->task == 0 )
  {
    return 0;
  } else
  {
    return d->task->log().size();
  }
}

QModelIndex 
TaskLogModel::parent(const QModelIndex& ) const
{
  return QModelIndex();
}

QModelIndex 
TaskLogModel::index(int row, int column, const QModelIndex& parent) const
{
  if ( parent.isValid() )
  {
    return QModelIndex();
  } else
  {
    return createIndex( row, column );
  }
}

QVariant TaskLogModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if ( orientation == Qt::Horizontal )
  {
    if ( role == Qt::DisplayRole )
    {
      switch ( section )
      {
        case ColumnTime: return i18n( "Time" );
        case ColumnMessage: return i18n( "Message" );
        default: return QVariant();
      }
    }
  }
  return QAbstractItemModel::headerData(section, orientation, role);
}

void 
TaskLogModel::newLogEntry( )
{
  if ( d->task )
  {
    beginInsertRows( QModelIndex(), 
                     d->task->log().size(), 
                     d->task->log().size() );
    endInsertRows();
  }
}

void 
TaskLogModel::logCleared()
{
  beginResetModel();
  endResetModel();
}


void 
TaskLogModel::setTask(core::TaskDecorator* task)
{
  if ( d->task )
  {
    disconnect( d->task, SIGNAL(debugMessage(MessageType,QString)),
                this, SLOT(newLogEntry()) );
    disconnect( d->task, SIGNAL(logCleared()), 
                this, SLOT(logCleared()) );
  }
  beginResetModel();
  d->task = task;
  endResetModel();
  if ( task )
  {
    connect( d->task, SIGNAL(debugMessage(MessageType,QString)), 
             this, SLOT(newLogEntry()) );
    connect( d->task, SIGNAL(logCleared()), 
                this, SLOT(logCleared()) );
  }
}

}

}