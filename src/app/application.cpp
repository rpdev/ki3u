/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "application.h"

#include "../gui/mainwindow.h"

namespace ki3u
{

namespace app
{

class Application::ApplicationPrivate
{
  
public:
  
  gui::MainWindow *mainWindow;
  
  ApplicationPrivate( Application *application ) :
    q( application )
  {
  }
  
  virtual ~ApplicationPrivate()
  {
  }
  
  void 
  initialize()
  {
    initializeMainWindow();
  }
  
private:
  
  Application *q;
  
  void 
  initializeMainWindow()
  {
    mainWindow = new gui::MainWindow();
  }
  
};

Application::Application( bool GUIenabled, bool configUnique ) : 
  KUniqueApplication(GUIenabled, configUnique),
  d( new ApplicationPrivate( this ) )
{
  d->initialize();
}

Application::~Application()
{
  delete d;
}

int 
Application::newInstance()
{
  return KUniqueApplication::newInstance();
}


}

}