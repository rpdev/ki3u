/* ============================================================
*
* This file is a part of the KI3U project
*
* Copyright (C) 2009 by RPdev
*
*
* This program is free software; you can redistribute it
* and/or modify it under the terms of the GNU General
* Public License as published by the Free Software Foundation;
* either version 3, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* ============================================================ */

#include "application.h"

#include "ki3u_app_config.h"

#include <KAboutData>
#include <KCmdLineArgs>
#include <KMessageBox>

#include <QApplication>


int main (int argc, char *argv[])
{
    KAboutData aboutData(
                         // The program name used internally.
                         "ki3u",
                         // The message catalog name
                         // If null, program name is used instead.
                         0,
                         // A displayable program name string.
                         ki18n( "KI3U" ),
                         // The program version string.
                         KI3U_APP_VERSION_STRING,
                         // Short description of what the app does.
                         ki18n( "Backup and Synchronization" ),
                         // The license this code is released under
                         KAboutData::License_GPL_V3,
                         // Copyright Statement
                         ki18n( "(c) 2009, 2010, 2011 The RPdev Team" ),
                         // Optional text shown in the About box.
                         // Can contain any information desired.
                         ki18n( "An easily extensible front-end intended for backing up and synchronizing your data." ),
                         // The program homepage string.
                         "http://www.rpdev.net/home/ki3u",
                         // The bug report email address
                         "feedback@rpdev.net");
    
    aboutData.addAuthor( ki18n("Martin Höher"), 
                         ki18n("Programmer"),
			 "martin@rpdev.net", 
                         "http://www.rpdev.net/home/martin");

    KCmdLineArgs::init( argc, argv, &aboutData );
    ki3u::app::Application app(true, true);
    
    int result = app.exec();
    return result;
}
