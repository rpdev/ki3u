/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "task.h"

namespace ki3u
{
  
namespace core
{

/**
  * @class Task
  * @brief Defines the interface for runable tasks.
  * 
  * A task is a chunk of work to do in the program, i.e. it can copy
  * files or start external programs to do so.
  * 
  * The interface that is required is rather minimal: It introduces all
  * methods a task must implement plus some additional signals that
  * the task should use to indicate certain events during its
  * processing.
  * 
  * Tracking of states and other meta data (e.g. user settable names and
  * descriptions) are handled via the TaskDecorator class; thus, each task
  * can concentrate on its own work.
  * 
  * @ingroup ki3u_core 
  */

/**
 * @fn Task::Task( QObject *parent )
 * @brief Constructor
 */

/**
  * @fn Task::~Task()
  * @brief Destructor.
  */

/**
  * @fn Task::setup()
  * @brief Prepare the task for being run.
  * 
  * The setup method is called before the task is actually run. This allows
  * it to do some preparation. Especially, the task should check whether
  * running actually makes sense (and maybe terminate otherwise).
  * This is crucial when multiple tasks are executed as a group: Here,
  * first all tasks should initialize ok before the first task is started.
  * 
  * @returns True if setting up the process was successful, otherwise false.
  */

/**
  * @fn Task::run()
  * @brief Starts executing the task.
  * 
  * This actually starts the task.
  * Each task does the following steps when run is called (these might
  * be implemented using signals and slots, thus, run returns usually immediately):
  * 
  * @li The task initializes all data it needs (this is not to mix up with the
  * precondition checking which is done in the setup method). If this initialization
  * was successfull, the task emits the runStarted signal with a value of true,
  * otherwise, it emits runStarted with false.
  * 
  * @li When the initialization was successfull, the task is now in a "running" state.
  * Depending on the expected amount of time this phase might need, the task emits
  * runProgress signals from time to time to inform observers about its current progress.
  * The value passed should be in the range [0..100]. Other values are allowed and will
  * be interpreted as "unknown" progress values.
  * 
  * @li As soon as the running phase is over and the task emits the runFinished signal,
  * passing again a boolean value indicating whether the run was successfull or not.
  * 
  * @returns True if starting the process was successful, otherwise false.
  */

/**
  * @fn Task::teardown()
  * @brief Clear up after a run.
  * 
  * This method is called in the tear-down phase after the task has been run.
  * The task can use it to do some cleaning up (e.g. a task to mount a network share
  * might unmount it in this method).
  * 
  * @returns True if clean up was successful, otherwise false.
  */

/**
  * @fn Task::clone()
  * @brief Clones the task object.
  * 
  * This method returns a clone of the task object. The clone should use exactly the
  * same settings as the original task.
  * 
  * @returns A clone of the Task or 0, of cloning is not supported.
  */

/**
  * @fn Task::save( KConfigGroup &config )
  * @brief Saves the tasks settings.
  * 
  * Lets the Task save its settings to the @p config group. The tasks writes all
  * its data using KDE's configuration system, so that it can be restored when 
  * the application is restarted.
  */

/**
  * @fn Task::restore(  KConfigGroup &config  )
  * @brief Restores a task.
  * 
  * Let the task load its data from the given @p config group.
  */

/**
  * @fn Task::configure()
  * @brief Configure the task.
  * 
  * This us used to configure the task.
  * Concrete tasks usually provide own dialogs to let the user set individual tasks up.
  */

/**
  * @fn Task::runStarted(bool ok)
  * @brief Indicates, that the task is initialized and entered "running" mode.
  * 
  * This signal is emitted after calling run: It indicates, that the task has finished initialization
  * and now either is in running mode or will shut down immediately.
  * 
  * @param ok True if initialization was successful and the task is now running or false otherwise.
  */

/**
  * @fn Task::runProgress( int progress )
  * @brief Indicates progress during run.
  * 
  * This signal can be emitted from time to time to inform observers of the current progress.
  * The value of progress is in the range [0..100]; all other values will be treated as
  * "unknown" progress or (depending on the purpose the value is used for) zero.
  */

/**
  * @fn Task::runFinished( bool ok )
  * @brief The task has finished finalizing its run.
  * 
  * This signal is emitted when the task has left running phase and cleared up.
  * 
  * @param ok True if the run was successful and clearing up resulted in no problems,
  * otherwise false.
  * 
  * @note This event is emitted as a result of a finished call to run; it has nothing to
  * do with the teardown() method, which returns immediately and thus does not use signals.
  */

}
  
}