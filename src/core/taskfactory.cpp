/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskfactory.h"

namespace ki3u
{
  
namespace core
{
    
/**
 * @class TaskFactory
 * @brief Creates Task objects.
 * 
 * The Factory class is used to create new Task objects at runtime.
 * Task implementors provide a Factory object, which is created dynamically
 * at runtime and then used for virtual construction of concrete tasks.
 * 
 * @ingroup ki3u_core
 * 
 */

/**
 * @fn Factory::createTask
 * @brief Create a new Task object
 * 
 * The method create a new Task object dynamically at
 * runtime and returns it.
 */

/**
 * @brief Constructor
 */
TaskFactory::TaskFactory( QObject* parent, 
                          const QVariantList &args ): 
  QObject( parent )
{
  Q_UNUSED( args );
}

/**
 * @brief Destructor
 */
TaskFactory::~TaskFactory()
{
}
    
}
  
}