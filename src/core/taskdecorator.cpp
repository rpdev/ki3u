/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskcontainer.h"
#include "taskdecorator.h"

#include <KConfigGroup>
#include <KDebug>

#include <QtGlobal>

namespace ki3u
{
  
namespace core
{

/**
 * @private
 * @brief Private class members for TaskDecorator.
 */
class TaskDecorator::TaskDecoratorPrivate
{
  
public:
  
  static const QString ConfigKeyFactoryId;
  static const QString ConfigKeyName;
  static const QString ConfigKeyLastSuccessfulRunDateTime;
  static const QString ConfigKeyComment;
  static const QString ConfigKeyChildTasks;
  static const QString ConfigKeyExecuteChildrenInParallel;
  static const QString ConfigKeyIgnoreChildErrors;
  
  static const QString ConfigGroupTaskConfig;
  
  Task *task;
  QString id;
  QString factoryId;
  QDateTime lastSuccessfulRunDateTime;
  
  QString name;
  QString comment;
  
  QStringList childTasks;
  bool executeChildrenInParallel;
  bool ignoreChildErrors;
  
  TaskState             state;
  int                   taskProgress;
  QList< LogEntry >     log;
  bool                  taskLocked;
  
  QList< TaskDecorator* > childTaskObjects;
  QList< TaskDecorator* > finishedTaskObjects;
  bool                    decoratedRunSuccess;
  bool                    childHadError;
  
  TaskDecoratorPrivate( TaskDecorator *taskDecorator ) :
    task( 0 ),
    id( QString() ),
    factoryId( QString() ),
    lastSuccessfulRunDateTime( QDateTime() ),
    name( QString() ),
    comment( QString() ),
    childTasks( QStringList() ),
    executeChildrenInParallel( false ),
    ignoreChildErrors( false ),
    state( Idle ),
    taskProgress( -1 ),
    log( QList< LogEntry >() ),
    taskLocked( false ),
    childTaskObjects( QList< TaskDecorator* >() ),
    finishedTaskObjects( QList< TaskDecorator* >() ),
    decoratedRunSuccess( false ),
    childHadError( false ),
    q( taskDecorator )
  {
  }
  
  virtual ~TaskDecoratorPrivate()
  {
  }
  
  TaskContainer* container() const
  {
    return dynamic_cast< TaskContainer* >( q->parent() );
  }
  
  void doEmitRunFinished( bool status ) 
  {
    if ( status )
    {
      lastSuccessfulRunDateTime = QDateTime::currentDateTime();
    }
    emit q->runFinished( status );
  }
  
private:
  
  TaskDecorator *q;
  
};

const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyFactoryId = "factoryId";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyName = "userName";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyLastSuccessfulRunDateTime = "lastSuccessfulRunDateTime";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyComment = "comment";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyChildTasks = "childTasks";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyExecuteChildrenInParallel = "executeChildrenInParallel";
const QString TaskDecorator::TaskDecoratorPrivate::ConfigKeyIgnoreChildErrors = "ignoreChildErrors";

const QString TaskDecorator::TaskDecoratorPrivate::ConfigGroupTaskConfig = "TaskConfiguration";


/**
 * @class TaskDecorator
 * @brief Wraps Task objects to provide further services.
 * 
 * TaskDecorators wrap Task objects to allow adding new services without 
 * overloading the Task interface itself. It should be easy to add new Task
 * types, thus, blowing up that interface would be undesireable.
 * 
 * Instead, the Task interface is kept minimal, and most "additional" services
 * are implemented via the TaskDecorator. This includes not only handling of
 * meta data (e.g. a name and description for each task a user can set) but also
 * tracking of the current state of the task.
 * 
 * Additionally, TaskDecorator makes sure, each task is started only once,
 * i.e. no Task is started again while it's still running. This makes
 * implementing new Task objects easy, as thy can rely on some contracts
 * and thus their implementation does not need to implement own
 * precautions for these cases.
 * 
 * @ingroup ki3u_core
 */

/**
 * @enum TaskDecorator::TaskState
 * 
 * The State type indicates the current phase of execution, in which a Task
 * can be. The state is actually calculated by the TaskDecorator by observing
 * signals from the underlying Task before re-emitting them.
 *
 * \var Idle
 * The Task is doing nothing (this is the default state)
 * 
 * @var Initialized
 * The setup() method has been successfully executed.
 * 
 * @var Starting 
 * The run() method has just been called.
 * 
 * @var Running
 * The task is currently running.
 * 
 * @var Finishing 
 * The Task has stopped processing (or setup() failed).
 * 
 * @note Getting the value correct depends on the decorated Task object to
 * use the signals as proposed in the protocol. Otherwise,
 * the decorator will certainly not be able to get the state right, resulting
 * in calling layers to not be able to handle things correctly.
 */

/**
 * @fn dataChanged()
 * @brief Some data has changed
 * 
 * This signal is emitted when some of the tasks data has been changed.
 * It can be used by observers to display these changes as soon as they happen
 * (e.g. in views).
 */

/**
 * @brief Constructor
 */
TaskDecorator::TaskDecorator( TaskContainer* parent ): 
  Task( parent ),
  d( new TaskDecoratorPrivate( this ) )
{
}

/**
 * @brief Destructor
 */
TaskDecorator::~TaskDecorator()
{
  if ( d->task )
  {
    delete d->task;
  }
  delete d;
}

/**
 * @brief Sets up the task for being run.
 * 
 * This will setup() the decorated task. If the decorated Task returns
 * true, the object will change its state() to 
 * {@link TaskState::Initialized Initialized},
 * otherwise, it will remain in {@link TaskState::Idle Idle} state.
 * 
 * @returns The return value of the decorated Task's setup() method or
 *              false.
 */
bool 
TaskDecorator::setup()
{
  if ( d->task )
  {
    bool result = d->task->setup();
    d->state = result ? Initialized : Idle;
    d->taskProgress = -1;
    d->log.clear();
    emit logCleared();
    emit dataChanged();
    
    // Build the list of child tasks
    d->childTaskObjects.clear();
    d->finishedTaskObjects.clear();
    d->decoratedRunSuccess = false;
    d->childHadError = false;
    foreach ( QString childId, d->childTasks )
    {
      TaskDecorator *child = d->container()->task( childId );
      if ( child )
      {
        d->childTaskObjects.append( child );
        decoratedDebugMessage( Task::Debug, 
                               i18n( "Scheduling task %1 for execution" )
                                .arg( child->name() ) );
      } else 
      {
        decoratedDebugMessage( Task::Error, 
                                i18n( "Unable to find child task with ID %1" )
                                  .arg( childId ) );
        if ( !( d->ignoreChildErrors ) )
        {
          return false;
        }
      }
    }
    
    return result;
  }
  return false;
}

/**
 * @brief Start the task,
 * 
 * Starts running the task. This will call the Task::run() of the decorated Task
 * if and only if it has been initialized property (i.e. the TaskDecorator's
 * state is Initialized) and returns the result value of the decorated Task's
 * run method.
 * 
 * @return The result value of the decorated Task's run method or false
 *         if the task is not initialized.
 */
bool 
TaskDecorator::run()
{
  if ( d->task && d->state == Initialized )
  {
    d->state = Starting;
    emit dataChanged();
    return d->task->run();
  }
  return false;
}

/**
 * @brief Finalizes the task.
 * 
 * This will call the the decorated Task object's teardown() method.
 * 
 * @returns Returns the result value of the decorated Task's teardown method
 *              or false if the underlying task is not set.
 */
bool 
TaskDecorator::teardown()
{
  d->state = Idle;
  d->taskProgress = -1;
  emit dataChanged();
  if ( d->task )
  {
    return d->task->teardown();
  }
  d->childTaskObjects.clear();
  d->finishedTaskObjects.clear();
  return false;
}

/**
 * @brief Clones the task
 * 
 * Creates a clone of the task. That means: It will return a TaskDecorator with
 * the same properties as the original decorator wrapping a clone of the
 * underlying Task object.
 * 
 * @note Calling the clone method of a TaskDecorator will always return
 * a new object (and never a null pointer as the Task object is allowed
 * to return). However, the newly created taskdecorator might wrap a null
 * pointer, of the Task returned one when its clone() method has been called.
 */
Task* 
TaskDecorator::clone()
{
  TaskDecorator *result = 
    new TaskDecorator( dynamic_cast< TaskContainer* >( parent() ) );
  if ( d->task )
  {
    result->setTask( d->task->clone() );
    result->d->name = d->name;
    result->d->comment = d->comment;
    result->d->childTasks = d->childTasks;
    result->d->executeChildrenInParallel = d->executeChildrenInParallel;
    result->d->ignoreChildErrors = d->ignoreChildErrors;
  }
  result->d->id = dynamic_cast< TaskContainer* >( parent() )->generateId();
  result->d->factoryId = d->factoryId;
  return result;
}

void 
TaskDecorator::save( KConfigGroup &config )
{
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyFactoryId, d->factoryId );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyName, d->name );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyLastSuccessfulRunDateTime, d->lastSuccessfulRunDateTime );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyComment, d->comment );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyChildTasks, d->childTasks );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyExecuteChildrenInParallel, d->executeChildrenInParallel );
  config.writeEntry( TaskDecoratorPrivate::ConfigKeyIgnoreChildErrors, d->ignoreChildErrors );
  if ( d->task )
  {
    KConfigGroup internalGroup = 
      config.group( TaskDecoratorPrivate::ConfigGroupTaskConfig );
    d->task->save( internalGroup );
  }
}

void 
TaskDecorator::restore( KConfigGroup &config )
{
  d->factoryId = config.readEntry( TaskDecoratorPrivate::ConfigKeyFactoryId );
  d->name = config.readEntry( TaskDecoratorPrivate::ConfigKeyName );
  d->lastSuccessfulRunDateTime = config.readEntry< QDateTime >( TaskDecoratorPrivate::ConfigKeyLastSuccessfulRunDateTime, QDateTime() );
  d->comment = config.readEntry( TaskDecoratorPrivate::ConfigKeyComment );
  d->childTasks = config.readEntry( TaskDecoratorPrivate::ConfigKeyChildTasks, QStringList() );
  d->executeChildrenInParallel = config.readEntry< bool >( TaskDecoratorPrivate::ConfigKeyExecuteChildrenInParallel, false );
  d->ignoreChildErrors = config.readEntry< bool >( TaskDecoratorPrivate::ConfigKeyIgnoreChildErrors, false );
  d->taskProgress = -1;
  if ( d->task ) 
  {
    delete d->task;
    d->task = 0;
  }
  Task *tmp = dynamic_cast< TaskContainer* >( parent() )->factory()->createTask( 
    d->factoryId );
  if ( tmp )
  {
    setTask( tmp );
    KConfigGroup internalGroup =
      config.group( TaskDecoratorPrivate::ConfigGroupTaskConfig );
    d->task->restore( internalGroup );
  }
  emit dataChanged();
}

void 
TaskDecorator::configure()
{
  if ( d->task )
  {
    d->task->configure();
  }
}

/**
 * @brief The ID of the task.
 * 
 * Returns the ID of the TaskDecorator. The id is unique inside a
 * TaskCollector. It is automatically generated when a new TaskDecorator
 * is created.
 */
QString 
TaskDecorator::id() const
{
  return d->id;
}

/**
 * @brief The ID of the factory that has created the underlying Task.
 * 
 * Returns the ID of the factory that has created the underlying
 * Task object.
 */
QString 
TaskDecorator::factoryId() const
{
  return d->factoryId;
}

/**
 * @brief Time of last successful run
 * 
 * Returns the time when the task finished the last time successfully.
 */
QDateTime 
TaskDecorator::lastSuccessfulRunDateTime() const
{
  return d->lastSuccessfulRunDateTime;
}


/**
 * @brief User visible name of the task
 */
QString 
TaskDecorator::name() const
{
  return d->name;
}

/**
 * @brief Sets user visible name for task
 */
void 
TaskDecorator::setName(const QString& name)
{
  d->name = name;
  emit dataChanged();
}

/**
 * @brief Comment describing the task.
 */
QString 
TaskDecorator::comment() const
{
  return d->comment;
}

/**
 * @brief Sets a comment for the task.
 */
void 
TaskDecorator::setComment(const QString& comment)
{
  d->comment = comment;
  emit dataChanged();
}

/**
 * @brief Child tasks to execute when this task is executed
 * 
 * Returns the list with IDs of child tasks that should be executed, when
 * this task is executed.
 */
const QStringList& 
TaskDecorator::childTasks() const
{
  return d->childTasks;
}

/**
 * @brief Set child tasks
 * 
 * @sa childTasks
 */
void 
TaskDecorator::setChildTasks(const QStringList& childTasks)
{
  d->childTasks = childTasks;
}

/**
 * @brief Shall child tasks be executed in parallel or one after each other
 * 
 * Returns, whether child tasks should be executed in parallel or
 * one after each other. By default, only one child
 * task is executed at once. Letting multiple child tasks to be executed
 * at once can speed up the overall process, however, when the child
 * tasks all use the same resource (e.g. network bandwidth) starting
 * all at once might decrease performance or render the system
 * unusable (e.g. other applications will need to wait much longer
 * when they too need network access).
 */
bool 
TaskDecorator::executeChildrenInParallel() const
{
  return d->executeChildrenInParallel;
}

/**
 * @brief Set, whether child tasks should be executed in parallel
 * 
 * @sa executeChildrenInParallel
 */
void 
TaskDecorator::setExecuteChildrenInParallel(bool executeInParallel)
{
  d->executeChildrenInParallel = executeInParallel;
}

/**
 * @brief Shall errors in child tasks be ignored?
 * 
 * When this is set to true, then errors in child tasks will be ignored. The
 * effect this has depends on executeChildrenInParallel.
 * 
 * If children are executed in parallel, this simply will ignore errors
 * regarding the return values of own methods or signals (i.e.
 * this task will report all success when it finishes even when
 * a child task actually could not complete without errors).
 * 
 * When executing children one after each other, this value determines,
 * if execution should continue after a child had an error. Consider
 * the children A, B and C of a task. If errors are ignored,
 * B and C are started even after task A had an error. If errors are
 * not ignored, the tasks B and C will not be started (independent on when
 * in the execution of A the error occurred).
 */
bool 
TaskDecorator::ignoreChildErrors() const
{
  return d->ignoreChildErrors;
}

/**
 * @brief Set, whether errors in child tasks should be ignored
 * 
 * @sa ignoreChildErrors
 */
void 
TaskDecorator::setIgnoreChildErrors(bool ignoreChildErrors)
{
  d->ignoreChildErrors = ignoreChildErrors;
}

/**
 * @brief Sets the task object.
 * 
 * @note Should be called only once, will assert if a task is already set.
 */
void 
TaskDecorator::setTask(Task* task)
{
  // This should be called at most once, so check, whether we already decorate
  // another Task
  Q_ASSERT( task != 0 );
  d->task = task;
  connect( task, SIGNAL(runStarted(bool)), 
           this, SLOT(decoratedStarted(bool)) );
  connect( task, SIGNAL(runProgress(int)), 
           this, SLOT(decoratedProgress(int)) );
  connect( task, SIGNAL(runFinished(bool)), 
           this, SLOT(decoratedFinished(bool)) );
  connect( task, SIGNAL(debugMessage(MessageType,QString)),
           this, SLOT(decoratedDebugMessage(MessageType,QString)) );
}

void 
TaskDecorator::setId(const QString& id)
{
  d->id = id;
  emit dataChanged();
}

/**
 * @brief The current state of the task.
 * 
 * The state indicates, in which phase of the actual run a task is.
 * 
 * The state is calculated from observing the underlying Task
 * object. It's signals are captured and evaluated, before being re-emitted.
 */
TaskDecorator::TaskState 
TaskDecorator::state() const
{
  return d->state;
}

/**
 * @brief The tasks progress
 * 
 * Returns the progress if the task.
 * Usually, this returns -1 if the task is not running currently. Otherwise,
 * it returns the last reported progress of the decorated task in a normalized
 * form, i.e. the value is either in the interval 0..100 or -1, if the progress
 * if unknown.
 */
int 
TaskDecorator::progress() const
{
  int p;
  if ( d->taskProgress < 0 || d->taskProgress > 100 )
  {
    p = -1;
  } else
  {
    p = d->taskProgress;
  }
  if ( d->state == Finishing )
  {
    p = 100;
  }
  p += d->finishedTaskObjects.size() * 100;
  foreach ( TaskDecorator *task, d->childTaskObjects )
  {
    int tp = task->progress();
    p += tp > 0 ? tp : 0;
  }
  return ( p * 100 ) / 
         ( 100 + 
         ( d->childTaskObjects.size() + d->finishedTaskObjects.size() ) * 100 );
}

/**
 * @brief The task's log
 * 
 * Returns the log of the task. The log contains all debug messages, the 
 * decorated task has emitted.
 */
const QList< TaskDecorator::LogEntry >& 
TaskDecorator::log() const
{
  return d->log;
}

/**
 * @brief Is the task locked?
 * 
 * Returns true, if the task has been locked.
 * 
 * @note Task locking is advisory. Actually, upper layers are responsible for
 * obeying the locks (or don't).
 */
bool 
TaskDecorator::isLocked() const
{
  return d->taskLocked;
}

/**
 * @brief Mark this and all child tasks as "in use"
 * 
 * This will set a flag on this task and all child tasks (both direct and
 * indirect ones). The main purpose is to let upper framework layers know,
 * a task is currently in use and should not be used, deleted or changed.
 * 
 * @returns True if locking the task and all children was successful, otherwise
 *          false.
 * 
 * @note If a child task cannot be found (remember that child tasks are referred
 *       to by their ID), this method can still succeed. Checking if all
 *       children are present must be done in each concrete situation.
 * 
 * @note The locking is advisory. Actually, upper framework layers could
 *       use the tasks anyway (e.g. start it once more, change properties or
 *       even delete it). However, the result then might be unpredictable
 *       (starting from just stupid behavior up to crashing due to 
 *       invalid memory access).
 */
bool 
TaskDecorator::lock()
{
  if ( d->taskLocked )
  {
    return false;
  }
  d->taskLocked = true;
  
  // We need to lock down all children too
  // Note: This all happens in one (main) thread
  QList< TaskDecorator* > locked;
  foreach ( QString id, d->childTasks )
  {
    TaskDecorator *task = d->container()->task( id );
    if ( task )
    {
      if ( task->lock() )
      {
        locked.append( task );
      } else
      {
        foreach ( TaskDecorator* lockedTask, locked )
        {
          lockedTask->unlock();
        }
        return false;
      }
    }
  }
  return true;
}

/**
 * @brief Unlocks the task
 * 
 * Removes the lock from this task and all child tasks. Note, that this
 * will not check the actual value of the flag but just oberride it.
 */
void 
TaskDecorator::unlock()
{
  foreach ( QString id, d->childTasks )
  {
    TaskDecorator *task = d->container()->task( id );
    if ( task )
    {
      task->unlock();
    }
  }
  d->taskLocked = false;
}

void 
TaskDecorator::setFactoryId(const QString& id)
{
  d->factoryId = id;
  emit dataChanged();
}

void 
TaskDecorator::decoratedStarted(bool ok)
{
  d->state = ok ? Running : Finishing;
  emit dataChanged();
  emit runStarted( ok );
}

void 
TaskDecorator::decoratedProgress(int progress)
{
  d->taskProgress = progress;
  emit runProgress( progress );
  emit dataChanged();
}

void 
TaskDecorator::decoratedFinished(bool ok)
{
  d->state = Finishing;
  emit dataChanged();
  if ( ok && d->childTaskObjects.length() > 0 )
  {
    connect( d->container()->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)),
             this, SLOT(taskRunnerFinishedTask(TaskDecorator*,bool)) );
    
    d->decoratedRunSuccess = ok;
    if ( d->executeChildrenInParallel )
    {
      scheduleParallelTasks();
    } else
    { 
      scheduleNextSequentialTask();
    }
  } else
  {
    d->doEmitRunFinished( ok );
  }
}

void 
TaskDecorator::decoratedDebugMessage(Task::MessageType type, const QString& message)
{
  LogEntry entry;
  entry.type = type;
  entry.time = QTime::currentTime();
  entry.message = message;
  d->log.append( entry );
  emit debugMessage( type, message );
}

void 
TaskDecorator::taskRunnerFinishedTask(TaskDecorator* task, bool success)
{
  if ( task && d->childTaskObjects.contains( task ) )
  {
    disconnect( task, SIGNAL(dataChanged()), this, SLOT(childTaskProgress()) );
    d->childTaskObjects.removeOne( task );
    d->finishedTaskObjects.append( task );
    if ( !( d->executeChildrenInParallel ) )
    {
      if ( !( success ) )
      {
        d->childHadError = true;
        if ( !( d->ignoreChildErrors ) )
        {
          disconnect( d->container()->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)),
                    this, SLOT(taskRunnerFinishedTask(TaskDecorator*,bool)) );
          d->doEmitRunFinished( false );
        }
      } else
      {
        // This will emit runFinished() automatically if no more tasks are a/v
        scheduleNextSequentialTask();
      }
    } else
    {
      if ( d->childTaskObjects.isEmpty() )
      {
        disconnect( d->container()->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)),
                    this, SLOT(taskRunnerFinishedTask(TaskDecorator*,bool)) );
        d->doEmitRunFinished( d->childHadError && d->ignoreChildErrors ? 
            d->decoratedRunSuccess : false );
      }
    }
  }
}

void 
TaskDecorator::childTaskProgress()
{
  // Let observers know, that they can update 
  // (because overall progress has changed)
  emit dataChanged();
}

void 
TaskDecorator::scheduleParallelTasks()
{
  foreach ( TaskDecorator *task, d->childTaskObjects )
  {
    bool success = d->container()->runner()->run( task );
    if ( !( success ) )
    {
      decoratedDebugMessage( Task::Error, 
                              i18n( "Failed to start task %1" )
                              .arg( task->name() ) );
      d->childTaskObjects.removeOne( task );
      d->finishedTaskObjects.append( task );
      if ( !( d->ignoreChildErrors ) )
      {
        d->childHadError = true;
      }
    } else
    {
      connect( task, SIGNAL(dataChanged()), this, SLOT(childTaskProgress()) );
    }
  }
  
  // Note: All children might have finished setup/run w/ error
  if ( d->childTaskObjects.isEmpty() )
  {
    disconnect( d->container()->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)),
                this, SLOT(taskRunnerFinishedTask(TaskDecorator*,bool)) );
    d->doEmitRunFinished( d->ignoreChildErrors ? true : false );
  }
}

void 
TaskDecorator::scheduleNextSequentialTask()
{
  while ( d->childTaskObjects.size() > 0 )
  {
    TaskDecorator *task = d->childTaskObjects.first();
    bool success = d->container()->runner()->run( task );
    if ( !( success ) )
    {
      decoratedDebugMessage( Task::Error, 
                             i18n( "Failed to start task %1" )
                              .arg( task->name() ) );
      d->childTaskObjects.removeOne( task );
      d->finishedTaskObjects.append( task );
      d->childHadError = true;
      if ( !( d->ignoreChildErrors ) )
      {
        d->doEmitRunFinished( false );
        break;
      }
    } else 
    {
      connect( task, SIGNAL(dataChanged()), this, SLOT(childTaskProgress()) );
      break;
    }
  }
  
  // No more child tasks to wait for... end it now
  if ( d->childTaskObjects.isEmpty() )
  {
    disconnect( d->container()->runner(), SIGNAL(finishedRunningTask(TaskDecorator*,bool)),
                this, SLOT(taskRunnerFinishedTask(TaskDecorator*,bool)) );
    d->doEmitRunFinished( false );
  }
}



  
}

}

