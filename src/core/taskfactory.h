/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKFACTORY_H
#define TASKFACTORY_H

#include "task.h"

#include "ki3u_core_config.h"

#include <kdemacros.h>
#include <KPluginFactory>
#include <KPluginLoader>

#define KI3U_PLUGIN_EXPORT( c ) \
  K_PLUGIN_FACTORY( KI3UTaskFactory, registerPlugin< c >(); ) \
  K_EXPORT_PLUGIN( KI3UTaskFactory("c") )
  
#include <QObject>
#include <QVariantList>

namespace ki3u
{
  
namespace core
{

class KI3U_CORE_EXPORT TaskFactory : public QObject
{
  
  Q_OBJECT
  
public:
  
  TaskFactory( QObject *parent = 0, const QVariantList &args = QVariantList() );
  virtual ~TaskFactory();
  
  virtual Task* createTask() = 0;
  
};

}
  
}

#endif // TASKFACTORY_H
