/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskrunner.h"

#include "taskcontainer.h"

#include <KLocale>

#include <QList>

namespace ki3u
{

namespace core
{

/**
 * @private
 */
class TaskRunner::TaskRunnerPrivate
{
  
public:
  
  TaskContainer                *container;
  QList< TaskDecorator* >       tasks;
  QString                       lastError;
  QStringList                   lastErrorLog;
  
  TaskRunnerPrivate( TaskRunner *runner, TaskContainer *container ) :
    container( container ),
    tasks(),
    lastError(),
    lastErrorLog(),
    q( runner )
  {
  }
  
  virtual ~TaskRunnerPrivate()
  {
  }
  
private:
  
  TaskRunner *q;
  
};

/**
 * @class TaskRunner
 * @brief Runs tasks
 * 
 * The TaskRunner is used to run tasks. It provides an simple interface to
 * start execution of tasks and observing their state during execution.
 */

/**
 * @brief Constructor
 */
TaskRunner::TaskRunner( TaskContainer* parent ) : 
  QObject(parent),
  d( new TaskRunnerPrivate( this, parent ) )
{
  
}

/**
 * @brief Destructor
 */
TaskRunner::~TaskRunner()
{
  delete d;
}

/**
 * @brief Run a task
 * 
 * This starts executing a @p task, doing some consistency checking before (e.g.
 * it will make sure, the task is not already running, either directly or
 * as part of another task's execution.
 * 
 * The runner will make sure the task is properly initialized and finalized
 * (thus, the calling context just has to pass a task into the runner and
 * it will do everything else required).
 * 
 * @brief Returns true if the task been started successfully, otherwise false.
 * 
 * @sa lastError, lastErrorLog
 */
bool 
TaskRunner::run(TaskDecorator* task)
{
  d->lastError.clear();
  d->lastErrorLog.clear();
  
  if ( task )
  {
    if ( d->tasks.contains( task ) )
    {
      d->lastError = i18n( "Task %1 already running" ).arg( task->name() );
      return false;
    }
    
    if ( task->state() != TaskDecorator::Idle )
    {
      d->lastError = i18n( "Task %1 is currently in use" ).arg( task->name() );
      d->lastErrorLog << i18n( "The task's state indicates, that the task is "
                               "currently running. This is possible, when the "
                               "task is used by another one during that tasks "
                               "execution." );
      return false;
    }
    
    // TODO: As soon as task nesting is implemented, we need to check circular references here
    
    if ( !( task->setup() ) )
    {
      d->lastError = i18n( "Failed to setup task %1" ).arg( task->name() );
      return false;
    }
    
    connect( task, SIGNAL(runFinished(bool)), this, SLOT(taskFinished(bool)) );
    
    if ( !( task->run() ) ) 
    {
      d->lastError = i18n( "Failed to start task %1" ).arg( task->name() );
      disconnect( task, SIGNAL(runFinished(bool)), 
                  this, SLOT(taskFinished(bool)) );
      task->teardown();
      return false;
    }
    
    d->tasks.append( task );
    return true;
  } else
  {
    d->lastError = i18n( "Received null pointer" );
    return false;
  }
}

/**
 * @brief Last error message.
 * 
 * Returns the last error message produced using the run() method.
 */
const QString& 
TaskRunner::lastError() const
{
  return d->lastError;
}

/**
 * @brief Last error log.
 * 
 * Returns the last error log that has been created when using the run() method.
 * The log might be empty (in fact, most errors produce only an error message 
 * and no log entries).
 * 
 * @sa lastError
 */
const QStringList& 
TaskRunner::lastErrorLog() const
{
  return d->lastErrorLog;
}

void 
TaskRunner::taskStarted(bool success)
{
  if ( !( success ) )
  {
    TaskDecorator *task = dynamic_cast< TaskDecorator* >( sender() );
    if ( task )
    {
      finalizeTask( task, success );
    }
  }
}

void 
TaskRunner::taskFinished( bool success )
{
  TaskDecorator *task = dynamic_cast< TaskDecorator* >( sender() );
  if ( task )
  {
    finalizeTask( task, success );
  }
}

void 
TaskRunner::finalizeTask( TaskDecorator* task, bool success )
{
  task->teardown();
  d->tasks.removeAll( task );
  disconnect( task, SIGNAL(runFinished(bool)), 
              this, SLOT(taskFinished(bool)) );
  emit finishedRunningTask( task, success );
}


}

}
