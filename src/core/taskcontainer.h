/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKCONTAINER_H
#define TASKCONTAINER_H

#include "factory.h"
#include "secretsstore.h"
#include "taskdecorator.h"
#include "taskrunner.h"

#include "ki3u_core_config.h"

#include <QList>
#include <QObject>

namespace ki3u
{

namespace core
{

class KI3U_CORE_EXPORT TaskContainer : public QObject
{
  
  Q_OBJECT

public:
  
  TaskContainer( QObject *parent = 0 );
  virtual ~TaskContainer();
  
  TaskDecorator* task( const QString &id ) const;
  TaskDecorator* createTask( const QString &factoryId );
  TaskDecorator* createTaskFrom( TaskDecorator *task );
  void removeTask( TaskDecorator* task );
  void removeTask( const QString &id );
  const QList< TaskDecorator* >& tasks() const;
  
  void save( KConfigGroup &config );
  void restore( KConfigGroup &config );
  
  QString generateId() const;
  
  Factory* factory() const;
  SecretsStore* secretsStore() const;
  TaskRunner* runner() const;
  
  friend class TaskDecorator;
  
signals:
  
  void taskListEmptied();
  void taskAppended( int index );
  void taskRemoved( int index );
  void taskDataChanged( int index );
  
private:
  
  class TaskContainerPrivate;
  TaskContainerPrivate *d;
  
private slots:
  
  void handleTaskDataChanged();
    
};

}

}

#endif // TASKCONTAINER_H
