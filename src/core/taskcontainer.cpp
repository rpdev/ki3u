/*
   ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskcontainer.h"

#include <KConfigGroup>
#include <KDebug>

#include <QList>

namespace ki3u
{

namespace core
{

/**
 * @private
 */
class TaskContainer::TaskContainerPrivate
{
public:
  
  static const QString ConfigKeyTaskId;
  
  static const QString ConfigGroupTasks;
  static const QString ConfigGroupSecretsStore;
  
  
  QList< TaskDecorator* >        tasks;
  QStringList                    deletedTasks;
  Factory                       *factory;
  SecretsStore                  *secretsStore;
  TaskRunner                    *taskRunner;
  
  TaskContainerPrivate( TaskContainer *taskContainer ) :
    tasks(),
    deletedTasks(),
    factory( 0 ),
    secretsStore( 0 ),
    taskRunner( 0 ),
    q( taskContainer )
  {
  }
  
  virtual ~TaskContainerPrivate()
  {
  }
  
  void 
  deleteAllTasks()
  {
    deletedTasks.clear();
    foreach( TaskDecorator *task, tasks )
    {
      delete task;
    }
    tasks.clear();
    emit q->taskListEmptied();
  }
  
  void
  appendTask( TaskDecorator *task )
  {
    tasks.append( task );
    connect( task, SIGNAL(dataChanged()), q, SLOT(handleTaskDataChanged()) );
    emit q->taskAppended( tasks.count() - 1 );
  }
  
  void
  removeTask( TaskDecorator *task )
  {
    int index = tasks.indexOf( task );
    if ( index >= 0 )
    {
      deletedTasks << task->id();
      delete tasks[ index ];
      tasks.removeAt( index );
      emit q->taskRemoved( index );
    }
  }
  
private:
  
  TaskContainer *q;
  
};

const QString TaskContainer::TaskContainerPrivate::ConfigKeyTaskId = "id";

const QString TaskContainer::TaskContainerPrivate::ConfigGroupTasks = "Tasks";
const QString TaskContainer::TaskContainerPrivate::ConfigGroupSecretsStore = 
  "SecretsStore";


/**
 * @class TaskContainer
 * @brief Collect TaskDecorator objects and provide easy task creation.
 * 
 * The TaskContainer is an aggregation object, which collects TaskDecorator 
 * objects. Additionally, it also has an internal Factory, which is used
 * to provide easy creation of new Task objects.
 * 
 * @ingroup ki3u_core
 */

/**
 * @brief Constructor
 */
TaskContainer::TaskContainer( QObject *parent ) :
  QObject( parent ),
  d( new TaskContainerPrivate( this ) )
{
  d->factory = new Factory( this );
  d->secretsStore = new SecretsStore( this );
  d->taskRunner = new TaskRunner( this );
}

/**
 * @brief Destructor
 */
TaskContainer::~TaskContainer()
{
  delete d;
}

/**
 * @brief Access task by ID
 * 
 * @returns The TaskDecorator object which has the given @p id or
 * 0, if the container does not have such a task.
 */
TaskDecorator* 
TaskContainer::task(const QString& id) const
{
  for ( int i = 0; i < d->tasks.size(); i++ )
  {
    if ( d->tasks[ i ]->id() == id )
    {
      return d->tasks[ i ];
    }
  }
  return 0;
}

/**
 * @brief Creates a new TaskDecorator
 * 
 * This method will use the factory with the given @p factoryId to create
 * a new Task object, which it will wrap immediately with a TaskDecorator 
 * object. The newly created task is appended to the list of task objects.
 * 
 * @returns A new TaskDecorator object.
 * 
 * @note The returned TaskDecorator might wrap a 0 pointer. This is basically
 *       no problem (the TaskDecorator will still be functional as all
 *       access to the internal reference to the Task is checked and thus
 *       there should not be crashes due to invalid memory access).
 */
TaskDecorator* 
TaskContainer::createTask(const QString& factoryId)
{
  Task *rawTask = d->factory->createTask( factoryId );
  TaskDecorator *task = new TaskDecorator( this );
  task->setTask( rawTask );
  task->setId( generateId() );
  task->setFactoryId( factoryId );
  d->appendTask( task );
  return task;
}

/**
 * @brief Create a new task from an existing one
 * 
 * Creates a new task by cloning the given @p task. The task must be stored
 * inside the task container.
 */
TaskDecorator* 
TaskContainer::createTaskFrom(TaskDecorator* task)
{
  if ( task && d->tasks.indexOf( task ) >= 0 )
  {
    TaskDecorator *newTask = dynamic_cast< TaskDecorator* >( task->clone() );
    if ( newTask )
    {
      d->appendTask( newTask );
    }
    return newTask;
  }
  return 0;
}


/**
 * @brief Remove and delete a task.
 * 
 * This will remove the @p task from the container and delete it, unless
 * the task is not stored in this container.
 */
void 
TaskContainer::removeTask(TaskDecorator* task)
{
  d->removeTask( task );
}

/**
 * @brief Remove and delete a task.
 * 
 * This will delete the task with the specified @p id from the container.
 */
void 
TaskContainer::removeTask(const QString& id)
{
  d->removeTask( task( id ) );
}

/**
 * @brief List of tasks
 * 
 * @return The list of tasks the container currently holds.
 */
const QList< TaskDecorator* >& 
TaskContainer::tasks() const
{
  return d->tasks;
}

/**
 * @brief Save the container and all tasks configuration.
 * 
 * This will save the containers own data plus the data of all tasks to the
 * given @p config group.
 */
void 
TaskContainer::save( KConfigGroup &config )
{
  KConfigGroup taskGroup = config.group( TaskContainerPrivate::ConfigGroupTasks );
  foreach( QString id, d->deletedTasks )
  {
    taskGroup.deleteGroup( id );
  }
  foreach( TaskDecorator *task, d->tasks )
  {
    KConfigGroup taskPrivateGroup = taskGroup.group( task->id() );
    task->save( taskPrivateGroup );
  }
  KConfigGroup secretsStoreGroup = 
    config.group( TaskContainerPrivate::ConfigGroupSecretsStore );
  d->secretsStore->save( secretsStoreGroup );
}

/**
 * @brief Restore the container and all tasks.
 * 
 * This will read configuration values from the given @p config group and
 * restore the container plus all former tasks.
 */
void 
TaskContainer::restore( KConfigGroup &config )
{
  d->deleteAllTasks();
  KConfigGroup taskGroup = config.group( TaskContainerPrivate::ConfigGroupTasks );
  foreach( QString taskId, taskGroup.groupList() )
  {
    TaskDecorator *task = new TaskDecorator( this );
    KConfigGroup taskPrivateGroup = taskGroup.group( taskId );
    task->restore( taskPrivateGroup );
    task->setId( taskId );
    d->appendTask( task );
  }
  KConfigGroup secretsStoreGroup = 
    config.group( TaskContainerPrivate::ConfigGroupSecretsStore );
  d->secretsStore->restore( secretsStoreGroup );
}

/**
 * @brief Generate a unique ID for a task.
 * 
 * This will generate a unique ID for a task, i.e. an ID which is not yet
 * present.
 * 
 * @returns New, unique task ID
 */
QString 
TaskContainer::generateId() const
{
  int i = 0;
  QString id = "task-" + QString::number( i );
  while( task( id ) ) 
  {
    i++;
    id = "task-" + QString::number( i );
  }
  return id;
}

/**
 * @brief The containers Factory
 * 
 * Returns the Factory object of the container, which is used for Task
 * instantiation.
 */
Factory* 
TaskContainer::factory() const
{
  return d->factory;
}

/**
 * @brief The secrets storage
 * 
 * @return The secrets storage used to securely store passwords and other
 *      sensible data.
 */
SecretsStore* 
TaskContainer::secretsStore() const
{
  return d->secretsStore;
}

/**
 * @brief The container's task runner
 * 
 * Returns the TaskRunner used by the container. This runner must be used
 * to run tasks.
 */
TaskRunner* 
TaskContainer::runner() const
{
  return d->taskRunner;
}



/**
 * @fn TaskContainer::taskListEmptied
 * @brief All tasks have been deleted and removed.
 * 
 * This signal is emitted, when the container releases all
 * task objects it holds at once. This happens e.g. when the container is
 * reloaded from configuration.
 */

/**
 * @fn TaskContainer::taskAppended( int index )
 * @brief A task has been added.
 * 
 * This signal is called when a task has been added to the container. This
 * happens either when the createTask() method is used or the container is
 * restored from configuration.
 * 
 * @param index The index of the newly inserted task.
 * 
 * @note New tasks are always added to the very end of the list.
 */

/**
 * @fn TaskContainer::taskRemoved( int index )
 * @brief A task has been removed.
 * 
 * This signal is sent when a task has been removed from the container.
 * 
 * @param index The index of the task to be removed.
 */

/**
 * @fn TaskContainer::taskDataChanged( int index )
 * @brief A tasks data has changed
 * 
 * This signal indicates, that a task's data has been changed. Observers
 * can use this to update views onto tasks.
 * 
 * @param index The index of the task which's data has changed
 */

/**
 * @brief Handle change of data in tasks
 */
void 
TaskContainer::handleTaskDataChanged()
{
  TaskDecorator *task = dynamic_cast< TaskDecorator* >( sender() );
  if ( task )
  {
    if ( d->tasks.contains( task ) )
    {
      emit taskDataChanged( d->tasks.indexOf( task ) );
    }
  }
}


}

}

