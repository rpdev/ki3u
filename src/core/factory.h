/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FACTORY_H
#define FACTORY_H

#include "task.h"
#include "taskfactorydecorator.h"

#include "ki3u_core_config.h"

#include <QList>
#include <QObject>

namespace ki3u
{
  
namespace core
{
  
class KI3U_CORE_EXPORT Factory : public QObject
{
  
  Q_OBJECT
  
public:
  
  Factory( QObject *parent = 0 );
  virtual ~Factory();
  
  Task* createTask( const QString &id ) const;
  
  const QList< TaskFactoryDecorator* >& concreteFactories() const;
  TaskFactoryDecorator* taskFactory( const QString &id );
  
protected:
  
private:
  
  class FactoryPrivate;
  FactoryPrivate *d;
  
};

}

}

#endif // FACTORY_H
