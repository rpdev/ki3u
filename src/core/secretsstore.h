/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SECRETSSTORE_H
#define SECRETSSTORE_H

#include "ki3u_core_config.h"

#include <KConfigGroup>

#include <QtCore/QObject>


namespace ki3u 
{
 
namespace core
{

class KI3U_CORE_EXPORT SecretsStore : public QObject
{
  
  Q_OBJECT
  
public:
  
  static const QString DefaultContext;
  
  explicit SecretsStore(QObject* parent = 0);
  virtual ~SecretsStore();
  
  bool hasKey( const QString &key, 
               const QString context = DefaultContext );
  QString value( const QString &key,
                 const QString &context = DefaultContext );
  void setValue( const QString &key,
                 const QString &value,
                 const QString &context = DefaultContext );
  void removeValue( const QString &key,
                    const QString &context = DefaultContext );
  void save( const KConfigGroup &config );
  void restore( const KConfigGroup &config );
  
private:
  
  class SecretsStorePrivate;
  SecretsStorePrivate *d;
  
};



}

}

#endif // SECRETSSTORE_H
