/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "taskfactorydecorator.h"

#include <KDebug>
#include <KPluginFactory>
#include <KPluginLoader>

namespace ki3u
{
  
namespace core
{
 
/**
 * @private
 */
class TaskFactoryDecorator::TaskFactoryDecoratorPrivate
{
  
public:
  
  TaskFactory *factory;
  
  QString id;
  QString name;
  QString description;
  QString comment;
  KIcon icon;
  
  TaskFactoryDecoratorPrivate() :
    id( QString() ),
    name( QString() ),
    description( QString() ),
    comment( QString() ),
    icon( KIcon() )
  {
  }
  
  void 
  dumpDebug()
  {
    kWarning() << "Beginning dump of Factory Meta Data";
    kWarning() << "ID: " << id;
    kWarning() << "Name: " << name;
    kWarning() << "Desciption: " << description;
    kWarning() << "Comment: " << comment;
  }
  
};

const QString TaskFactoryDecorator::ServiceIdPropertyName = "ki3u-service-id";

/**
 * @class TaskFactoryDecorator
 * @brief Decorates TaskFactory objects providing further meta information
 * 
 * TaskFactoryDecorator objects wrap TaskFactory object. They are used to 
 * provide further meta information that is read from the dektop file that
 * has proposed the plugin file from which the TaskFactory has been loaded.
 * 
 * @ingroup ki3u_core
 */

/**
 * @brief Constructor
 */
TaskFactoryDecorator::TaskFactoryDecorator( QObject* parent ): 
  TaskFactory( parent ),
  d( new TaskFactoryDecoratorPrivate() )
{
}

/**
 * @brief Destructor
 */
TaskFactoryDecorator::~TaskFactoryDecorator()
{
  delete d;
}

/**
 * @brief Creates a new task object
 * 
 * Uses the decorated factory to create a new Task object.
 */
Task* 
TaskFactoryDecorator::createTask()
{
  if ( d->factory )
  {
    return d->factory->createTask();
  }
  return 0;
}

/**
 * @brief The factory's ID
 * 
 * Returns the ID of the factory.
 */
const QString& 
TaskFactoryDecorator::id() const
{
  return d->id;
}

/**
 * @brief The factory's name
 * 
 * Returns a (displayable) name of the factory.
 */
const QString& 
TaskFactoryDecorator::name() const
{
  return d->name;
}

/**
 * @brief The factory's description
 * 
 * Returns a (displayable) descriptive text for the factory.
 */
const QString& 
TaskFactoryDecorator::description() const
{
  return d->description;
}

/**
 * @brief A comment further describing the factory
 * 
 * Returns a (displayable) text further describing the
 * factory.
 */
const QString& 
TaskFactoryDecorator::comment() const
{
  return d->comment;
}

/**
 * @brief An icon for the factory
 * 
 * Returns an icon for the factory.
 */
const KIcon& 
TaskFactoryDecorator::icon() const
{
  return d->icon;
}

/**
 * @brief Load a TaskFactory from a library
 * 
 * Loads a TaskFactory from the library pointed to by the @p service.
 * Additionally, this will load meta information from the service and store it.
 */
bool 
TaskFactoryDecorator::loadLibrary( const KService::Ptr& service )
{
  KPluginFactory *factory = KPluginLoader( service->library() ).factory();
  if ( factory && service->propertyNames().contains( ServiceIdPropertyName ) )
  {
    d->factory = factory->create< TaskFactory >( this );
    d->id = service->property( ServiceIdPropertyName, QVariant::String ).toString();
    d->name = service->name();
    d->description = service->genericName();
    d->comment = service->comment();
    d->icon = KIcon( service->icon() );
    return true;
  }
  return false;
}

}

}