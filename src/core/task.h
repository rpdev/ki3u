/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASK_H
#define TASK_H

#include "ki3u_core_config.h"

#include <QtCore/QObject>

// Forward declarations
class KConfigGroup;

namespace ki3u 
{
namespace core 
{

class KI3U_CORE_EXPORT Task : public QObject
{
  
  Q_OBJECT

public:
  
  typedef enum MessageType
  {
    Debug = 0,
    Error
  } MessageType;
  
  Task( QObject *parent = 0 ) : QObject( parent ) {}
  virtual ~Task() {}
    
  virtual bool setup() = 0;
  virtual bool run() = 0;
  virtual bool teardown() = 0;
    
  virtual Task* clone() = 0;
    
  virtual void save( KConfigGroup &config ) = 0;
  virtual void restore( KConfigGroup &config ) = 0;
    
  virtual void configure() = 0;
    
signals:
  
  void runStarted( bool ok );
  void runProgress( int progress );
  void runFinished( bool ok );
  
  void debugMessage( MessageType type, const QString &message );
  
};

}
  
}

#endif // TASK_H
