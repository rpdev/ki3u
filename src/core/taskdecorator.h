/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKDECORATOR_H
#define TASKDECORATOR_H

#include "task.h"

#include "ki3u_core_config.h"

#include <QDateTime>
#include <QStringList>
#include <QTime>

namespace ki3u
{
 
namespace core
{
  
class TaskContainer;
 
class KI3U_CORE_EXPORT TaskDecorator : public Task
{
  
  Q_OBJECT
  
  Q_PROPERTY( QString id READ id )
  Q_PROPERTY( QString factoryId READ factoryId )
  
  Q_PROPERTY( QString name READ name WRITE setName )
  Q_PROPERTY( QString comment READ comment WRITE setComment )
  
  Q_PROPERTY( QStringList childTasks READ childTasks WRITE setChildTasks )
  Q_PROPERTY( bool executeChildrenInParallel READ executeChildrenInParallel WRITE setExecuteChildrenInParallel )
  Q_PROPERTY( bool ignoreChildErrors READ ignoreChildErrors WRITE setIgnoreChildErrors )
  
  Q_PROPERTY( TaskState state READ state )
  Q_PROPERTY( int progress READ progress )
  
public:
  
  typedef enum TaskState
  {
    Idle = 0,
    Initialized,
    Starting,
    Running,
    Finishing
  } TaskState;
  
  typedef struct LogEntry
  {
    MessageType         type;
    QTime               time;
    QString             message;
  } LogEntry;
  
  TaskDecorator( TaskContainer *parent );
  virtual ~TaskDecorator();
  
  // Implementation of Task interface
    
  virtual bool setup();
  virtual bool run();
  virtual bool teardown();
    
  virtual Task* clone();
    
  virtual void save( KConfigGroup &config );
  virtual void restore( KConfigGroup &config );
    
  virtual void configure();
  
  // Extended interface
  
  QString id() const;
  QString factoryId() const;
  QDateTime lastSuccessfulRunDateTime() const;
  
  QString name() const;
  void setName( const QString &name );
  
  QString comment() const;
  void setComment( const QString &comment );
  
  const QStringList& childTasks() const;
  void setChildTasks( const QStringList &childTasks );
  
  bool executeChildrenInParallel() const;
  void setExecuteChildrenInParallel( bool executeInParallel );
  
  bool ignoreChildErrors() const;
  void setIgnoreChildErrors( bool ignoreChildErrors );
  
  TaskState state() const;
  int progress() const;
  const QList< LogEntry >& log() const;
  
  bool isLocked() const;
  
  friend class TaskContainer;
  
public slots:
  
  bool lock();
  void unlock();
  
signals:
  
  void dataChanged();
  void logCleared();
  
protected:
  
private:
  
  class TaskDecoratorPrivate;
  TaskDecoratorPrivate *d;
  
  void setTask( Task *task );
  void setId( const QString &id );
  void setFactoryId( const QString &id );
  
private slots:
  
  void decoratedStarted( bool ok );
  void decoratedProgress( int progress );
  void decoratedFinished( bool ok );
  void decoratedDebugMessage( MessageType type, const QString &message );
  
  void taskRunnerFinishedTask( TaskDecorator *task, bool success );
  
  void childTaskProgress();
  
  void scheduleParallelTasks();
  void scheduleNextSequentialTask();
  
};

}

}

#endif // TASKDECORATOR_H
