/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKRUNNER_H
#define TASKRUNNER_H

#include "taskdecorator.h"

#include "ki3u_core_config.h"

#include <QtCore/QObject>
#include <QStringList>

namespace ki3u
{

namespace core
{

// Forward declarations:
class TaskContainer;

class KI3U_CORE_EXPORT TaskRunner : public QObject
{
  
  Q_OBJECT
  
public:
  
    explicit TaskRunner( TaskContainer* parent );
    virtual ~TaskRunner();
    
    bool run( TaskDecorator *task );
    const QString& lastError() const;
    const QStringList& lastErrorLog() const;
    
signals:
  
  void finishedRunningTask( TaskDecorator *task, bool success );
    
private:
  
  class TaskRunnerPrivate;
  TaskRunnerPrivate *d;
  
private slots:
  
  void taskStarted( bool success );
  void taskFinished( bool success );
  void finalizeTask( TaskDecorator *task, bool success );
  
};

}

}

#endif // TASKRUNNER_H
