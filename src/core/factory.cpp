/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "factory.h"

#include <KDebug>
#include <KServiceTypeTrader>

namespace ki3u
{

namespace core
{

/**
 * @private
 */
class Factory::FactoryPrivate
{
  
public:
  
  QList< TaskFactoryDecorator* > concreteFactories;
  
  TaskFactoryDecorator* 
  factoryForId( const QString &id ) const
  {
    foreach( TaskFactoryDecorator *factory, concreteFactories )
    {
      if ( factory->id() == id )
      {
          return factory;
      }
    }
    return 0;
  }
  
  void 
  loadFactories( Factory *parent )
  {
    kDebug() << "Going to load plugins";
    
    KService::List offers = KServiceTypeTrader::self()->query( "KI3U/Solution" );

    KService::List::const_iterator it;
    for ( it = offers.begin(); it < offers.end(); ++it )
    {
        KService::Ptr service = *it;
        qDebug() << "Trying " << service->library();
        
        TaskFactoryDecorator *factory = new TaskFactoryDecorator( parent );
        if ( factory->loadLibrary( service ) ) 
        {
          concreteFactories.append( factory );
          kDebug() << "Successfully loaded plugin from " << service->library();
        } else
        {
          qWarning() << service->library() << " seems not to be a valid plugin";
          delete factory;
        }
    }
  }
  
};


/**
 * @class Factory
 * @brief Creates Task objects
 * 
 * The Factory class is both a factory to produce new Task objects and an 
 * aggregation that collects TaskFactory objects. This design allows to have a
 * single central object for object creation with simple parametrization.
 * 
 * The factoy holds a list of TaskDecoratorFactory objects. These concrete
 * factories are loaded dynamically at runtime by using KDE's service trader
 * framework. That means: Concrete factories are available via plugins
 * (dynamically loadable libraries), that get loaded at runtime. For each
 * plugin, a meta file (a *.desktop file) must exist. The information
 * from that file is used by the service trader framework.
 * 
 * As soon as a Factory object is created, it will query the service framework
 * for all KI3U compatible plugins (i.e. plugins, which use "KI3U/Solution"
 * as type in their desktop file) and wrap the loaded TaskFactory objects with
 * a TaskFactoryDecorator. That decorator will also load some further
 * meta information from the desktop files, thus, the actual plugins
 * only have to implement the TaskFactory and Task interfaces and provide
 * a desktop file - no information is present twice (i.e. the TaskFactory
 * implementation does not need to implement some id() method itself).
 * 
 * @ingroup ki3u_core
 */

/**
 * @brief Constructor
 */
Factory::Factory( QObject* parent ): 
  QObject( parent ),
  d( new FactoryPrivate() )
{
  d->loadFactories( this );
}

/**
 * @brief Destructor
 */
Factory::~Factory()
{
  delete d;
}

/**
 * @brief Creates a new Task object.
 * 
 * The method will create a new Task object using the factory which has
 * the specified @p id.
 * 
 * @returns A new Task object created using the requested id or 0 on error.
 */
Task* 
Factory::createTask(const QString& id) const
{
  TaskFactoryDecorator *factory = d->factoryForId( id );
  if ( factory )
  {
    return factory->createTask();
  }
  return 0;
}

/**
 * @brief List of available concrete TaskFactory objects
 * 
 * @returns The list of TaskFactoryDecorator objects, the factory uses for
 *          object creation.
 */
const QList< TaskFactoryDecorator* >& 
Factory::concreteFactories() const
{
  return d->concreteFactories;
}

/**
 * @brief Search for TaskFactory by id
 * 
 * Returns the TaskFactory with the specified @p id or
 * 0, if no factory was found that matches.
 */
TaskFactoryDecorator* 
Factory::taskFactory(const QString& id)
{
  foreach( TaskFactoryDecorator *factory,  d->concreteFactories )
  {
    if ( factory->id() == id )
    {
      return factory;
    }
  }
  return 0;
}




  
}

}