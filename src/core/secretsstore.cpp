/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "secretsstore.h"

#include <KWallet/Wallet>

#include <QHash>

namespace ki3u 
{
 
namespace core
{

/**
 * @private
 */
class SecretsStore::SecretsStorePrivate
{
  
public:
  
  static const QString WalletFolder;
  static const QString KnownSecretsConfigGroup;
  
  QHash< QString, QHash< QString, bool > > knownSecrets;
  
  KWallet::Wallet *wallet;
  
  SecretsStorePrivate( SecretsStore *secretsStore ) :
    knownSecrets( QHash< QString, QHash< QString, bool > >() ),
    wallet( 0 ),
    q( secretsStore )
  {
  }
  
  virtual ~SecretsStorePrivate()
  {
    if ( wallet )
    {
      delete wallet;
    }
  }
  
  KWallet::Wallet*
  getWallet()
  {
    if ( wallet == 0 )
    {
      wallet = KWallet::Wallet::openWallet( KWallet::Wallet::LocalWallet(), 
                                            0, 
                                            KWallet::Wallet::Synchronous );
      if ( wallet )
      {
        if ( !( wallet->hasFolder( WalletFolder ) ) )
        {
          wallet->createFolder( WalletFolder );
        }
        wallet->setFolder( WalletFolder );
      }
    }
    return wallet;
  }
  
private:
  
  SecretsStore *q;
  
};

const QString SecretsStore::SecretsStorePrivate::WalletFolder = "ki3u";
const QString SecretsStore::SecretsStorePrivate::KnownSecretsConfigGroup = "KnownSecrets";

/**
 * @class SecretsStore
 * @brief Provides secrets storage
 * 
 * The SecretsStore class can be used to store secrets such as passwords
 * so the user does not have to provide them every time.
 * 
 * Currently, this provides an easy to use interface to the KWallet system. 
 * Clients can query the store whether it has a certain key for a given
 * context and - if so - read this value. New values can be added if the
 * user decides to store secrets.
 * 
 * @ingroup ki3u_core
 */


const QString SecretsStore::DefaultContext = "default";

/**
 * @brief Constructor
 */
SecretsStore::SecretsStore(QObject* parent) : 
  QObject(parent),
  d( new SecretsStorePrivate( this ) )
{
}

/**
 * @brief Destructor
 */
SecretsStore::~SecretsStore()
{
  delete d;
}

/**
 * @brief Test whether a given key is present
 * 
 * Checks, whether the store has a value for @p key in the given
 * @p context. Clients can use that to make sure, an entry is present in the
 * store before requesting it to read or delete a specified entry. This could
 * avoid displaying dialogs to the user when it makes no sense (e.g. a service
 * wants to make sure, a password is deleted from the store, but the value is
 * not present; this would nevertheless display a dialog to the user requesting
 * access to the wallet system).
 * 
 * @returns True if the store has a value for the given key, otherwise false.
 */
bool 
SecretsStore::hasKey( const QString& key, const QString context )
{
  return d->knownSecrets.value( 
    context, QHash< QString, bool >() ).value( key, false );
}

/**
 * @brief Get a value from the store
 * 
 * Returns the value for the given @p key in the specified @p context from
 * the store.
 */
QString 
SecretsStore::value( const QString& key, const QString& context )
{
  KWallet::Wallet *wallet = d->getWallet();
  if ( wallet )
  {
    QMap< QString, QString > map;
    if ( wallet->readMap( context, map  ) == 0 )
    {
      return map.value( key, QString() );
    }
  }
  return QString();
}

/**
 * @brief Write value to store
 * 
 * Writes the @p value into the store using the given @p key and @p context.
 */
void 
SecretsStore::setValue( const QString& key, 
                        const QString& value, 
                        const QString& context )
{
  KWallet::Wallet *wallet = d->getWallet();
  if ( wallet )
  {
    QMap< QString, QString > map;
    if ( wallet->readMap( context, map ) )
    {
      map.insert( key, value );
      if ( wallet->writeMap( key, map ) == 0 )
      {
        d->knownSecrets[ context ].insert( key, true );
      }
    }
  }
}

/**
 * @brief Remove a value from password store
 * 
 * Removes the value associated to the given @p key in the given @p context
 * from the store.
 */
void 
SecretsStore::removeValue( const QString& key, 
                           const QString& context )
{
  KWallet::Wallet *wallet = d->getWallet();
  if ( wallet )
  {
    QMap< QString, QString > map;
    if ( wallet->readMap( context, map ) == 0 )
    {
      map.remove( key );
      wallet->writeMap( key, map );
    }
  }
}

/**
 * @brief Save store configuration
 * 
 * Writes the store's configuration to the specified @p config object.
 * 
 * @note This will just write meta information (e.g. which keys are stored).
 * The secrets itself are stored in KWallet.
 */
void 
SecretsStore::save(const KConfigGroup& config)
{
  KConfigGroup knownSecrets = config.group( 
    SecretsStorePrivate::KnownSecretsConfigGroup );
  foreach ( QString context, d->knownSecrets.keys() )
  {
    knownSecrets.writeEntry( context, 
                              QStringList( 
                                d->knownSecrets.value( context ).keys() ) );
  }
}

/**
 * @brief Restore store configuration
 * 
 * Reads the store's consiguration from the specified @p config group.
 * 
 * @sa save()
 */
void 
SecretsStore::restore(const KConfigGroup& config)
{
  KConfigGroup knownSecrets = config.group(
    SecretsStorePrivate::KnownSecretsConfigGroup );
  d->knownSecrets.clear();
  foreach ( QString context, knownSecrets.entryMap().keys() )
  {
    QStringList keys = knownSecrets.readEntry< QStringList >( context, QStringList() );
    foreach ( QString key, keys )
    {
      d->knownSecrets[ context ].insert( key, true );
    }
  }
}

}

}