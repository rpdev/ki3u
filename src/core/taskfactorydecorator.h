/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef TASKFACTORYDECORATOR_H
#define TASKFACTORYDECORATOR_H

#include "taskfactory.h"

#include "ki3u_core_config.h"

#include <KIcon>
#include <KService>

namespace ki3u

{

namespace core
{

class KI3U_CORE_EXPORT TaskFactoryDecorator : public TaskFactory
{
  
  Q_OBJECT
  
  Q_PROPERTY( QString id READ id )
  Q_PROPERTY( QString name READ name )
  Q_PROPERTY( QString description READ description )
  Q_PROPERTY( QString comment READ comment )
  Q_PROPERTY( KIcon icon READ icon )
  
public:
  
  static const QString ServiceIdPropertyName;
  
  TaskFactoryDecorator( QObject* parent = 0 );
  virtual ~TaskFactoryDecorator();
  
  // Implementation of TaskFactory interface
  virtual Task* createTask();
  
  // Additional Interface
  
  const QString& id() const;
  const QString& name() const;
  const QString& description() const;
  const QString& comment() const;
  const KIcon&   icon() const;
  
  
  friend class Factory;
  
protected:
  
private:
  
  class TaskFactoryDecoratorPrivate;
  TaskFactoryDecoratorPrivate *d;
  
  bool loadLibrary( const KService::Ptr &service );
  
};

}

}

#endif // TASKFACTORYDECORATOR_H
