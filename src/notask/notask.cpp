/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "notask.h"

#include <QTimer>

NoTask::NoTask(QObject* parent) : 
  Task(parent)
{
}

NoTask::~NoTask()
{
}

void NoTask::configure()
{
  // Empty
}

void NoTask::restore(KConfigGroup& )
{
  // Nothing to do here
}

void NoTask::save(KConfigGroup& )
{
  // Nothing to do here
}

ki3u::core::Task* NoTask::clone()
{
  return new NoTask();
}

bool NoTask::teardown()
{
  return true;
}

bool NoTask::run()
{
  QTimer::singleShot( 0, this, SLOT(doSendRunStarted()) );
  return true;
}

bool NoTask::setup()
{
  return true;
}

void NoTask::doSendRunStarted()
{
  emit runStarted( true );
  QTimer::singleShot( 0, this, SLOT(doSendRunFinished()) );
}

void NoTask::doSendRunFinished()
{
  emit runProgress( 100 );
  emit runFinished( true );
}
