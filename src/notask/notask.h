/*
   ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NOTASK_H
#define NOTASK_H

#include "../core/task.h"

class NoTask : public ki3u::core::Task
{

  Q_OBJECT
  
public:
  
  NoTask( QObject* parent = 0 );
  virtual ~NoTask();
  
  // Implementation of Task interface
  virtual void configure();
  virtual void restore(KConfigGroup&);
  virtual void save(KConfigGroup&);
  virtual ki3u::core::Task* clone();
  virtual bool teardown();
  virtual bool run();
  virtual bool setup();
    
private slots:
  
  void doSendRunStarted();
  void doSendRunFinished();
};

#endif // NOTASK_H
