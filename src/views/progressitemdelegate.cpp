/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "progressitemdelegate.h"

#include <QApplication>

namespace ki3u
{
  
namespace views
{
  

/**
 * @private
 */
class ProgressItemDelegate::ProgressItemDelegatePrivate
{
  
public:
  
  ProgressItemDelegatePrivate( ProgressItemDelegate *itemDelegate ) :
    q( itemDelegate )
  {
  }
  
  virtual ~ProgressItemDelegatePrivate()
  {
  }
  
private:
  
  ProgressItemDelegate *q;
  
};

ProgressItemDelegate::ProgressItemDelegate( QObject* parent ) : 
  QItemDelegate( parent ),
  d( new ProgressItemDelegatePrivate( this ) )
{

}

ProgressItemDelegate::~ProgressItemDelegate()
{
  delete d;
}

void 
ProgressItemDelegate::paint( QPainter* painter, 
                             const QStyleOptionViewItem& option, 
                             const QModelIndex& index ) const
{
  
  if ( !( index.data( Qt::UserRole ).isNull() ) )
  {
    int progress = index.data( Qt::UserRole ).toInt();
    
    QStyleOptionProgressBar progressBarOption;
    progressBarOption.rect = option.rect;
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.progress = progress;
    progressBarOption.text = progress >= 0 ? 
      QString( "%1 - %2%" )
        .arg( index.data( Qt::DisplayRole ).toString() )
        .arg( progress ) :
      index.data( Qt::DisplayRole ).toString();
    progressBarOption.textVisible = true;
    
    QApplication::style()->drawControl( QStyle::CE_ProgressBar, 
                                        &progressBarOption,
                                        painter
                                      );
  } else
  {
    QItemDelegate::paint( painter, option, index );
  }  
}

}

}

