/*
    ki3u - Backup program for the KDE Desktop Environment
    Copyright (C) 2011  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PROGRESSITEMDELEGATE_H
#define PROGRESSITEMDELEGATE_H

#include "ki3u_views_config.h"

#include <QItemDelegate>

namespace ki3u
{
  
namespace views
{
  
class KI3U_VIEWS_EXPORT ProgressItemDelegate : public QItemDelegate
{

  Q_OBJECT
  
public:
  
  explicit ProgressItemDelegate(QObject* parent = 0);
  virtual ~ProgressItemDelegate();
  
  // QAbstractItemDelegate interface implementation
  virtual void paint(QPainter* painter, 
                     const QStyleOptionViewItem& option, 
                     const QModelIndex& index) const;
    
private:
  
  class ProgressItemDelegatePrivate;
  ProgressItemDelegatePrivate *d;
  
};

}

}

#endif // PROGRESSITEMDELEGATE_H
