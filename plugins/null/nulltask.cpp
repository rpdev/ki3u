/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nulltask.h"

#include "nullplugin.h"

#include <KDebug>

#include <QMessageBox>
#include <QTimer>

namespace KI3U
{

namespace Plugins
{

NullTask::NullTask( NullPlugin* parent ): Task( parent )
{
}


void
NullTask::configure()
{
    QMessageBox::information( 0, tr( "NULL Task Configuration" ),
                              tr( "A NULL task does not have a configuration.") );
}

const QString
NullTask::id() const
{
    return NullPlugin::ID;
}

bool
NullTask::finalize()
{
    emit statusChanged( tr( "NULL Task finalized" ), Task::Information );
    kDebug() << "Null task" << objectName() << " finalized";
    return true;
}

bool
NullTask::initialize()
{
    m_progress = 0;
    emit statusChanged( tr( "NULL Task initialized" ), Task::Information );
    kDebug() << "Null task" << objectName() << " initialized";
    return true;
}

bool
NullTask::run()
{
    kDebug() << "Null task" << objectName() << " started";
    onTimer();
    emit statusChanged( tr( "NULL Task started" ), Task::Information );
    return true;
}

void NullTask::onTimer()
{
    if ( m_progress < 100 )
    {
        m_progress++;
        emit progressChanged( m_progress );
        QTimer::singleShot( 600, this, SLOT(onTimer()) );
        kDebug() << "Null task" << objectName() << " ticked";
    } else
    {
        emit runFinished( true );
        kDebug() << "Null task" << objectName() << "finished";
    }
}

}

}