/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nullplugin.h"

#include "nulltask.h"

#include <QMessageBox>

namespace KI3U
{
namespace Plugins
{

KI3U_PLUGIN_EXPORT( NullPlugin )
    
const QString NullPlugin::ID = "net.rpdev.ki3u.plugins.nullplugin";

void NullPlugin::configure()
{
    QMessageBox::information( 0, tr( "NULL Solution Configuration" ),
                              tr( "The NULL plugin has no configuration." ) );
}

Task* NullPlugin::createTask()
{
    return new NullTask( this );
}

const QString NullPlugin::id() const
{
    return ID;
}


}

}