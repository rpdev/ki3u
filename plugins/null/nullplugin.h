/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef NULLPLUGIN_H
#define NULLPLUGIN_H

#include <plugin.h>
#include <plugin_macros.h>
#include <solutionmanager.h>

namespace KI3U
{

/**
 * @brief Core Plugins
 *
 * The Plugins namespace holds all core plugins if KI3U.
 */
namespace Plugins
{

class NullPlugin : public Plugin
{
    public:

        static const QString ID;

        NullPlugin(QObject *&parent, const QList<QVariant> &args  = QList<QVariant>() )
            : Plugin( dynamic_cast< SolutionManager* >( parent ), args )
        {
        }

        Task* createTask();
        void configure();
        const QString id() const;
};

}

}
#endif // NULLPLUGIN_H
