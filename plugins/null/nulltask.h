/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef NULLTASK_H
#define NULLTASK_H

#include <task.h>

namespace KI3U
{

namespace Plugins
{

class NullPlugin;

class NullTask : public Task
{

    Q_OBJECT
    
    public:

        NullTask( NullPlugin* parent = 0 );
        
        void configure();
        bool finalize();
        const QString id() const;
        bool initialize();
        bool run();

    private:

        int m_progress;

    private slots:

        void onTimer();

};

}

}
#endif // NULLTASK_H
