/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "groupplugin.h"

#include "grouptask.h"

#include <solutionmanager.h>

#include <KMessageBox>

namespace KI3U
{
    
namespace Plugins
{

KI3U_PLUGIN_EXPORT( GroupPlugin )

class GroupPlugin::Private
{

    public:

        Private( GroupPlugin *plugin )
            : m_plugin( plugin )
        {
        }

        virtual ~Private()
        {
        }
        
    private:

        GroupPlugin *m_plugin;
        
};


const QString GroupPlugin::ID = "net.rpdev.ki3u.plugins.groupplugin";

GroupPlugin::GroupPlugin(QObject* parent, const QList< QVariant >& args)
    : Plugin( dynamic_cast< SolutionManager* >( parent ), args),
      d( new Private( this ) )
{

}

GroupPlugin::~GroupPlugin()
{
    delete d;
}

const QString GroupPlugin::id() const
{
    return ID;
}

QByteArray GroupPlugin::saveSettings()
{
    QByteArray data;
    QDataStream stream( &data, QIODevice::WriteOnly );

    stream << Plugin::saveSettings();
    
    return data;
}

void GroupPlugin::restoreSettings(const QByteArray& data)
{
    if ( !( data.isEmpty() ) )
    {
        QDataStream stream( &const_cast< QByteArray& >( data ), QIODevice::ReadOnly );

        QByteArray parentData;
        stream >> parentData;
        Plugin::restoreSettings( parentData );
    }
}

KI3U::Task* GroupPlugin::createTask()
{
    return new GroupTask( this );
}

KI3U::TaskCreationWizard* GroupPlugin::taskCreationWizard()
{
    return 0;
}

void GroupPlugin::configure()
{
    KMessageBox::information( 0,
                              tr( "The Group plugin currently has no configuration." ),
                              tr( "Group Configuration" ) );
}


}

}
