/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "grouptask.h"

#include "groupplugin.h"
#include "taskconfigdialog.h"

#include <datamanager.h>
#include <plugin.h>
#include <solutionmanager.h>
#include <taskmanager.h>

#include <KLocalizedString>

namespace KI3U
{

namespace Plugins
{


class GroupTask::Private
{
    public:

        enum State
        {
            ReadyToRun      = 0,
            Initializing    = 1,
            Running         = 2,
            Finalizing      = 3
        };

        Private( GroupTask *task )
            : m_task( task ),
              m_tasks( QStringList() ),
              m_ignoreNotFound( false ),
              m_ignoreInitializationFail( false ),
              m_ignoreRunFail( false ),
              m_foundTasks( QList< Task* >() ),
              m_state( ReadyToRun )
        {
            
        }

        virtual ~Private()
        {
            
        }

        QByteArray saveSettings()
        {
            QByteArray data;
            QDataStream stream( &data, QIODevice::WriteOnly );

            stream << m_tasks
                   << m_ignoreNotFound
                   << m_ignoreInitializationFail
                   << m_ignoreRunFail;

            return data;
        }

        void restoreSettings( QByteArray data )
        {
            if ( !( data.isEmpty() ) )
            {
                QDataStream stream( &data, QIODevice::ReadOnly );

                stream >> m_tasks
                       >> m_ignoreNotFound
                       >> m_ignoreInitializationFail
                       >> m_ignoreRunFail;
            }
        }


        const QStringList& tasks() const {
            return m_tasks;
        }

        void setTasks( const QStringList& t ) {
            m_tasks = t;
        }
        
        bool ignoreNotFound() const {
            return m_ignoreNotFound;
        }

        void setIgnoreNotFound( bool inf ) {
            m_ignoreNotFound = inf;
        }

        bool ignoreInitializationFail() const {
            return m_ignoreInitializationFail;
        }

        void setIgnoreInitializationFail( bool iif ) {
            m_ignoreInitializationFail = iif;
        }

        bool ignoreRunFail() const {
            return m_ignoreRunFail;
        }

        void setIgnoreRunFail( bool irf ) {
            m_ignoreRunFail = irf;
        }

        const QList< KI3U::Task* >& foundTasks() const {
            return m_foundTasks;
        }

        void setFoundTasks( const QList< KI3U::Task* >& ft ) {
            m_foundTasks = ft;
        }

        KI3U::Plugins::GroupTask::Private::State state() const {
            return m_state;
        }

        void setState( KI3U::Plugins::GroupTask::Private::State s ) {
            m_state = s;
        }
    private:

        GroupTask *m_task;

        QStringList m_tasks;
        bool m_ignoreNotFound;
        bool m_ignoreInitializationFail;
        bool m_ignoreRunFail;

        QList< Task* > m_foundTasks;
        State          m_state;
        
};



GroupTask::GroupTask( GroupPlugin* parent)
    : Task(parent),
      d( new Private( this ) )
{

}

GroupTask::~GroupTask()
{
    delete d;
}


const
QString GroupTask::id() const
{
    return GroupPlugin::ID;
}

QByteArray
GroupTask::saveSettings()
{
    QByteArray data;
    QDataStream stream( &data, QIODevice::WriteOnly );

    stream << Task::saveSettings()
           << d->saveSettings();

    return data;
}

void
GroupTask::restoreSettings(const QByteArray& data)
{
    if ( !( data.isEmpty() ) )
    {
        QDataStream stream( &const_cast< QByteArray& >( data ), QIODevice::ReadOnly );

        QByteArray parentData, ownData;
        stream >> parentData >> ownData;
        if ( !( parentData.isEmpty() ) )
        {
            Task::restoreSettings( parentData );
        }
        if ( !( ownData.isEmpty() ) )
        {
            d->restoreSettings( ownData );
        }
    }
}

bool
GroupTask::initialize()
{
    d->setState( Private::Initializing );
    emit statusChanged( i18n( "Searching for tasks..." ), Task::Information );
    emit progressChanged( -1 );
    QList< Task* > found;
    foreach ( QString taskId, d->tasks() )
    {
        Task *task = plugin()->solutionManager()->dataManager()->tasks()->taskById( taskId );
        if ( task )
        {
            TaskMeta* meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( task );
            if ( found.contains( task ) )
            {
                emit statusChanged( i18n( "Task %1 already found. Will NOT add twice.", meta->name() ),
                                    Task::Warning );
            } else
            {
                found.append( task );
                emit statusChanged( i18n( "Found task %1", meta->name() ), Task::Information );
            }
        } else
        {
            emit statusChanged( i18n( "Task %1 not found.", taskId ),
                                ignoreNotFound() ? Task::Warning : Task::Error );
            if ( !( ignoreNotFound() ) )
            {
                d->setState( Private::ReadyToRun );
                return false;
            }
        }
    }

    QList< Task* > initialized;
    foreach( Task* task, found )
    {
        if ( task->initialize() )
        {
            initialized.append( task );
        } else
        {
            TaskMeta *meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( task );
            emit statusChanged( i18n( "Failed to initialize task %1", meta->name() ),
                                ignoreInitializationFail() ? Task::Warning : Task::Error );
            if ( !( ignoreInitializationFail() ) )
            {
                while ( initialized.count() > 0 )
                {
                    Task* t = initialized.last();
                    initialized.pop_back();
                    t->finalize();
                }
                d->setState( Private::ReadyToRun );
                return false;
            }
        }
    }

    if ( initialized.count() == 0 )
    {
        d->setState( Private::ReadyToRun );
        return false;
    }

    d->setFoundTasks( initialized );
    return true;
}

bool
GroupTask::run()
{
    return beginRunTask( d->foundTasks().first() );
}

bool
GroupTask::finalize()
{
    QList< Task* > tasks = d->foundTasks();
    while ( tasks.count() > 0 )
    {
        Task* t = tasks.last();
        tasks.pop_back();
        TaskMeta* meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( t );
        if ( t->finalize() )
        {
            emit statusChanged( i18n( "Successfully finalized task %1", meta->name() ),
                                Task::Information );
        } else
        {
            emit statusChanged( i18n( "Failed to finalize task %1", meta->name() ),
                                Task::Information );
        }
    }
    emit statusChanged( i18n( "Finished!" ), Task::Information );
    return true;
}

void
GroupTask::configure()
{
    TaskConfigDialog dialog( this );
    if ( dialog.exec() == KDialog::Accepted )
    {
        d->setTasks( dialog.tasks() );
        d->setIgnoreNotFound( dialog.ignoreNotFound() );
        d->setIgnoreInitializationFail( dialog.ignoreInitializationFail() );
        d->setIgnoreRunFail( dialog.ignoreRunFail() );
    }
}

KI3U::TaskEntry*
GroupTask::data()
{
    return KI3U::Task::data();
}

const
QStringList& GroupTask::tasks()
{
    return d->tasks();
}

bool
GroupTask::ignoreNotFound()
{
    return d->ignoreNotFound();
}

bool
GroupTask::ignoreInitializationFail()
{
    return d->ignoreInitializationFail();
}

bool
GroupTask::ignoreRunFail()
{
    return d->ignoreRunFail();
}

bool
GroupTask::beginRunTask(Task* t )
{
    connect( t, SIGNAL(runFinished(bool)), this, SLOT(taskFinished(bool)) );
    connect( t, SIGNAL(progressChanged(int)), this, SLOT(taskProgess(int)) );
    connect( t, SIGNAL(statusChanged(QString,Task::StatusMessageType)), this, SLOT(taskStatusChanged(QString,Task::StatusMessageType)) );

    TaskMeta* meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( t );
    if ( t->run() )
    {
        emit statusChanged( i18n( "Started task %1", meta->name() ), Task::Information );
    } else
    {
        emit statusChanged( i18n( "Failed to start task %1", meta->name() ),
                            ignoreRunFail() ? Task::Warning : Task::Error );
        if ( !( ignoreRunFail() ) )
        {
            endRunTask( t );
            return false;
        }
    }
    int max = d->foundTasks().count() * 100;
    int min = d->foundTasks().indexOf( t ) * 100;
    if ( max > 0 )
    {
        emit progressChanged( ( min * 100 ) / max );
    }
    return true;
}

void
GroupTask::endRunTask(Task* t)
{
    disconnect( t, SIGNAL(runFinished(bool)), this, SLOT(taskFinished(bool)) );
    disconnect( t, SIGNAL(progressChanged(int)), this, SLOT(taskProgess(int)) );
    disconnect( t, SIGNAL(statusChanged(QString,Task::StatusMessageType)), this, SLOT(taskStatusChanged(QString,Task::StatusMessageType)) );
}

Task*
GroupTask::findNextTask(Task* t)
{
    int index = d->foundTasks().indexOf( t );
    if ( index >= 0 && index < d->foundTasks().count() - 1 )
    {
        return d->foundTasks().at( index + 1 );
    }
    return 0;
}


void
GroupTask::taskFinished(bool success)
{
    Task* task = dynamic_cast< Task* >( sender() );
    if ( task )
    {
        TaskMeta* meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( task );
        endRunTask( task );
        if ( success )
        {
            emit statusChanged( i18n( "Task %1 finished successfully", meta->name() ),
                                Task::Information );
        } else
        {
            emit statusChanged( i18n( "Task %1 finished with errors", meta->name() ),
                                ignoreRunFail() ? Task::Warning : Task::Error );
            if ( !( ignoreRunFail() ) )
            {
                emit runFinished( false );
                return;
            }
        }
        Task* next = findNextTask( task );
        if ( next )
        {
            if ( !( beginRunTask( next ) ) )
            {
                emit runFinished( false );
            }
        } else
        {
            emit runFinished( true );
        }
    } else
    {
        emit statusChanged( i18n( "Unknown problem: A non-Task send us a finished signal!" ), Task::Error );
        emit runFinished( false );
    }
}

void
GroupTask::taskProgess(int progress)
{
    Task* task = dynamic_cast< Task* >( sender() );
    if ( task )
    {
        int max = d->foundTasks().count() * 100;
        int min = d->foundTasks().indexOf( task ) * 100;
        if ( max > 0 )
        {
            if ( progress >= 0 )
            {
                emit progressChanged( ( ( min + progress ) * 100 ) / max );
            } else
            {
                emit progressChanged( ( min * 100 ) / max );
            }
        }
    }
}

void
GroupTask::taskStatusChanged(const QString msg, Task::StatusMessageType type)
{
    Task* task = dynamic_cast< Task* >( sender() );
    if ( task )
    {
        TaskMeta* meta = plugin()->solutionManager()->dataManager()->tasks()->taskMeta( task );
        emit statusChanged( QString( "%1: %2" ).arg( meta->name() ).arg( msg ), type );
    }
}



}

}
