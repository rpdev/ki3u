/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GROUPTASK_H
#define GROUPTASK_H

#include <task.h>

namespace KI3U
{

namespace Plugins
{

class GroupPlugin;

class GroupTask : public Task
{
    Q_OBJECT

    public:

        GroupTask( GroupPlugin* parent = 0);
        virtual ~GroupTask();
        
        const QString id() const;
        QByteArray saveSettings();
        void restoreSettings(const QByteArray& data);
        bool initialize();
        bool run();
        bool finalize();
        void configure();
        TaskEntry* data();

        const QStringList& tasks();
        bool ignoreNotFound();
        bool ignoreInitializationFail();
        bool ignoreRunFail();

    private:

        class Private;
        Private *d;

        bool beginRunTask( Task* t );
        void endRunTask( Task* t );
        Task *findNextTask( Task* t );

    private slots:

        void taskFinished( bool success );
        void taskProgess( int progress );
        void taskStatusChanged( const QString msg, Task::StatusMessageType type );
};

}

}
#endif // GROUPTASK_H
