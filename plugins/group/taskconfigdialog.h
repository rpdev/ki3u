/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2009, 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TASKCONFIGDIALOG_H
#define TASKCONFIGDIALOG_H

#include <KPageDialog>

namespace KI3U
{

namespace Plugins
{

class GroupTask;

class TaskConfigDialog : public KPageDialog
{

    Q_OBJECT

    public:

    TaskConfigDialog( GroupTask* task, QWidget* parent = 0, Qt::WFlags flags = 0 );
    virtual ~TaskConfigDialog();

    const QStringList tasks() const;
    bool ignoreNotFound() const;
    bool ignoreInitializationFail() const;
    bool ignoreRunFail() const;

    private:

        class Private;
        Private* d;

        GroupTask* m_task;

        void applySettings();

    private slots:

        void moveToRun();
        void moveToNotRun();
};

}

}

#endif // TASKCONFIGDIALOG_H
