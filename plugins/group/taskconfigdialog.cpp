/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2009, 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "taskconfigdialog.h"

#include "ui_tasksetup.h"

#include "grouptask.h"

#include "datamanager.h"
#include "solutionmanager.h"
#include "taskmanager.h"
#include "plugin.h"

#include <KIcon>


namespace KI3U
{

namespace Plugins
{

class TaskConfigDialog::Private
{

    public:

        Private( TaskConfigDialog* dialog )
            : m_dialog( dialog )
        {
            
        }

        virtual ~Private()
        {
            
        }

        Ui::TaskSetup* taskSetupPage;

    private:

        TaskConfigDialog* m_dialog;

};


TaskConfigDialog::TaskConfigDialog(GroupTask* task, QWidget* parent, Qt::WFlags flags)
    : KPageDialog( parent, flags ),
      d( new Private( this ) ),
      m_task( task )
{
    d->taskSetupPage = new Ui::TaskSetup();
    QWidget* taskSetupWidget = new QWidget( this );
    d->taskSetupPage->setupUi( taskSetupWidget );
    connect( d->taskSetupPage->runButton, SIGNAL(clicked()), this, SLOT(moveToRun()) );
    connect( d->taskSetupPage->dontRunButton, SIGNAL(clicked()), this, SLOT(moveToNotRun()) );
    addPage( taskSetupWidget, i18n( "Tasks" ) )->setIcon( KIcon( "document-multiple" ) );

    applySettings();
}

TaskConfigDialog::~TaskConfigDialog()
{
    delete d;
}

const QStringList
TaskConfigDialog::tasks() const
{
    QStringList result;
    for ( int i = 0; i < d->taskSetupPage->runList->count(); i++ )
    {
        result.append( d->taskSetupPage->runList->item( i )->data( Qt::UserRole ).toString() );
    }
    return result;
}

bool
TaskConfigDialog::ignoreNotFound() const
{
    return d->taskSetupPage->ignoreNotFound->isChecked();
}

bool
TaskConfigDialog::ignoreInitializationFail() const
{
    return d->taskSetupPage->ignoreInitializationFail->isChecked();
}

bool TaskConfigDialog::ignoreRunFail() const
{
    return d->taskSetupPage->ignoreRunFail->isChecked();
}

void TaskConfigDialog::applySettings()
{
    d->taskSetupPage->runList->clear();
    d->taskSetupPage->availableList->clear();
    
    foreach ( QString id, m_task->tasks() )
    {
        Task *t = m_task->plugin()->solutionManager()->dataManager()->tasks()->taskById( id );
        if ( t )
        {
            QListWidgetItem* item = new QListWidgetItem();
            TaskMeta* meta = m_task->plugin()->solutionManager()->dataManager()->tasks()->taskMeta( t );
            const PluginMeta* pmeta = m_task->plugin()->solutionManager()->pluginMeta( t->plugin() );
            item->setText( meta->name() );
            item->setIcon( QIcon( pmeta->icon() ) );
            item->setData( Qt::UserRole, id );
            d->taskSetupPage->runList->addItem( item );
        }
    }

    foreach ( Task* t, m_task->plugin()->solutionManager()->dataManager()->tasks()->tasks() )
    {
        if ( !( m_task->tasks().contains( t->objectName() ) ) )
        {
            TaskMeta* meta = m_task->plugin()->solutionManager()->dataManager()->tasks()->taskMeta( t );
            const PluginMeta* pmeta = m_task->plugin()->solutionManager()->pluginMeta( t->plugin() );
            QListWidgetItem* item = new QListWidgetItem();
            item->setText( meta->name() );
            item->setIcon( QIcon( pmeta->icon() ) );
            item->setData( Qt::UserRole, t->objectName() );
            d->taskSetupPage->availableList->addItem( item );
        }
    }

    d->taskSetupPage->ignoreNotFound->setChecked( m_task->ignoreNotFound() );
    d->taskSetupPage->ignoreInitializationFail->setChecked( m_task->ignoreInitializationFail() );
    d->taskSetupPage->ignoreRunFail->setChecked( m_task->ignoreRunFail() );
}

void
TaskConfigDialog::moveToRun()
{
    if ( d->taskSetupPage->availableList->currentItem() )
    {
        d->taskSetupPage->runList->addItem( d->taskSetupPage->availableList->takeItem( d->taskSetupPage->availableList->currentIndex().row() ) );
    }
}

void
TaskConfigDialog::moveToNotRun()
{
    if ( d->taskSetupPage->runList->currentItem() )
    {
        d->taskSetupPage->availableList->addItem( d->taskSetupPage->runList->takeItem( d->taskSetupPage->runList->currentIndex().row() ) );
    }
}


}

}
