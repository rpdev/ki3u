/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef RSYNCPLUGIN_H
#define RSYNCPLUGIN_H

#include <plugin.h>

namespace KI3U
{

namespace Plugins
{
    
class RsyncPlugin : public Plugin
{

    Q_OBJECT

    public:

        static const QString ID;

        RsyncPlugin( QObject *&parent, const QList<QVariant> &args  = QList<QVariant>() );
        virtual ~RsyncPlugin();
        
        const QString id() const;
        QByteArray saveSettings();
        void restoreSettings(const QByteArray& data);
        KI3U::Task* createTask();
        KI3U::TaskCreationWizard* taskCreationWizard();
        void configure();

        inline const QString rsyncExecutable();

    private:

        class RsyncPluginPrivate;
        RsyncPluginPrivate *d;
};

}

}

#endif // RSYNCPLUGIN_H
