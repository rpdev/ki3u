/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "rsyncplugin.h"

#include "rsynctask.h"

#include <solutionmanager.h>

namespace KI3U
{

namespace Plugins
{

KI3U_PLUGIN_EXPORT( RsyncPlugin )

class RsyncPlugin::RsyncPluginPrivate
{
    public:

        RsyncPluginPrivate( RsyncPlugin *plugin )
            : m_plugin( plugin ),
              m_rsyncExecutable( "rsync" )
        {
        }

        const QString rsyncExecutable()
        {
            return m_rsyncExecutable;
        }

        void setRsyncExecutable( const QString exe )
        {
            m_rsyncExecutable = exe;
        }
        
    private:

        RsyncPlugin *m_plugin;

        QString m_rsyncExecutable;
};

const QString RsyncPlugin::ID = "net.rpdev.ki3u.plugins.rsyncplugin";

RsyncPlugin::RsyncPlugin( QObject *&parent, const QList<QVariant> &args)
    : Plugin( dynamic_cast< SolutionManager* >( parent ), args ),
      d( new RsyncPluginPrivate( this ) )
{
}

RsyncPlugin::~RsyncPlugin()
{
    delete d;
}

const QString RsyncPlugin::id() const
{
    return ID;
}

QByteArray RsyncPlugin::saveSettings()
{
    return KI3U::Plugin::saveSettings();
}

void RsyncPlugin::restoreSettings(const QByteArray& data)
{
    KI3U::Plugin::restoreSettings(data);
}

KI3U::Task* RsyncPlugin::createTask()
{
    return new RsyncTask( this );
}

KI3U::TaskCreationWizard* RsyncPlugin::taskCreationWizard()
{
    return KI3U::Plugin::taskCreationWizard();
}

void RsyncPlugin::configure()
{
    KI3U::Plugin::configure();
}


const QString RsyncPlugin::rsyncExecutable()
{
    return d->rsyncExecutable();
}

}

}