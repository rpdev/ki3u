/* ============================================================
*
* This file is a part of the KI3U project
*
* Copyright (C) 2009 by RPdev
*
*
* This program is free software; you can redistribute it
* and/or modify it under the terms of the GNU General
* Public License as published by the Free Software Foundation;
* either version 3, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* ============================================================ */

#include "rsyncbackup.h"

#include <KDebug>


RSyncConfigWindow::RSyncConfigWindow(QWidget* parent): KDialog(parent)
{
  QWidget* widget = new QWidget(this);
  configWindow.setupUi(widget);
  setMainWidget(widget);
  setModal(true);
  setCaption("RSync Backup Configuration");
  // Would it be of any use to allow backing up single files...?
  configWindow.srcLocation->setMode(KFile::Directory);
  configWindow.dstLocation->setMode(KFile::Directory);
  // We do not want to rely on the designer setting the "correct" page himself ;)
  configWindow.tabWidget->setCurrentIndex(0);
}








RSyncBackup::RSyncBackup() : configWindow(), process()
{
  srcLocation = "/home/";
  dstLocation = "/tmp/ki3u-rsync/";
  showProgress = true;
  rsyncExe = "rsync";
  useChecksums = false;
  updateFiles = false;
  deleteFiles = true;
  preserveHardLinks = false;
  preserveACLs = false;
  preserveXattrs = false;
  preserveTime = true;
  preservePermissions = true;
  preserveOwner = false;
  preserveGroup = false;

  procIsRunning = false;

  process.setOutputChannelMode(KProcess::SeparateChannels);

  connect(&process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(onProcessFinished(int, QProcess::ExitStatus)));
  connect(&process, SIGNAL(readyReadStandardOutput()), this, SLOT(onProcessOut()));
  connect(&process, SIGNAL(readyReadStandardError()), this, SLOT(onProcessError()));
  connect(&configWindow, SIGNAL(okClicked()), this, SLOT(applyConfiguration()));
  connect(configWindow.configWindow.swapSrcDstButton, SIGNAL(clicked(bool)), this, SLOT(swapSrcDst(bool)));
  connect(configWindow.configWindow.setDefaultRSyncButton, SIGNAL(clicked(bool)), this, SLOT(setDefaultRSyncExe(bool)));
}


RSyncBackup::~RSyncBackup()
{
  if (running())
    stop();
}



void RSyncBackup::configure()
{
  // General settings
  configWindow.configWindow.srcLocation->setUrl(srcLocation);
  configWindow.configWindow.dstLocation->setUrl(dstLocation);
  configWindow.configWindow.showProgress->setChecked(showProgress);
  // Backend settings
  configWindow.configWindow.rsyncExePath->setUrl(rsyncExe);
  // Mode settings
  configWindow.configWindow.useChecksumSkipping->setChecked(useChecksums);
  configWindow.configWindow.updateOnly->setChecked(updateFiles);
  configWindow.configWindow.deleteFiles->setChecked(deleteFiles);
  // Preserve settings
  configWindow.configWindow.preserveHardlinks->setChecked(preserveHardLinks);
  configWindow.configWindow.preserveACLs->setChecked(preserveACLs);
  configWindow.configWindow.preserveExtendedAttrs->setChecked(preserveXattrs);
  configWindow.configWindow.preserveTime->setChecked(preserveTime);
  configWindow.configWindow.preservePermissions->setChecked(preservePermissions);
  configWindow.configWindow.preserveOwner->setChecked(preserveOwner);
  configWindow.configWindow.preserveGroup->setChecked(preserveGroup);
  configWindow.show();
}


bool RSyncBackup::run()
{
  if (!procIsRunning) {

    inDryRun = true;
    initialCount = 1;
    dryRunOut.clear();

    if (!showProgress)
    {
      inDryRun = false;
    }

    setupProcess();
    process.start();
    if (process.pid() > 0)
    {
      procIsRunning = true;
      emit started();
      emit progress();
    }
    return procIsRunning;
  }
  return false;
}


bool RSyncBackup::running()
{
  return procIsRunning;
}

void RSyncBackup::suspend()
{
  //TODO: Implement suspending
}

bool RSyncBackup::suspended()
{
  return false;
}

void RSyncBackup::resume()
{
  //TODO: Implement resuming
}

void RSyncBackup::stop()
{
  if (running())
  {
    process.kill();
    procIsRunning = false;
    emit finished();
  }
}

int RSyncBackup::progressValue()
{
  if (inDryRun || !showProgress)
  {
    return -1;
  }
  else
  {
    if (initialCount == 0)
    {
      initialCount = 1;
    }
    return ((initialCount - dryRunOut.count()) * 100) / initialCount;
  }
}

void RSyncBackup::saveConfiguration(KConfigGroup* cfg)
{
  // General settings
  cfg->writeEntry("src", srcLocation);
  cfg->writeEntry("dst", dstLocation);
  cfg->writeEntry("showProgress", showProgress);
  // Backend settings
  cfg->writeEntry("backend", rsyncExe);
  // Mode settings
  cfg->writeEntry("useCheckSums", useChecksums);
  cfg->writeEntry("updateFiles", updateFiles);
  cfg->writeEntry("deleteFiles", deleteFiles);
  // Preserve settings
  cfg->writeEntry("preserveHardLinks", preserveHardLinks);
  cfg->writeEntry("preserveACLs", preserveACLs);
  cfg->writeEntry("preserveXattrs", preserveXattrs);
  cfg->writeEntry("preserveTime", preserveTime);
  cfg->writeEntry("preservePermissions", preservePermissions);
  cfg->writeEntry("preserveOwner", preserveOwner);
  cfg->writeEntry("preserveGroup", preserveGroup);
}

bool RSyncBackup::loadConfiguration(KConfigGroup* cfg)
{
  // General settings
  srcLocation = cfg->readEntry("src", srcLocation);
  dstLocation = cfg->readEntry("dst", dstLocation);
  showProgress = cfg->readEntry<bool>("showProgress", showProgress);
  // Backend settings
  rsyncExe = cfg->readEntry("backend", "rsync");
  // Mode settings
  useChecksums = cfg->readEntry<bool>("useCheckSums", useChecksums);
  updateFiles = cfg->readEntry<bool>("updateFiles", updateFiles);
  deleteFiles = cfg->readEntry<bool>("deleteFiles", deleteFiles);
  // Preserve settings
  preserveHardLinks = cfg->readEntry<bool>("preserveHardLinks", preserveHardLinks);
  preserveACLs = cfg->readEntry<bool>("preserveACLs", preserveACLs);
  preserveXattrs = cfg->readEntry<bool>("preserveXattrs", preserveXattrs);
  preserveTime = cfg->readEntry<bool>("preserveTime", preserveTime);
  preservePermissions = cfg->readEntry<bool>("preservePermissions", preservePermissions);
  preserveOwner = cfg->readEntry<bool>("preserveOwner", preserveOwner);
  preserveGroup = cfg->readEntry<bool>("preserveGroup", preserveGroup);
  return true;
}

void RSyncBackup::setupProcess()
{
  process.clearProgram();
  process << rsyncExe
	  << "-a"  // run in archive mode
	  << "-v"; // be verbosive
  if (inDryRun)
  {
    // enter dry run mode, where we only want
    // to get the number of files to back up finally
    process << "-n";
  }
  // Mode
  if (useChecksums)
  {
    process << "-c";
  }
  if (updateFiles)
  {
    process << "-u";
  }
  if (deleteFiles)
  {
    process << "--delete";
  }
  // Preserve
  if (preserveHardLinks)
  {
    process << "-H";
  }
  if (preserveACLs)
  {
    process << "-A";
  }
  if (preserveXattrs)
  {
    process << "-X";
  }
  if (preserveTime)
  {
    process << "-t";
  }
  if (preservePermissions)
  {
    process << "-p";
  }
  if (preserveOwner)
  {
    process << "-o";
  }
  if (preserveGroup)
  {
    process << "-g";
  }
  // Location
  process << srcLocation
	  << dstLocation;
  kDebug() << process.program();
}

void RSyncBackup::onProcessFinished(int , QProcess::ExitStatus )
{
  if (inDryRun)
  {
    initialCount = dryRunOut.count();
    inDryRun = false;
    setupProcess();
    process.start();
    procIsRunning = process.pid() > -1;
  }
  else
  {
    procIsRunning = false;
  }
  if (!procIsRunning)
  {
    emit finished();
  }
}

void RSyncBackup::onProcessOut()
{
  process.setReadChannel(QProcess::StandardOutput);
  while (process.canReadLine())
  {
    QByteArray buf = process.readLine();
    QString line = QString::fromLocal8Bit(buf);
    configWindow.configWindow.lastRunLog->append(
			"<span style='color: black'>" + line + "</span>");
    if (inDryRun)
    {
      dryRunOut.append(line);
    }
    else
    {
      if (dryRunOut.contains(line))
      {
	dryRunOut.removeAll(line);
	emit progress();
      }
    }
  }
}

void RSyncBackup::onProcessError()
{
  process.setReadChannel(QProcess::StandardError);
  while (process.canReadLine())
  {
    QByteArray buf = process.readLine();
    configWindow.configWindow.lastRunLog->append("<span style='color: red'>" + QString::fromLocal8Bit(buf) + "</span>");
  }
}

void RSyncBackup::applyConfiguration()
{
  // General settings
  srcLocation = configWindow.configWindow.srcLocation->text();
  dstLocation = configWindow.configWindow.dstLocation->text();
  showProgress = configWindow.configWindow.showProgress->isChecked();
  // Backend settings
  rsyncExe = configWindow.configWindow.rsyncExePath->text();
  // Mode settings
  useChecksums = configWindow.configWindow.useChecksumSkipping->isChecked();
  updateFiles = configWindow.configWindow.updateOnly->isChecked();
  deleteFiles = configWindow.configWindow.deleteFiles->isChecked();
  // Preserve settings
  preserveHardLinks = configWindow.configWindow.preserveHardlinks->isChecked();
  preserveACLs = configWindow.configWindow.preserveACLs->isChecked();
  preserveXattrs = configWindow.configWindow.preserveExtendedAttrs->isChecked();
  preserveTime = configWindow.configWindow.preserveTime->isChecked();
  preservePermissions = configWindow.configWindow.preservePermissions->isChecked();
  preserveOwner = configWindow.configWindow.preserveOwner->isChecked();
  preserveGroup = configWindow.configWindow.preserveGroup->isChecked();
}

void RSyncBackup::swapSrcDst(bool)
{
  QString tmp = configWindow.configWindow.srcLocation->text();
  configWindow.configWindow.srcLocation->setUrl(configWindow.configWindow.dstLocation->text());
  configWindow.configWindow.dstLocation->setUrl(tmp);
}


void RSyncBackup::setDefaultRSyncExe(bool )
{
  configWindow.configWindow.rsyncExePath->setUrl(QString("rsync"));
}





Backup* createRSyncBackup()
{
  return new RSyncBackup();
}

#include "rsyncbackup.moc"