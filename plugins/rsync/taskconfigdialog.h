/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2009, 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef TASKCONFIGDIALOG_H
#define TASKCONFIGDIALOG_H

#include <KPageDialog>

namespace KI3U
{
    
namespace Plugins
{

class RsyncTask;

class TaskConfigDialog : public KPageDialog
{

    Q_OBJECT

    public:

        TaskConfigDialog( RsyncTask *task, QWidget* parent = 0, Qt::WFlags flags = 0 );
        virtual ~TaskConfigDialog();

        int transferMode() const;
        const QString protocol() const;
        const QString host() const;
        quint16 port() const;
        const QString user() const;
        const QString pass() const;
        bool keepPassword() const;
        const QString source() const;
        const QString destination() const;
        const QString sshAskPass() const;
        const QString sshUser() const;
        const QString rsyncModule() const;
        bool dryRun() const;

        bool archiveMode() const;
        bool recurse() const;
        bool directories() const;
        bool skipBasedOnChecksums() const;
        bool skipOnFileSize() const;
        bool update() const;
        bool updateInPlace() const;
        bool delayUpdates() const;
        bool appendData() const;
        bool appendVerify() const;
        bool dontCrossFileSystems() const;
        bool dontCreateNewFiles() const;
        bool skipExistingFiles() const;
        bool removeExtraneousFiles() const;
        int deleteMode() const;
        bool deleteExcluded() const;
        bool deleteIgnoreErrors() const;
        bool forceDeleteDirectories() const;
        quint32 maxDelete() const;
        quint32 modificationTimeWindow() const;
        bool ignoreTimes() const;
        bool backup() const;
        const QString backupDir() const;
        const QString backupSuffix() const;
        bool copySymLinksAsIs() const;
        bool resolveSymLinks() const;
        bool resolveUnsafeSymLinks() const;
        bool ignoreOuterSymLinks() const;
        bool resolveDirSymLinks() const;
        bool treatDirSymLinksAsDir() const;
        bool preserveHardLinks() const;
        bool preservePermissions() const;
        bool preserveExecutability() const;
        bool preserveACLs() const;
        bool preserveExtendedAttributes() const;
        bool preserveOwner() const;
        bool preserveGroup() const;
        bool preserveDevices() const;
        bool preserveSpecialFiles() const;
        bool preserveModificationTimes() const;
        bool dontPreserveDirectoryModTimes() const;
        bool numericIds() const;
        bool autoIgnoreCVSStyle() const;
        const QStringList excludePattern() const;
        const QString excludePatternFile() const;
        const QStringList includePattern() const;
        const QString includePatternFile() const;
        const QString tempDirectory() const;
        const QString logFile() const;
        const QString sourceListFile() const;

        bool copyWholeFiles() const;
        quint32 blockSize() const;
        quint64 maxTransferSize() const;
        quint64 minTransferSize() const;
        bool keepPartiallyTransferredFiles() const;
        const QString partialDir() const;
        int ipProtocol() const;
        bool compress() const;
        quint8 compressLevel() const;
        const QString skipCompressSuffixes() const;
        quint32 bandwidthLimitInKBPS() const;
        quint32 timeout() const;
        quint32 daemonConnectionTimeout() const;

    private:

        class TaskConfigDialogPrivate;
        TaskConfigDialogPrivate *d;

        RsyncTask *m_task;

        void setupGui();
        void applyTaskSettings();

        void setAccountEnabledStates();

    private slots:

        void transferTypeChanged( int );
        void sshSelected( bool );
        void rsyncSelected( bool );
};

}

}

#endif // TASKCONFIGDIALOG_H
