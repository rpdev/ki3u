/* ============================================================
*
* This file is a part of the KI3U project
*
* Copyright (C) 2009 by RPdev
*
*
* This program is free software; you can redistribute it
* and/or modify it under the terms of the GNU General
* Public License as published by the Free Software Foundation;
* either version 3, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* ============================================================ */

#ifndef RSYNCBACKUP_H
#define RSYNCBACKUP_H

/** @file
This file provides an interface to the rsync backup program.
Rsync can be used to backup and synchronize your files locally or remote.
To speed up the process, it uses different methods to determine, whether a
file is up-to-date in the backup location or not.
When transferring to remote locations, rsync uses a delta-transfer mode to
speed up and safe bandwidth.
@author Martin Höher <martin@rpdev.net>
*/

#include <KApplication>
#include <KLocale>
#include <KConfigGroup>
#include <KDialog>
#include <KProcess>

#include "Backup.h"

#include "ui_rsyncconfigwindow.h"

//! RSync configuration window class
/**
This class provides the user interface to configure
the rsync backup task.
*/
class RSyncConfigWindow : public KDialog
{

  Q_OBJECT

  public:
    Ui::RSyncConfigWindow configWindow;

  public:
    RSyncConfigWindow(QWidget* parent = 0);
};

//! rsync backup interface
/**
This class is an interface to the rsync backup program.
*/
class RSyncBackup : public Backup
{

  Q_OBJECT

  private:
    RSyncConfigWindow configWindow;

    QString srcLocation;
    QString dstLocation;
    bool showProgress;
    QString rsyncExe;
    bool useChecksums;
    bool updateFiles;
    bool deleteFiles;
    bool preserveHardLinks;
    bool preserveACLs;
    bool preserveXattrs;
    bool preserveTime;
    bool preservePermissions;
    bool preserveOwner;
    bool preserveGroup;

    KProcess process;
    bool procIsRunning;

    bool inDryRun;
    QStringList dryRunOut;
    int initialCount;

  public:

    RSyncBackup();
    virtual ~RSyncBackup();

    static QString ID()
    {
      return "BACKUP_SOLUTION_RSYNC";
    }

     //! Configure the backup
    /**
    This should show a configuration dialog where one can set up the
    backup.
    */
    void configure();

    //! Start the backup process
    /**
    Starts the backup process. The process should be run in a separate thread, so that the user
    can continue working.
    */
    bool run();

    //! Returns the current state.
    /**
    This returns, whether the backup process is still running or not.
    */
    bool running();

    //! Suspend the backup
    /**
    Suspends the backup, so that the user can continue later.
    */
    void suspend();

    //! Suspended state
    /**
    Returns true, if the backup is currently suspended, otherwise false.
    */
    bool suspended();

    //! Resume the backup
    /**
    Resumes a previousely suspended backup process.
    */
    void resume();

    //! Stop the backup
    /**
    Stops the running backup process.
    */
    void stop();

    //! Returns current progress
    /**
    This returns a number indicating the current progress.
    */
    int progressValue();

    //! Save the plugin configuration.
    /**
    This method should be used to write the backup task's
    configuration using KDE's internal configuration system.
    @p cfg provides a high level wrapper, the backup task can use to easily
    store and load its settings.
    */
    void saveConfiguration(KConfigGroup* cfg);

    //! Load the task's configuration.
    /**
    This method should use the configuration object @p cfg to load its settings.
    @return True if everything was OK, otherwise false.
    */
    bool loadConfiguration(KConfigGroup* cfg);

  private:

    //! Set up the process object.
    /**
    This will set up the process object with the user specified parameters.
    */
    void setupProcess();

  private slots:

    //! Act when process finished
    /**
    This must be triggered when the process finished.
    It will emit further signals to the main program, so
    that it can update its display status.
    */
    void onProcessFinished(int, QProcess::ExitStatus);

    //! Read process output
    /**
    Read available output from stdout.
    */
    void onProcessOut();

    //! Read process error.
    /**
    Read available data from process stderr->
    */
    void onProcessError();

    //! Apply the configuration.
    /**
    This slot is connected to the "OK" button in the configuration dialog.
    It will copy the values given there.
    */
    void applyConfiguration();

    //! Swap source and destination.
    /**
    Swaps the source and the destination location of the backup.
    */
    void swapSrcDst(bool);

    //! Sets the default rsync executable.
    /**
    Restores the default rsync executable path.
    */
    void setDefaultRSyncExe(bool);

};




Backup* createRSyncBackup();

#endif