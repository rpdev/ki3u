/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef RSYNCTASK_H
#define RSYNCTASK_H

#include <task.h>

#include <KProcess>

namespace KI3U
{

namespace Plugins
{

/**
 * @brief Backup task using rsync as backend.
 */
class RsyncTask : public Task
{

    Q_OBJECT

    public:

        /**
         * @brief Type of source and destination.
         *
         * This type is used to indicate the type of the source and destination
         * locations.
         */
        enum SourceSinkType
        {
            LocalOnly = 0,          //!< Both source and destination are mounted in the local file system.
            SourceRemote = 1,       //!< The source is a remote host. 
            DestinationRemote = 2   //!< The destination is a remote host.
        };

        /**
         * @brief When to delete extraneous files?
         *
         * This type is used to indicate, when to delete extraneous files on
         * the receiver.
         */
        enum DeleteMode
        {
            DeleteBefore = 0,   //  --delete-before         receiver deletes before transfer (default)
            DeleteDuring = 1,  // --delete-during         receiver deletes during xfer, not before
            DeleteDelay = 2, // --delete-delay          find deletions during, delete after
            DeleteAfter = 3 // --delete-after          receiver deletes after transfer, not before
        };

        /**
         * @brief IP Protocol version to prefer.
         *
         * This can be used to explicitly prefer an IP protocol.
         */
        enum IPProtocol
        {
            NoPreference = 0,
            PreferIPv4 = 1, // -4, --ipv4                  prefer IPv4
            PreferIPv6 = 2 // -6, --ipv6                  prefer IPv6
        };
        
        RsyncTask(Plugin* parent = 0);
        virtual ~RsyncTask();
        
        const QString id() const;
        QByteArray saveSettings();
        void restoreSettings(const QByteArray& data);
        bool initialize();
        bool run();
        bool finalize();
        void configure();
        KI3U::TaskEntry* data();
        void prepareRemove();

        const QString source() const;
        const QString destination() const;
        SourceSinkType transferMode() const;
        const QString user() const;
        const QString pass() const;
        const QString host() const;
        bool storePassword() const;
        quint16 port() const;
        const QString protocol() const;
        const QString sshAskPass() const;
        const QString sshUser() const;
        const QString rsyncModule() const;

        bool skipBasedOnChecksums() const;
        bool archiveMode() const;
        bool recurse() const;
        bool backup() const;
        const QString backupDir() const;
        const QString backupSuffix() const;
        bool update() const;
        bool updateInPlace() const;
        bool appendData() const;
        bool appendVerify() const;
        bool directories() const;
        bool copySymLinksAsIs() const;
        bool resolveSymLinks() const;
        bool resolveUnsafeLinks() const;
        bool ignoreOuterSymLinks() const;
        bool resolveDirSymLinks() const;
        bool treatDirSymLinkAsDir() const;
        bool preserveHardLinks() const;
        bool preservePermissions() const;
        bool preserveExecutability() const;
        bool preserveACLs() const;
        bool preserveExtendedAttributes() const;
        bool preserveOwner() const;
        bool preserveGroup() const;
        bool preserveDevices() const;
        bool preserveSpecialFiles() const;
        bool preserveModificationTimes() const;
        bool dontPreserveDirectoryModTimes() const;
        bool attemtSuperUser() const;
        bool fakeSuperUser() const;
        bool effectiveSparseFiles() const;
        bool dryRun() const;
        bool copyWholeFiles() const;
        bool dontCrossFileSystems() const;
        quint32 blockSize() const;
        const QString remoteShell() const;
        const QString remoteRsyncPath() const;
        bool dontCreateNewFiles() const;
        bool skipExistingFiles() const;
        bool removeSourceAfterSync() const;
        bool removeExtraneousFiles() const;
        DeleteMode deleteMode() const;
        bool deleteExcluded() const;
        bool deleteIgnoreErrors() const;
        bool forceDeleteDirectories() const;
        quint32 maxDelete() const;
        quint64 maxTransferSize() const;
        quint64 minTransferSize() const;
        bool keepPartiallyTransferredFiles() const;
        const QString partialDir() const;
        bool delayUpdates() const;
        bool numericIds() const;
        quint32 timeout() const;
        quint32 daemonConnectionTimeout();
        bool ignoreTimes() const;
        bool skipOnFileSize() const;
        quint32 modificationTimeWindow() const;
        const QString tempDirectory() const;
        bool compress() const;
        quint8 compressLevel() const;
        const QString skipCompressSuffixes() const;
        bool autoIgnoreCVSStyle() const;
        const QStringList excludePattern() const;
        const QString excludePatternFile() const;
        const QStringList includePattern() const;
        const QString includePatternFile() const;
        const QString sourceListFile() const;
        bool protectArgs() const;
        const QString logFile() const;
        quint32 bandwidthLimitInKBPS() const;
        quint32 protocolVersion() const;
        const QString charset() const;
        IPProtocol ipProtocol() const;

    private:

        static const quint32 STORAGE_VERSION_0;
        static const quint32 STORAGE_VERSION;

        class RsyncTaskPrivate;
        RsyncTaskPrivate *d;

    private slots:

        void processFinished( int exitCode, QProcess::ExitStatus exitStatus );
        void readStandardOutput();
        void readStandardError();
        
};

}

}

#endif // RSYNCTASK_H
