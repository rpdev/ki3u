/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "rsynctask.h"

#include "rsyncplugin.h"
#include "taskconfigdialog.h"

#include <datamanager.h>
#include <passwordmanager.h>
#include <solutionmanager.h>

#include <KDebug>
#include <KPasswordDialog>

#include <QDataStream>
#include <QFileInfo>
#include <QStringList>
#include <klocalizedstring.h>

namespace KI3U
{

namespace Plugins
{

class RsyncTask::RsyncTaskPrivate
{
    public:

        RsyncTaskPrivate( RsyncTask *task )
            : m_task( task ),
              m_process( 0 ),
              m_source( QString() ),
              m_destination( QString() ),
              m_transferMode( LocalOnly ),
              m_user( QString() ),
              m_pass( QString() ),
              m_storePassword( false ),
              m_host( QString() ),
              m_port( 0 ),
              m_protocol( QString() ),
              m_sshAskPass( QString( "ksshaskpass" ) ),
              m_sshUser( QString() ),
              m_rsyncModule( QString() ),
              m_skipBasedOnChecksums( false ),
              m_archiveMode( true ),
              m_recurse( false ),
              m_backup( false ),
              m_backupDir( QString() ),
              m_backupSuffix( QString() ),
              m_update( false ),
              m_updateInPlace( false ),
              m_appendData( false ),
              m_appendVerify( false ),
              m_directories( false ),
              m_copySymLinksAsIs( false ),
              m_resolveSymLinks( false ),
              m_resolveUnsafeSymLinks( false ),
              m_ignoreOuterSymLinks( false ),
              m_resolveDirSymLink( false ),
              m_treatDirSymLinkAsDir( false ),
              m_preserveHardLinks( false ),
              m_preservePermissions( false ),
              m_preserveExecutability( false ),
              m_preserveACLs( false ),
              m_preserveExtendedAttributes( false ),
              m_preserveOwner( false ),
              m_preserveGroup( false ),
              m_preserveDevices( false ),
              m_preserveSpecialFiles( false ),
              m_preserveModificationTimes( false ),
              m_dontPreserveDirectoryModTimes( false ),
              m_attemtSuperUser( false ),
              m_fakeSuperUser( false ),
              m_effectiveSparseFiles( false ),
              m_dryRun( false ),
              m_copyWholeFiles( false ),
              m_dontCrossFileSystems( false ),
              m_blockSize( 0 ),
              m_remoteShell( QString() ),
              m_remoteRsyncPath( QString() ),
              m_dontCreateNewFiles( false ),
              m_skipExistingFiles( false ),
              m_removeSourceAfterSync( false ),
              m_removeExtraneousFiles( false ),
              m_deleteMode( RsyncTask::DeleteAfter ),
              m_deleteExcluded( false ),
              m_deleteIgnoreErrors( false ),
              m_forceDeleteDirectories( false ),
              m_maxDelete( 0 ),
              m_maxTransferSize( 0 ),
              m_minTransferSize( 0 ),
              m_keepPartiallyTransferredFiles( false ),
              m_partialDir( QString() ),
              m_delayUpdates( false ),
              m_numericIds( false ),
              m_timeout( 0 ),
              m_daemonConnectionTimeout( 0 ),
              m_ignoreTimes( false ),
              m_skiptOnFileSize( false ),
              m_modificcationTimeWindow( 0 ),
              m_tempDirectory( QString() ),
              m_compress( false ),
              m_compressLevel( 5 ),
              m_skipCompressSuffixes( QString() ),
              m_autoIgnoreCVSStyle( false ),
              m_excludePattern( QStringList() ),
              m_excludePatternFile( QString() ),
              m_includePattern( QStringList() ),
              m_includePatternFile( QString() ),
              m_sourceListFile( QString() ),
              m_protectArgs( false ),
              m_logFile( QString() ),
              m_bandwidthLimitInKBPS( 0 ),
              m_protocolVersion( 0 ),
              m_charset( QString() ),
              m_ipProtocol( RsyncTask::NoPreference )
        {
        }

        QByteArray saveSettings() const
        {
            QByteArray data;
            QDataStream stream( &data, QIODevice::WriteOnly );

            stream << RsyncTaskPrivate::STORAGE_VERSION
                   << m_source
                   << m_destination
                   << static_cast< quint32 >( m_transferMode )
                   << m_user
                   << m_storePassword
                   << m_host
                   << m_port
                   << m_protocol
                   << m_sshAskPass
                   << m_sshUser
                   << m_rsyncModule
                   << m_skipBasedOnChecksums
                   << m_archiveMode
                   << m_recurse
                   << m_backup
                   << m_backupDir
                   << m_backupSuffix
                   << m_update
                   << m_updateInPlace
                   << m_appendData
                   << m_appendVerify
                   << m_directories
                   << m_copySymLinksAsIs
                   << m_resolveSymLinks
                   << m_appendData
                   << m_appendVerify
                   << m_directories
                   << m_copySymLinksAsIs
                   << m_resolveSymLinks
                   << m_resolveUnsafeSymLinks
                   << m_ignoreOuterSymLinks
                   << m_resolveDirSymLink
                   << m_treatDirSymLinkAsDir
                   << m_preserveHardLinks
                   << m_preservePermissions
                   << m_preserveExecutability
                   << m_preserveACLs
                   << m_preserveExtendedAttributes
                   << m_preserveOwner
                   << m_preserveGroup
                   << m_preserveDevices
                   << m_preserveSpecialFiles
                   << m_preserveModificationTimes
                   << m_dontPreserveDirectoryModTimes
                   << m_attemtSuperUser
                   << m_fakeSuperUser
                   << m_effectiveSparseFiles
                   << m_dryRun
                   << m_copyWholeFiles
                   << m_dontCrossFileSystems
                   << m_blockSize
                   << m_remoteShell
                   << m_remoteRsyncPath
                   << m_dontCreateNewFiles
                   << m_skipExistingFiles
                   << m_removeSourceAfterSync
                   << m_removeExtraneousFiles
                   << static_cast< quint32 >( m_deleteMode )
                   << m_deleteExcluded
                   << m_deleteIgnoreErrors
                   << m_forceDeleteDirectories
                   << m_maxDelete
                   << m_maxTransferSize
                   << m_minTransferSize
                   << m_keepPartiallyTransferredFiles
                   << m_partialDir
                   << m_delayUpdates
                   << m_numericIds
                   << m_timeout
                   << m_daemonConnectionTimeout
                   << m_ignoreTimes
                   << m_skiptOnFileSize
                   << m_modificcationTimeWindow
                   << m_tempDirectory
                   << m_compress
                   << m_compressLevel
                   << m_skipCompressSuffixes
                   << m_autoIgnoreCVSStyle
                   << m_excludePattern
                   << m_excludePatternFile
                   << m_includePattern
                   << m_includePatternFile
                   << m_sourceListFile
                   << m_protectArgs
                   << m_logFile
                   << m_bandwidthLimitInKBPS
                   << m_protocolVersion
                   << m_charset
                   << static_cast< quint32 >( m_ipProtocol );
            return data;
        }

        void restoreSettings( QByteArray data )
        {
            QDataStream stream( &data, QIODevice::ReadOnly );

            quint32 storageVersion;

            quint32 transferMode, deleteMode, ipProtocol;
            stream >> storageVersion
                   >> m_source
                   >> m_destination
                   >> transferMode
                   >> m_user
                   >> m_storePassword
                   >> m_host
                   >> m_port
                   >> m_protocol
                   >> m_sshAskPass
                   >> m_sshUser
                   >> m_rsyncModule
                   >> m_skipBasedOnChecksums
                   >> m_archiveMode
                   >> m_recurse
                   >> m_backup
                   >> m_backupDir
                   >> m_backupSuffix
                   >> m_update
                   >> m_updateInPlace
                   >> m_appendData
                   >> m_appendVerify
                   >> m_directories
                   >> m_copySymLinksAsIs
                   >> m_resolveSymLinks
                   >> m_appendData
                   >> m_appendVerify
                   >> m_directories
                   >> m_copySymLinksAsIs
                   >> m_resolveSymLinks
                   >> m_resolveUnsafeSymLinks
                   >> m_ignoreOuterSymLinks
                   >> m_resolveDirSymLink
                   >> m_treatDirSymLinkAsDir
                   >> m_preserveHardLinks
                   >> m_preservePermissions
                   >> m_preserveExecutability
                   >> m_preserveACLs
                   >> m_preserveExtendedAttributes
                   >> m_preserveOwner
                   >> m_preserveGroup
                   >> m_preserveDevices
                   >> m_preserveSpecialFiles
                   >> m_preserveModificationTimes
                   >> m_dontPreserveDirectoryModTimes
                   >> m_attemtSuperUser
                   >> m_fakeSuperUser
                   >> m_effectiveSparseFiles
                   >> m_dryRun
                   >> m_copyWholeFiles
                   >> m_dontCrossFileSystems
                   >> m_blockSize
                   >> m_remoteShell
                   >> m_remoteRsyncPath
                   >> m_dontCreateNewFiles
                   >> m_skipExistingFiles
                   >> m_removeSourceAfterSync
                   >> m_removeExtraneousFiles
                   >> deleteMode
                   >> m_deleteExcluded
                   >> m_deleteIgnoreErrors
                   >> m_forceDeleteDirectories
                   >> m_maxDelete
                   >> m_maxTransferSize
                   >> m_minTransferSize
                   >> m_keepPartiallyTransferredFiles
                   >> m_partialDir
                   >> m_delayUpdates
                   >> m_numericIds
                   >> m_timeout
                   >> m_daemonConnectionTimeout
                   >> m_ignoreTimes
                   >> m_skiptOnFileSize
                   >> m_modificcationTimeWindow
                   >> m_tempDirectory
                   >> m_compress
                   >> m_compressLevel
                   >> m_skipCompressSuffixes
                   >> m_autoIgnoreCVSStyle
                   >> m_excludePattern
                   >> m_excludePatternFile
                   >> m_includePattern
                   >> m_includePatternFile
                   >> m_sourceListFile
                   >> m_protectArgs
                   >> m_logFile
                   >> m_bandwidthLimitInKBPS
                   >> m_protocolVersion
                   >> m_charset
                   >> ipProtocol;
                   m_transferMode = static_cast< SourceSinkType >( transferMode );
                   m_deleteMode = static_cast< DeleteMode >( deleteMode );
                   m_ipProtocol = static_cast< IPProtocol >( ipProtocol );
        }

        KProcess *process()
        {
            return m_process;
        }

        void setProcess( KProcess *process )
        {
            m_process = process;
        }

        const QString& source() const
        {
            return m_source;
        }

        void setSource( const QString& s )
        {
            m_source = s;
        }

        const QString& destination() const
        {
            return m_destination;
        }

        void setDestination( const QString& d )
        {
            m_destination = d;
        }

        KI3U::Plugins::RsyncTask::SourceSinkType transferMode() const
        {
            return m_transferMode;
        }

        void setTransferMode( KI3U::Plugins::RsyncTask::SourceSinkType tm )
        {
            m_transferMode = tm;
        }

        const QString& user() const
        {
            return m_user;
        }

        void setUser( const QString& u )
        {
            m_user = u;
        }

        const QString& pass()
        {
            if ( m_pass.isEmpty() && m_storePassword )
            {
                m_pass = m_task->plugin()->solutionManager()->dataManager()->passwordManager()->password( m_task->objectName() );
            }
            return m_pass;
        }

        void setPass( const QString& p )
        {
            m_pass = p;
            if ( m_storePassword )
            {
                m_task->plugin()->solutionManager()->dataManager()->passwordManager()->setPassword( m_task->objectName(), m_pass );
            }
        }

        bool storePassword()
        {
            return m_storePassword;
        }

        void setStorePassword( bool sp )
        {
            m_storePassword = sp;
            if ( !( m_storePassword ) )
            {
                m_task->plugin()->solutionManager()->dataManager()->passwordManager()->removePassword( m_task->objectName() );
            } else
            {
                m_task->plugin()->solutionManager()->dataManager()->passwordManager()->setPassword( m_task->objectName(), m_pass );
            }
        }

        const QString& host() const
        {
            return m_host;
        }

        void setHost( const QString& h )
        {
            m_host = h;
        }

        quint16 port() const
        {
            return m_port;
        }

        void setPort( quint16 p )
        {
            m_port = p;
        }

        const QString& protocol() const
        {
            return m_protocol;
        }

        void setProtocol( const QString& p )
        {
            m_protocol = p;
        }

        const QString& sshAskPass() const
        {
            return m_sshAskPass;
        }

        void setSshAskPass( const QString& sap )
        {
            m_sshAskPass = sap;
        }

        const QString& sshUser() const
        {
            return m_sshUser;
        }

        void setSshUser( const QString& su )
        {
            m_sshUser = su;
        }

        const QString& rsyncModule() const
        {
            return m_rsyncModule;
        }

        void setRsyncModule( const QString& rm )
        {
            m_rsyncModule = rm;
        }

        bool skipBasedOnChecksums() const
        {
            return m_skipBasedOnChecksums;
        }

        void setSkipBasedOnChecksums( bool sboc )
        {
            m_skipBasedOnChecksums = sboc;
        }

        bool archiveMode() const
        {
            return m_archiveMode;
        }

        void setArchiveMode( bool am )
        {
            m_archiveMode = am;
        }

        bool recurse() const
        {
            return m_recurse;
        }

        void setRecurse( bool r )
        {
            m_recurse = r;
        }

        bool backup() const
        {
            return m_backup;
        }

        void setBackup( bool b )
        {
            m_backup = b;
        }

        const QString& backupDir() const
        {
            return m_backupDir;
        }

        void setBackupDir( const QString& bd )
        {
            m_backupDir = bd;
        }

        const QString& backupSuffix() const
        {
            return m_backupSuffix;
        }

        void setBackupSuffix( const QString& bs )
        {
            m_backupSuffix = bs;
        }

        bool update() const
        {
            return m_update;
        }

        void setUpdate( bool u )
        {
            m_update = u;
        }

        bool updateInPlace() const
        {
            return m_updateInPlace;
        }

        void setUpdateInPlace( bool uip )
        {
            m_updateInPlace = uip;
        }

        bool appendData() const
        {
            return m_appendData;
        }

        void setAppendData( bool ad )
        {
            m_appendData = ad;
        }

        bool appendVerify() const
        {
            return m_appendVerify;
        }

        void setAppendVerify( bool av )
        {
            m_appendVerify = av;
        }

        bool directories() const
        {
            return m_directories;
        }

        void setDirectories( bool d )
        {
            m_directories = d;
        }

        bool copySymLinksAsIs() const
        {
            return m_copySymLinksAsIs;
        }

        void setCopySymLinksAsIs( bool cslai )
        {
            m_copySymLinksAsIs = cslai;
        }

        bool resolveSymLinks() const
        {
            return m_resolveSymLinks;
        }

        void setResolveSymLinks( bool rsl )
        {
            m_resolveSymLinks = rsl;
        }

        bool resolveUnsafeSymLinks() const
        {
            return m_resolveUnsafeSymLinks;
        }

        void setResolveUnsafeSymLinks( bool rusl )
        {
            m_resolveUnsafeSymLinks = rusl;
        }

        bool ignoreOuterSymLinks() const
        {
            return m_ignoreOuterSymLinks;
        }

        void setIgnoreOuterSymLinks( bool iosl )
        {
            m_ignoreOuterSymLinks = iosl;
        }

        bool resolveDirSymLink() const
        {
            return m_resolveDirSymLink;
        }

        void setResolveDirSymLink( bool rdsl )
        {
            m_resolveDirSymLink = rdsl;
        }

        bool treatDirSymLinkAsDir() const
        {
            return m_treatDirSymLinkAsDir;
        }

        void setTreatDirSymLinkAsDir( bool tdslad )
        {
            m_treatDirSymLinkAsDir = tdslad;
        }

        bool preserveHardLinks() const
        {
            return m_preserveHardLinks;
        }

        void setPreserveHardLinks( bool phl )
        {
            m_preserveHardLinks = phl;
        }

        bool preservePermissions() const
        {
            return m_preservePermissions;
        }

        void setPreservePermissions( bool pp )
        {
            m_preservePermissions = pp;
        }

        bool preserveExecutability() const
        {
            return m_preserveExecutability;
        }

        void setPreserveExecutability( bool pe )
        {
            m_preserveExecutability = pe;
        }

        bool preserveACLs() const
        {
            return m_preserveACLs;
        }

        void setPreserveACLs( bool pa )
        {
            m_preserveACLs = pa;
        }

        bool preserveExtendedAttributes() const
        {
            return m_preserveExtendedAttributes;
        }

        void setPreserveExtendedAttributes( bool pea )
        {
            m_preserveExtendedAttributes = pea;
        }

        bool preserveOwner() const
        {
            return m_preserveOwner;
        }

        void setPreserveOwner( bool po )
        {
            m_preserveOwner = po;
        }

        bool preserveGroup() const
        {
            return m_preserveGroup;
        }

        void setPreserveGroup( bool pg )
        {
            m_preserveGroup = pg;
        }

        bool preserveDevices() const
        {
            return m_preserveDevices;
        }

        void setPreserveDevices( bool pd )
        {
            m_preserveDevices = pd;
        }

        bool preserveSpecialFiles() const
        {
            return m_preserveSpecialFiles;
        }

        void setPreserveSpecialFiles( bool psf )
        {
            m_preserveSpecialFiles = psf;
        }

        bool preserveModificationTimes() const
        {
            return m_preserveModificationTimes;
        }

        void setPreserveModificationTimes( bool pmt )
        {
            m_preserveModificationTimes = pmt;
        }

        bool dontPreserveDirectoryModTimes() const
        {
            return m_dontPreserveDirectoryModTimes;
        }

        void setDontPreserveDirectoryModTimes( bool dpdmt )
        {
            m_dontPreserveDirectoryModTimes = dpdmt;
        }

        bool attemtSuperUser() const
        {
            return m_attemtSuperUser;
        }

        void setAttemtSuperUser( bool asu )
        {
            m_attemtSuperUser = asu;
        }

        bool fakeSuperUser() const
        {
            return m_fakeSuperUser;
        }

        void setFakeSuperUser( bool fsu )
        {
            m_fakeSuperUser = fsu;
        }

        bool effectiveSparseFiles() const
        {
            return m_effectiveSparseFiles;
        }

        void setEffectiveSparseFiles( bool esf )
        {
            m_effectiveSparseFiles = esf;
        }

        bool dryRun() const
        {
            return m_dryRun;
        }

        void setDryRun( bool dr )
        {
            m_dryRun = dr;
        }

        bool copyWholeFiles() const
        {
            return m_copyWholeFiles;
        }

        void setCopyWholeFiles( bool cwf )
        {
            m_copyWholeFiles = cwf;
        }

        bool dontCrossFileSystems() const
        {
            return m_dontCrossFileSystems;
        }

        void setDontCrossFileSystems( bool dcfs )
        {
            m_dontCrossFileSystems = dcfs;
        }

        quint32 blockSize() const
        {
            return m_blockSize;
        }

        void setBlockSize( quint32 bs )
        {
            m_blockSize = bs;
        }

        const QString remoteShell() const
        {
            return m_remoteShell;
        }

        void setRemoteShell( const QString &rs )
        {
            m_remoteShell = rs;
        }

        const QString& remoteRsyncPath() const
        {
            return m_remoteRsyncPath;
        }

        void setRemoteRsyncPath( const QString& rrp )
        {
            m_remoteRsyncPath = rrp;
        }

        bool dontCreateNewFiles() const
        {
            return m_dontCreateNewFiles;
        }

        void setDontCreateNewFiles( bool dcnf )
        {
            m_dontCreateNewFiles = dcnf;
        }

        bool skipExistingFiles() const
        {
            return m_skipExistingFiles;
        }

        void setSkipExistingFiles( bool sef )
        {
            m_skipExistingFiles = sef;
        }

        bool removeSourceAfterSync() const
        {
            return m_removeSourceAfterSync;
        }

        void setRemoveSourceAfterSync( bool rsas )
        {
            m_removeSourceAfterSync = rsas;
        }

        bool removeExtraneousFiles() const
        {
            return m_removeExtraneousFiles;
        }

        void setRemoveExtraneousFiles( bool ref )
        {
            m_removeExtraneousFiles = ref;
        }

        KI3U::Plugins::RsyncTask::DeleteMode deleteMode() const
        {
            return m_deleteMode;
        }

        void setDeleteMode( KI3U::Plugins::RsyncTask::DeleteMode dm )
        {
            m_deleteMode = dm;
        }

        bool deleteExcluded() const
        {
            return m_deleteExcluded;
        }

        void setDeleteExcluded( bool de )
        {
            m_deleteExcluded = de;
        }

        bool deleteIgnoreErrors() const
        {
            return m_deleteIgnoreErrors;
        }

        void setDeleteIgnoreErrors( bool die )
        {
            m_deleteIgnoreErrors = die;
        }

        bool forceDeleteDirectories() const
        {
            return m_forceDeleteDirectories;
        }

        void setForceDeleteDirectories( bool fdd )
        {
            m_forceDeleteDirectories = fdd;
        }

        quint32 maxDelete() const
        {
            return m_maxDelete;
        }

        void setMaxDelete( quint32 md )
        {
            m_maxDelete = md;
        }

        quint64 maxTransferSize() const
        {
            return m_maxTransferSize;
        }

        void setMaxTransferSize( quint64 mts )
        {
            m_maxTransferSize = mts;
        }

        quint64 minTransferSize() const
        {
            return m_minTransferSize;
        }

        void setMinTransferSize( quint64 mts )
        {
            m_minTransferSize = mts;
        }

        bool keepPartiallyTransferredFiles() const
        {
            return m_keepPartiallyTransferredFiles;
        }

        void setKeepPartiallyTransferredFiles( bool kptf )
        {
            m_keepPartiallyTransferredFiles = kptf;
        }

        const QString& partialDir() const
        {
            return m_partialDir;
        }

        void setPartialDir( const QString& pd )
        {
            m_partialDir = pd;
        }

        bool delayUpdates() const
        {
            return m_delayUpdates;
        }

        void setDelayUpdates( bool du )
        {
            m_delayUpdates = du;
        }

        bool numericIds() const
        {
            return m_numericIds;
        }

        void setNumericIds( bool ni )
        {
            m_numericIds = ni;
        }

        quint32 timeout() const
        {
            return m_timeout;
        }

        void setTimeout( quint32 t )
        {
            m_timeout = t;
        }

        quint32 daemonConnectionTimeout() const
        {
            return m_daemonConnectionTimeout;
        }

        void setDaemonConnectionTimeout( quint32 dct )
        {
            m_daemonConnectionTimeout = dct;
        }

        bool ignoreTimes() const
        {
            return m_ignoreTimes;
        }

        void setIgnoreTimes( bool it )
        {
            m_ignoreTimes = it;
        }

        bool skiptOnFileSize() const
        {
            return m_skiptOnFileSize;
        }

        void setSkiptOnFileSize( bool sofs )
        {
            m_skiptOnFileSize = sofs;
        }

        quint32 modificcationTimeWindow() const
        {
            return m_modificcationTimeWindow;
        }

        void setModificcationTimeWindow( quint32 mtw )
        {
            m_modificcationTimeWindow = mtw;
        }

        const QString& tempDirectory() const
        {
            return m_tempDirectory;
        }

        void setTempDirectory( const QString& td )
        {
            m_tempDirectory = td;
        }

        bool compress() const
        {
            return m_compress;
        }

        void setCompress( bool c )
        {
            m_compress = c;
        }

        quint8 compressLevel() const
        {
            return m_compressLevel;
        }

        void setCompressLevel( quint8 cl )
        {
            m_compressLevel = cl;
        }

        const QString& skipCompressSuffixes() const
        {
            return m_skipCompressSuffixes;
        }

        void setSkipCompressSuffixes( const QString& scs )
        {
            m_skipCompressSuffixes = scs;
        }

        bool autoIgnoreCVSStyle() const
        {
            return m_autoIgnoreCVSStyle;
        }

        void setAutoIgnoreCVSStyle( bool aic )
        {
            m_autoIgnoreCVSStyle = aic;
        }

        const QStringList& excludePattern() const
        {
            return m_excludePattern;
        }

        void setExcludePattern( const QStringList& ep )
        {
            m_excludePattern = ep;
        }

        const QString& excludePatternFile() const
        {
            return m_excludePatternFile;
        }

        void setExcludePatternFile( const QString& epf )
        {
            m_excludePatternFile = epf;
        }

        const QStringList& includePattern() const
        {
            return m_includePattern;
        }

        void setIncludePattern( const QStringList& ip )
        {
            m_includePattern = ip;
        }

        const QString& includePatternFile() const
        {
            return m_includePatternFile;
        }

        void setIncludePatternFile( const QString& ipf )
        {
            m_includePatternFile = ipf;
        }

        const QString& sourceListFile() const
        {
            return m_sourceListFile;
        }

        void setSourceListFile( const QString& slf )
        {
            m_sourceListFile = slf;
        }

        bool protectArgs() const
        {
            return m_protectArgs;
        }

        void setProtectArgs( bool pa )
        {
            m_protectArgs = pa;
        }

        const QString& logFile() const
        {
            return m_logFile;
        }

        void setLogFile( const QString& lf )
        {
            m_logFile = lf;
        }

        quint32 bandwidthLimitInKBPS() const
        {
            return m_bandwidthLimitInKBPS;
        }

        void setBandwidthLimitInKBPS( quint32 blik )
        {
            m_bandwidthLimitInKBPS = blik;
        }

        quint32 protocolVersion() const
        {
            return m_protocolVersion;
        }

        void setProtocolVersion( quint32 pv )
        {
            m_protocolVersion = pv;
        }

        const QString& charset() const
        {
            return m_charset;
        }

        void setCharset( const QString& c )
        {
            m_charset = c;
        }

        KI3U::Plugins::RsyncTask::IPProtocol ipProtocol() const
        {
            return m_ipProtocol;
        }

        void setIpProtocol( KI3U::Plugins::RsyncTask::IPProtocol ip )
        {
            m_ipProtocol = ip;
        }

        QStringList toArguments()
        {
            QStringList result;

            result << "--progress";

            switch ( m_transferMode )
            {
                case LocalOnly:
                {
                    result << m_source << m_destination;
                    break;
                }

                case SourceRemote:
                {
                    if ( m_protocol == "rsync" )
                    {
                        result << QString( "rsync://%1%2%3%4%5/%6%7" ).arg( m_user )
                                                                      .arg( m_user.isEmpty() ? "" : "@" )
                                                                      .arg( m_host )
                                                                      .arg( m_port > 0 ? ":" : "" )
                                                                      .arg( m_port > 0 ? QString( "%1" ).arg( m_port ) : "" )
                                                                      .arg( m_rsyncModule )
                                                                      .arg( "\"" + m_source + "\"" )
                               << m_destination;
                    } else
                    if ( m_protocol == "ssh" )
                    {
                        if ( m_port > 0 )
                        {
                            result << "-e" << QString( "ssh -p %1" ).arg( m_port );
                        } else
                        {
                            result << "-e" << "ssh";
                        }
                        result << QString( "%1%2%3:%4" ).arg( m_sshUser )
                                                       .arg( m_sshUser.isEmpty() ? "" : "@" )
                                                       .arg( m_host )
                                                       .arg( "\"" + m_source + "\"" )
                               << m_destination;
                    }
                    break;
                }

                case DestinationRemote:
                {
                    if ( m_protocol == "rsync" )
                    {
                        result << m_source
                               << QString( "rsync://%1%2%3%4%5/%6%7" ).arg( m_user )
                                                                      .arg( m_user.isEmpty() ? "" : "@" )
                                                                      .arg( m_host )
                                                                      .arg( m_port > 0 ? ":" : "" )
                                                                      .arg( m_port > 0 ? QString( "%1" ).arg( m_port ) : "" )
                                                                      .arg( m_rsyncModule )
                                                                      .arg( "\"" + m_destination + "\"" );
                    } else
                    if ( m_protocol == "ssh" )
                    {
                        if ( m_port > 0 )
                        {
                            result << "-e" << QString( "ssh -p %1" ).arg( m_port );
                        } else
                        {
                            result << "-e" << "ssh";
                        }
                        result << m_source
                               << QString( "%1%2%3:%4" ).arg( m_sshUser )
                                                       .arg( m_sshUser.isEmpty() ? "" : "@" )
                                                       .arg( m_host )
                                                       .arg( "\"" + m_destination + "\"" );
                    }
                    break;
                }
            }
//             QString m_source;
//             QString m_destination;
//             SourceSinkType m_transferMode;
//             QString m_user;
//             QString m_pass;
//             bool m_storePassword;
//             QString m_host;
//             quint16 m_port;
//             QString m_protocol;

            if ( m_skipBasedOnChecksums )
            {
                result << "-c";
            }
            if ( m_archiveMode )
            {
                result << "-a";
            }
            if ( m_recurse )
            {
                result << "-r";
            }
            if ( m_backup )
            {
                result << "-b";
            }
            if ( !( m_backupDir.isEmpty() ) )
            {
                result << QString( "--backup-dir=%1" ).arg( m_backupDir );
            }
            if ( !( m_backupSuffix.isEmpty() ) )
            {
                result << QString( "--suffix=%1" ).arg( m_backupSuffix );
            }
            if ( m_update )
            {
                result << "-u";
            }
            if ( m_updateInPlace )
            {
                result << "--inplace";
            }
            if ( m_appendData )
            {
                result << "--append";
            }
            if ( m_appendVerify )
            {
                result << "--append-verify";
            }
            if ( m_directories )
            {
                result << "-d";
            }
            if ( m_copySymLinksAsIs )
            {
                result << "-l";
            }
            if ( m_resolveSymLinks )
            {
                result << "-L";
            }
            if ( m_resolveUnsafeSymLinks )
            {
                result << "--copy-unsafe-links";
            }
            if ( m_ignoreOuterSymLinks )
            {
                result << "--safe-links";
            }
            if ( m_resolveDirSymLink )
            {
                result << "-k";
            }
            if ( m_treatDirSymLinkAsDir )
            {
                result << "-K";
            }
            if ( m_preserveHardLinks )
            {
                result << "-H";
            }
            if ( m_preservePermissions )
            {
                result << "-p";
            }
            if ( m_preserveExecutability )
            {
                result << "-E";
            }
            if ( m_preserveACLs )
            {
                result << "-A";
            }
            if ( m_preserveExtendedAttributes )
            {
                result << "-X";
            }
            if ( m_preserveOwner )
            {
                result << "-o";
            }
            if ( m_preserveGroup )
            {
                result << "-g";
            }
            if ( m_preserveDevices )
            {
                result << "--devices";
            }
            if ( m_preserveSpecialFiles )
            {
                result << "--specials";
            }
            if ( m_preserveModificationTimes )
            {
                result << "-t";
            }
            if ( m_dontPreserveDirectoryModTimes )
            {
                result << "-O";
            }
            if ( m_attemtSuperUser )
            {
                result << "--super";
            }
            if ( m_fakeSuperUser )
            {
                result << "--fake-super";
            }
            if ( m_effectiveSparseFiles )
            {
                result << "m_effectiveSparseFiles";
            }
            if ( m_dryRun )
            {
                result << "-n";
            }
            if ( m_copyWholeFiles )
            {
                result << "-W";
            }
            if ( m_dontCrossFileSystems )
            {
                result << "-x";
            }
            if ( m_blockSize > 0 )
            {
                result << QString( "--block-size=%1" ).arg( m_blockSize );
            }
            if ( !( m_remoteShell.isEmpty() ) )
            {
                result << QString( "--rsh=%1" ).arg( m_remoteShell );
            }
            if ( !( m_remoteRsyncPath.isEmpty() ) )
            {
                result << QString( "--rsync-path=%1" ).arg( m_remoteRsyncPath );
            }
            if ( m_dontCreateNewFiles )
            {
                result << "--existing";
            }
            if ( m_skipExistingFiles )
            {
                result << "--ignore-existing";
            }
            if ( m_removeSourceAfterSync )
            {
                result << "--remove-source-files";
            }
            if ( m_removeExtraneousFiles )
            {
                result << "--delete";
                switch ( m_deleteMode )
                {
                    case DeleteAfter: result << "--delete-after"; break;
                    case DeleteBefore: result << "--delete-before"; break;
                    case DeleteDelay: result << "--delete-delay"; break;
                    case DeleteDuring: result << "--delete-during"; break;
                }
                if ( m_deleteExcluded )
                {
                    result << "--delete-excluded";
                }
                if ( m_deleteIgnoreErrors )
                {
                    result << "--ignore-errors";
                }
                if ( m_forceDeleteDirectories )
                {
                    result << "--force";
                }
                if ( m_maxDelete > 0 )
                {
                    result << QString( "--max-delete=%1" ).arg( m_maxDelete );
                }
            }
            if ( m_maxTransferSize > 0 )
            {
                result << QString( "--max-size=%1" ).arg( m_maxTransferSize );
            }
            if ( m_minTransferSize > 0 )
            {
                result << QString( "--max-size=%1" ).arg( m_minTransferSize );
            }
            if ( m_keepPartiallyTransferredFiles )
            {
                result << "--partial";
            }
            if ( !( m_partialDir.isEmpty() ) )
            {
                result << QString( "--partial-dir=%1" ).arg( m_partialDir );
            }
            if ( m_delayUpdates )
            {
                result << "--delay-updates";
            }
            if ( m_numericIds )
            {
                result << "--numeric-ids";
            }
            if ( m_timeout > 0 )
            {
                result << QString( "--timeout=%1" ).arg( m_timeout );
            }
            if ( m_daemonConnectionTimeout > 0 )
            {
                result << QString( "--contimeout=%1" ).arg( m_daemonConnectionTimeout );
            }
            if ( m_ignoreTimes )
            {
                result << "-I";
            }
            if ( m_skiptOnFileSize )
            {
                result << "--size-only";
            }
            if ( m_modificcationTimeWindow > 0 )
            {
                result << QString( "--modify-window=%1" ).arg( m_modificcationTimeWindow );
            }
            if ( !( m_tempDirectory.isEmpty() ) )
            {
                result << QString( "--temp-dir=%1" ).arg( m_tempDirectory );
            }
            if ( m_compress )
            {
                result << "-z";
                if ( ( m_compressLevel > 0 ) && ( m_compressLevel <= 9 ) )
                {
                    result << QString( "--compress-level=%1" ).arg( m_compressLevel );
                }
            }
            if ( !( m_skipCompressSuffixes.isEmpty() ) )
            {
                result << QString( "--skip-compress=%1" ).arg( m_skipCompressSuffixes );
            }
            if ( m_autoIgnoreCVSStyle )
            {
                result << "-C";
            }
            foreach ( QString exclude, m_excludePattern )
            {
                if ( !( exclude.isEmpty() ) )
                {
                    result << QString( "--exclude=%1" ).arg( exclude );
                }
            }
            if ( QFileInfo( m_excludePatternFile ).isReadable() )
            {
                result << QString( "--exclude-from=%1" ).arg( m_excludePatternFile );
            }
            foreach ( QString include, m_includePattern )
            {
                if ( !( include.isEmpty() ) )
                {
                    result << QString( "--include=%1" ).arg( include );
                }
            }
            if ( QFileInfo( m_includePatternFile ).isReadable() )
            {
                result << QString( "--include-from=%1" ).arg( m_includePatternFile );
            }
            if ( QFileInfo( m_sourceListFile ).isReadable() )
            {
                result << QString( "--files-from=%1" ).arg( m_sourceListFile );
            }
            if ( m_protectArgs )
            {
                result << "-s";
            }
            if ( !( m_logFile.isEmpty() ) )
            {
                result << QString( "--log-file=%1" ).arg( m_logFile );
            }
            if ( m_bandwidthLimitInKBPS > 0 )
            {
                result << QString( "--bwlimit=%1" ).arg( m_bandwidthLimitInKBPS );
            }
            if ( m_protocolVersion > 0 )
            {
                result << QString( "--protocol=%1" ).arg( m_protocolVersion );
            }
            if ( !( m_charset.isEmpty() ) )
            {
                result << QString( "--iconv=%1" ).arg( m_charset );
            }
            switch( m_ipProtocol )
            {
                case PreferIPv4: result << "-4"; break;
                case PreferIPv6: result << "-6"; break;
                default: ;
            }
            return result;
        }

    private:

        static const quint32 STORAGE_VERSION_0;
        static const quint32 STORAGE_VERSION;

        RsyncTask *m_task;

        KProcess *m_process;

        QString m_source;
        QString m_destination;
        SourceSinkType m_transferMode;
        QString m_user;
        QString m_pass;
        bool m_storePassword;
        QString m_host;
        quint16 m_port;
        QString m_protocol;
        QString m_sshAskPass;
        QString m_sshUser;
        QString m_rsyncModule;

        bool m_skipBasedOnChecksums; // -c, --checksum              skip based on checksum, not mod-time & size
        bool m_archiveMode; //-a, --archive               archive mode; equals -rlptgoD (no -H,-A,-X)
        bool m_recurse; //-r, --recursive             recurse into directories
        bool m_backup; // -b, --backup      rectly, but whe          make backups (see --suffix & --backup-dir)
        QString m_backupDir; //    --backup-dir=DIR        make backups into hierarchy based in DIR
        QString m_backupSuffix; //    --suffix=SUFFIX         backup suffix (default ~ w/o --backup-dir)
        bool m_update; //-u, --update                skip files that are newer on the receiver
        bool m_updateInPlace; //    --inplace               update destination files in-place
        bool m_appendData; //    --append                append data onto shorter files
        bool m_appendVerify; //    --append-verify         --append w/old data in file checksum
        bool m_directories; //-d, --dirs                  transfer directories without recursing
        bool m_copySymLinksAsIs; //-l, --links                 copy symlinks as symlinks
        bool m_resolveSymLinks; //-L, --copy-links            transform symlink into referent file/dir
        bool m_resolveUnsafeSymLinks; //    --copy-unsafe-links     only "unsafe" symlinks are transformed
        bool m_ignoreOuterSymLinks; //    --safe-links            ignore symlinks that point outside the tree
        bool m_resolveDirSymLink; //-k, --copy-dirlinks         transform symlink to dir into referent dir
        bool m_treatDirSymLinkAsDir; //-K, --keep-dirlinks         treat symlinked dir on receiver as dir
        bool m_preserveHardLinks; //-H, --hard-links            preserve hard links
        bool m_preservePermissions; //-p, --perms                 preserve permissions
        bool m_preserveExecutability; // -E, --executability         preserve executability
        bool m_preserveACLs; // -A, --acls                  preserve ACLs (implies -p)
        bool m_preserveExtendedAttributes; //-X, --xattrs                preserve extended attributes
        bool m_preserveOwner; // -o, --owner                 preserve owner (super-user only)
        bool m_preserveGroup; //-g, --group                 preserve group
        bool m_preserveDevices; //    --devices               preserve device files (super-user only)
        bool m_preserveSpecialFiles; //    --specials              preserve special files
        bool m_preserveModificationTimes; //-t, --times                 preserve modification times
        bool m_dontPreserveDirectoryModTimes; //-O, --omit-dir-times        omit directories from --times
        bool m_attemtSuperUser; //    --super                 receiver attempts super-user activities
        bool m_fakeSuperUser; //    --fake-super            store/recover privileged attrs using xattrs
        bool m_effectiveSparseFiles; //-S, --sparse                handle sparse files efficiently
        bool m_dryRun; // -n, --dry-run               perform a trial run with no changes made
        bool m_copyWholeFiles; //-W, --whole-file            copy files whole (w/o delta-xfer algorithm)
        bool m_dontCrossFileSystems; // -x, --one-file-system       don't cross filesystem boundaries
        quint32 m_blockSize; //-B, --block-size=SIZE       force a fixed checksum block-size
        QString m_remoteShell; // -e, --rsh=COMMAND           specify the remote shell to use
        QString m_remoteRsyncPath; //    --rsync-path=PROGRAM    specify the rsync to run on remote machine
        bool m_dontCreateNewFiles; //    --existing              skip creating new files on receiver
        bool m_skipExistingFiles; //    --ignore-existing       skip updating files that exist on receiver
        bool m_removeSourceAfterSync; //    --remove-source-files   sender removes synchronized files (non-dir)
        bool m_removeExtraneousFiles; //    --delete                delete extraneous files from dest dirs
        DeleteMode m_deleteMode; 
        bool m_deleteExcluded; //    --delete-excluded       also delete excluded files from dest dirs
        bool m_deleteIgnoreErrors; //    --ignore-errors         delete even if there are I/O errors
        bool m_forceDeleteDirectories; //    --force                 force deletion of dirs even if not empty
        quint32 m_maxDelete; //    --max-delete=NUM        don't delete more than NUM files
        quint64 m_maxTransferSize; //    --max-size=SIZE         don't transfer any file larger than SIZE
        quint64 m_minTransferSize; //    --min-size=SIZE         don't transfer any file smaller than SIZE
        bool m_keepPartiallyTransferredFiles; //    --partial               keep partially transferred files
        QString m_partialDir; //   --partial-dir=DIR       put a partially transferred file into DIR
        bool m_delayUpdates; //    --delay-updates         put all updated files into place at end
        bool m_numericIds; //    --numeric-ids           don't map uid/gid values by user/group name
        quint32 m_timeout; //    --timeout=SECONDS       set I/O timeout in seconds
        quint32 m_daemonConnectionTimeout; //    --contimeout=SECONDS    set daemon connection timeout in seconds
        bool m_ignoreTimes; // -I, --ignore-times          don't skip files that match size and time
        bool m_skiptOnFileSize; //    --size-only             skip files that match in size
        quint32 m_modificcationTimeWindow; //    --modify-window=NUM     compare mod-times with reduced accuracy
        QString m_tempDirectory; //-T, --temp-dir=DIR          create temporary files in directory DIR
        bool m_compress; //-z, --compress              compress file data during the transfer
        quint8 m_compressLevel; //    --compress-level=NUM    explicitly set compression level
        QString m_skipCompressSuffixes; //    --skip-compress=LIST    skip compressing files with suffix in LIST
        bool m_autoIgnoreCVSStyle; //-C, --cvs-exclude           auto-ignore files in the same way CVS does
        QStringList m_excludePattern; //    --exclude=PATTERN       exclude files matching PATTERN
        QString m_excludePatternFile; //    --exclude-from=FILE     read exclude patterns from FILE
        QStringList m_includePattern; //    --include=PATTERN       don't exclude files matching PATTERN
        QString m_includePatternFile; //    --include-from=FILE     read include patterns from FILE
        QString m_sourceListFile; //    --files-from=FILE       read list of source-file names from FILE
        bool m_protectArgs; //-s, --protect-args          no space-splitting; wildcard chars only
        QString m_logFile; //    --log-file=FILE         log what we're doing to the specified FILE
        quint32 m_bandwidthLimitInKBPS; //    --bwlimit=KBPS          limit I/O bandwidth; KBytes per second
        quint32 m_protocolVersion; //    --protocol=NUM          force an older protocol version to be used
        QString m_charset; //    --iconv=CONVERT_SPEC    request charset conversion of filenames
        IPProtocol m_ipProtocol;        
};


const quint32 RsyncTask::STORAGE_VERSION_0 = 0;
const quint32 RsyncTask::STORAGE_VERSION = RsyncTask::STORAGE_VERSION_0;

const quint32 RsyncTask::RsyncTaskPrivate::STORAGE_VERSION_0 = 0;
const quint32 RsyncTask::RsyncTaskPrivate::STORAGE_VERSION = RsyncTask::RsyncTaskPrivate::STORAGE_VERSION_0;



RsyncTask::RsyncTask(Plugin* parent)
    : Task(parent),
      d( new RsyncTaskPrivate( this ) )
{
}

RsyncTask::~RsyncTask()
{
    delete d;
}

const QString RsyncTask::id() const
{
    return RsyncPlugin::ID;
}

QByteArray RsyncTask::saveSettings()
{
    QByteArray data;
    QDataStream stream( &data, QIODevice::WriteOnly );

    stream << RsyncTask::STORAGE_VERSION;
    stream << Task::saveSettings()
           << d->saveSettings();
    return data;
}

void RsyncTask::restoreSettings(const QByteArray& data)
{
    QDataStream stream( &const_cast< QByteArray& >( data ), QIODevice::ReadOnly );

    quint32 storageVersion;

    QByteArray taskData, privateData;
    
    stream >> storageVersion
           >> taskData
           >> privateData;
    Task::restoreSettings( taskData );
    d->restoreSettings( privateData );
}

bool RsyncTask::initialize()
{
    if ( d->process() )
    {
        emit statusChanged( i18n( "The task is already running! Initialization failed." ), Error );
        return false;
    }
    if ( d->source().isEmpty() )
    {
        emit statusChanged( i18n( "No source specified!" ), Error );
        return false;
    }
    if ( d->destination().isEmpty() )
    {
        emit statusChanged( i18n( "No destination specified!" ), Error );
        return false;
    }
    d->setProcess( new KProcess( this ) );
    d->process()->setOutputChannelMode( KProcess::SeparateChannels );
    connect( d->process(), SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(processFinished(int,QProcess::ExitStatus)) );
    connect( d->process(), SIGNAL(readyReadStandardOutput()), this, SLOT(readStandardOutput()) );
    connect( d->process(), SIGNAL(readyReadStandardError()), this, SLOT(readStandardError()) );
    emit statusChanged( i18n( "Initialization successful" ), Information );
    return true;
}

bool RsyncTask::run()
{
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert( "SSH_ASKPASS", d->sshAskPass() );
    env.insert( "RSYNC_PASSWORD", d->pass() );
    d->process()->setProcessEnvironment( env );
    d->process()->setProgram( "rsync", d->toArguments() );
    d->process()->start();
    d->process()->closeWriteChannel();
    emit progressChanged( -1 );
    emit statusChanged( i18n( "Started rsync process: " ) + d->process()->program().join( " " ), Information );
    kDebug() << "Started rsync:" << d->process()->program();
    return d->process()->waitForStarted();
}

bool RsyncTask::finalize()
{
    if ( d->process() )
    {
        d->process()->deleteLater();
        d->setProcess( 0 );
        emit statusChanged( i18n( "Task finalized" ), Information );
        return true;
    }
    emit statusChanged( i18n( "The task is not running, but finalize is called!" ), Warning );
    return false;
}

void RsyncTask::configure()
{
    TaskConfigDialog tcd( this );
    if ( tcd.exec() == KDialog::Accepted )
    {
        d->setTransferMode( ( SourceSinkType ) tcd.transferMode() );
        d->setProtocol( tcd.protocol() );
        d->setHost( tcd.host() );
        d->setPort( tcd.port() );
        d->setUser( tcd.user() );
        if ( !( tcd.pass().isEmpty() ) )
        {
            d->setPass( tcd.pass() );
        }
        d->setStorePassword( tcd.keepPassword() );
        d->setSource( tcd.source() );
        d->setDestination( tcd.destination() );
        d->setSshAskPass( tcd.sshAskPass() );
        d->setSshUser( tcd.sshUser() );
        d->setRsyncModule( tcd.rsyncModule() );
        d->setDryRun( tcd.dryRun() );

        d->setArchiveMode( tcd.archiveMode() );
        d->setRecurse( tcd.recurse() );
        d->setDirectories( tcd.directories() );
        d->setSkipBasedOnChecksums( tcd.skipBasedOnChecksums() );
        d->setSkiptOnFileSize( tcd.skipOnFileSize() );
        d->setUpdate( tcd.update() );
        d->setUpdateInPlace( tcd.updateInPlace() );
        d->setDelayUpdates( tcd.delayUpdates() );
        d->setAppendData( tcd.appendData() );
        d->setAppendVerify( tcd.appendVerify() );
        d->setDontCrossFileSystems( tcd.dontCrossFileSystems() );
        d->setDontCreateNewFiles( tcd.dontCreateNewFiles() );
        d->setSkipExistingFiles( tcd.skipExistingFiles() );
        d->setRemoveExtraneousFiles( tcd.removeExtraneousFiles() );
        d->setDeleteMode( ( DeleteMode ) tcd.deleteMode() );
        d->setDeleteExcluded( tcd.deleteExcluded() );
        d->setDeleteIgnoreErrors( tcd.deleteIgnoreErrors() );
        d->setForceDeleteDirectories( tcd.forceDeleteDirectories() );
        d->setMaxDelete( tcd.maxDelete() );
        d->setModificcationTimeWindow( tcd.modificationTimeWindow() );
        d->setIgnoreTimes( tcd.ignoreTimes() );
        d->setBackup( tcd.backup() );
        d->setBackupDir( tcd.backupDir() );
        d->setBackupSuffix( tcd.backupSuffix() );
        d->setCopySymLinksAsIs( tcd.copySymLinksAsIs() );
        d->setResolveSymLinks( tcd.resolveSymLinks() );
        d->setResolveUnsafeSymLinks( tcd.resolveUnsafeSymLinks() );
        d->setIgnoreOuterSymLinks( tcd.ignoreOuterSymLinks() );
        d->setResolveDirSymLink( tcd.resolveDirSymLinks() );
        d->setTreatDirSymLinkAsDir( tcd.treatDirSymLinksAsDir() );
        d->setPreserveHardLinks( tcd.preserveHardLinks() );
        d->setPreservePermissions( tcd.preservePermissions() );
        d->setPreserveExecutability( tcd.preserveExecutability() );
        d->setPreserveACLs( tcd.preserveACLs() );
        d->setPreserveExtendedAttributes( tcd.preserveExtendedAttributes() );
        d->setPreserveOwner( tcd.preserveOwner() );
        d->setPreserveGroup( tcd.preserveGroup() );
        d->setPreserveDevices( tcd.preserveDevices() );
        d->setPreserveSpecialFiles( tcd.preserveSpecialFiles() );
        d->setPreserveModificationTimes( tcd.preserveModificationTimes() );
        d->setDontPreserveDirectoryModTimes( tcd.dontPreserveDirectoryModTimes() );
        d->setNumericIds( tcd.numericIds() );
        d->setAutoIgnoreCVSStyle( tcd.autoIgnoreCVSStyle() );
        d->setExcludePattern( tcd.excludePattern() );
        d->setExcludePatternFile( tcd.excludePatternFile() );
        d->setIncludePattern( tcd.includePattern() );
        d->setIncludePatternFile( tcd.includePatternFile() );
        d->setTempDirectory( tcd.tempDirectory() );
        d->setLogFile( tcd.logFile() );
        d->setSourceListFile( tcd.sourceListFile() );

        d->setCopyWholeFiles( tcd.copyWholeFiles() );
        d->setBlockSize( tcd.blockSize() );
        d->setMaxTransferSize( tcd.maxTransferSize() );
        d->setMinTransferSize( tcd.minTransferSize() );
        d->setKeepPartiallyTransferredFiles( tcd.keepPartiallyTransferredFiles() );
        d->setPartialDir( tcd.partialDir() );
        d->setIpProtocol( ( IPProtocol ) tcd.ipProtocol() );
        d->setCompress( tcd.compress() );
        d->setCompressLevel( tcd.compressLevel() );
        d->setSkipCompressSuffixes( tcd.skipCompressSuffixes() );
        d->setBandwidthLimitInKBPS( tcd.bandwidthLimitInKBPS() );
        d->setTimeout( tcd.timeout() );
        d->setDaemonConnectionTimeout( tcd.daemonConnectionTimeout() );
    }
}

KI3U::TaskEntry* RsyncTask::data()
{
    return KI3U::Task::data();
}

void
RsyncTask::prepareRemove()
{
    KI3U::Task::prepareRemove();
    plugin()->solutionManager()->dataManager()->passwordManager()->removePassword( objectName() );
}


const QString
RsyncTask::source() const
{
    return d->source();
}

const QString
RsyncTask::destination() const
{
    return d->destination();
}

RsyncTask::SourceSinkType
RsyncTask::transferMode() const
{
    return d->transferMode();
}

const QString
RsyncTask::user() const
{
    return d->user();
}

const QString
RsyncTask::pass() const
{
    return d->pass();
}

const QString
RsyncTask::host() const
{
    return d->host();
}

bool
RsyncTask::storePassword() const
{
    return d->storePassword();
}

quint16
RsyncTask::port() const
{
    return d->port();
}

const QString
RsyncTask::protocol() const
{
    return d->protocol();
}

const QString
RsyncTask::sshAskPass() const
{
    return d->sshAskPass();
}

const QString
RsyncTask::sshUser() const
{
    return d->sshUser();
}

const QString
RsyncTask::rsyncModule() const
{
    return d->rsyncModule();
}

bool
RsyncTask::skipBasedOnChecksums() const
{
    return d->skipBasedOnChecksums();
}

bool
RsyncTask::archiveMode() const
{
    return d->archiveMode();
}

bool
RsyncTask::recurse() const
{
    return d->recurse();
}

bool
RsyncTask::backup() const
{
    return d->backup();
}

const QString
RsyncTask::backupDir() const
{
    return d->backupDir();
}

const QString
RsyncTask::backupSuffix() const
{
    return d->backupSuffix();
}

bool
RsyncTask::update() const
{
    return d->update();
}

bool
RsyncTask::updateInPlace() const
{
    return d->updateInPlace();
}

bool
RsyncTask::appendData() const
{
    return d->appendData();
}

bool
RsyncTask::appendVerify() const
{
    return d->appendVerify();
}

bool
RsyncTask::directories() const
{
    return d->directories();
}

bool
RsyncTask::copySymLinksAsIs() const
{
    return d->copySymLinksAsIs();
}

bool
RsyncTask::resolveSymLinks() const
{
    return d->resolveSymLinks();
}

bool
RsyncTask::resolveUnsafeLinks() const
{
    return d->resolveUnsafeSymLinks();
}

bool
RsyncTask::ignoreOuterSymLinks() const
{
    return d->ignoreOuterSymLinks();
}

bool
RsyncTask::resolveDirSymLinks() const
{
    return d->resolveDirSymLink();
}

bool
RsyncTask::treatDirSymLinkAsDir() const
{
    return d->treatDirSymLinkAsDir();
}

bool
RsyncTask::preserveHardLinks() const
{
    return d->preserveHardLinks();
}

bool
RsyncTask::preservePermissions() const
{
    return d->preservePermissions();
}

bool
RsyncTask::preserveExecutability() const
{
    return d->preserveExecutability();
}

bool
RsyncTask::preserveACLs() const
{
    return d->preserveACLs();
}

bool
RsyncTask::preserveExtendedAttributes() const
{
    return d->preserveExtendedAttributes();
}

bool
RsyncTask::preserveOwner() const
{
    return d->preserveOwner();
}

bool
RsyncTask::preserveGroup() const
{
    return d->preserveGroup();
}

bool
RsyncTask::preserveDevices() const
{
    return d->preserveDevices();
}

bool
RsyncTask::preserveSpecialFiles() const
{
    return d->preserveSpecialFiles();
}

bool
RsyncTask::preserveModificationTimes() const
{
    return d->preserveModificationTimes();
}

bool
RsyncTask::dontPreserveDirectoryModTimes() const
{
    return d->dontPreserveDirectoryModTimes();
}

bool
RsyncTask::attemtSuperUser() const
{
    return d->attemtSuperUser();
}

bool
RsyncTask::fakeSuperUser() const
{
    return d->fakeSuperUser();
}

bool
RsyncTask::effectiveSparseFiles() const
{
    return d->effectiveSparseFiles();
}

bool
RsyncTask::dryRun() const
{
    return d->dryRun();
}

bool
RsyncTask::copyWholeFiles() const
{
    return d->copyWholeFiles();
}

bool
RsyncTask::dontCrossFileSystems() const
{
    return d->dontCrossFileSystems();
}

quint32
RsyncTask::blockSize() const
{
    return d->blockSize();
}

const QString
RsyncTask::remoteShell() const
{
    return d->remoteShell();
}

const QString
RsyncTask::remoteRsyncPath() const
{
    return d->remoteRsyncPath();
}

bool
RsyncTask::dontCreateNewFiles() const
{
    return d->dontCreateNewFiles();
}

bool
RsyncTask::skipExistingFiles() const
{
    return d->skipExistingFiles();
}

bool
RsyncTask::removeSourceAfterSync() const
{
    return d->removeSourceAfterSync();
}

bool
RsyncTask::removeExtraneousFiles() const
{
    return d->removeExtraneousFiles();
}

RsyncTask::DeleteMode
RsyncTask::deleteMode() const
{
    return d->deleteMode();
}

bool
RsyncTask::deleteExcluded() const
{
    return d->deleteExcluded();
}

bool
RsyncTask::deleteIgnoreErrors() const
{
    return d->deleteIgnoreErrors();
}

bool
RsyncTask::forceDeleteDirectories() const
{
    return d->forceDeleteDirectories();
}

quint32
RsyncTask::maxDelete() const
{
    return d->maxDelete();
}

quint64
RsyncTask::maxTransferSize() const
{
    return d->maxTransferSize();
}

quint64
RsyncTask::minTransferSize() const
{
    return d->minTransferSize();
}

bool
RsyncTask::keepPartiallyTransferredFiles() const
{
    return d->keepPartiallyTransferredFiles();
}

const QString
RsyncTask::partialDir() const
{
    return d->partialDir();
}

bool
RsyncTask::delayUpdates() const
{
    return d->delayUpdates();
}

bool
RsyncTask::numericIds() const
{
    return d->numericIds();
}

quint32
RsyncTask::timeout() const
{
    return d->timeout();
}

quint32
RsyncTask::daemonConnectionTimeout()
{
    return d->daemonConnectionTimeout();
}

bool
RsyncTask::ignoreTimes() const
{
    return d->ignoreTimes();
}

bool
RsyncTask::skipOnFileSize() const
{
    return d->skiptOnFileSize();
}

quint32
RsyncTask::modificationTimeWindow() const
{
    return d->modificcationTimeWindow();
}

const QString
RsyncTask::tempDirectory() const
{
    return d->tempDirectory();
}

bool
RsyncTask::compress() const
{
    return d->compress();
}

quint8
RsyncTask::compressLevel() const
{
    return d->compressLevel();
}

const QString
RsyncTask::skipCompressSuffixes() const
{
    return d->skipCompressSuffixes();
}

bool
RsyncTask::autoIgnoreCVSStyle() const
{
    return d->autoIgnoreCVSStyle();
}

const QStringList
RsyncTask::excludePattern() const
{
    return d->excludePattern();
}

const QString
RsyncTask::excludePatternFile() const
{
    return d->excludePatternFile();
}

const QStringList
RsyncTask::includePattern() const
{
    return d->includePattern();
}

const QString
RsyncTask::includePatternFile() const
{
    return d->includePatternFile();
}

const QString
RsyncTask::sourceListFile() const
{
    return d->sourceListFile();
}

bool
RsyncTask::protectArgs() const
{
    return d->protectArgs();
}

const QString
RsyncTask::logFile() const
{
    return d->logFile();
}

quint32
RsyncTask::bandwidthLimitInKBPS() const
{
    return d->bandwidthLimitInKBPS();
}

quint32
RsyncTask::protocolVersion() const
{
    return d->protocolVersion();
}

const QString
RsyncTask::charset() const
{
    return d->charset();
}

RsyncTask::IPProtocol
RsyncTask::ipProtocol() const
{
    return d->ipProtocol();
}

void
RsyncTask::processFinished( int exitCode, QProcess::ExitStatus exitStatus )
{
    emit statusChanged( i18n( "Run finished" ), Information );
    emit progressChanged( 100 );
    emit runFinished( exitCode == 0 && exitStatus == QProcess::NormalExit );
}

void
RsyncTask::readStandardOutput()
{
    d->process()->setReadChannel( QProcess::StandardOutput );
    while ( d->process()->canReadLine() )
    {
        QString line = d->process()->readLine();
        line = line.trimmed();
        QRegExp exp( "to-check=([0-9]*)/([0-9]*)" );
        if ( exp.indexIn( line ) >= 0 )
        {
            if ( exp.capturedTexts().count() >= 3 )
            {
                int toCheck = exp.capturedTexts()[ 1 ].toInt();
                int totalFiles = exp.capturedTexts()[ 2 ].toInt();
                int checkedFiles = totalFiles - toCheck;
                int progress = -1;
                if ( checkedFiles == 0 )
                {
                    progress = 0;
                } else
                {
                    progress = checkedFiles * 100 / totalFiles;
                    if ( progress < 0 || progress > 100 )
                    {
                        progress = -1;
                    }
                }
                kDebug() << "Rsync task" << objectName() << "progress:" << progress;
                emit progressChanged( progress );
            }
        } else
        {
            kDebug() << line;
            emit statusChanged( line, Information );
        }
    }
}

void
RsyncTask::readStandardError()
{
    d->process()->setReadChannel( QProcess::StandardError );
    while ( d->process()->canReadLine() )
    {
        QString line = d->process()->readLine();
        line = line.trimmed();
        kWarning() << line;
        emit statusChanged( line, Error );
    }
}

}

}