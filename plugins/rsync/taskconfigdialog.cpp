/*
    KI3U - Backup and Synchronization for KDE
    Copyright (C) 2009, 2010  Martin Höher <martin@rpdev.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "taskconfigdialog.h"

#include "ui_accountpage.h"
#include "ui_modepage.h"
#include "ui_transferpage.h"

#include "rsynctask.h"

#include <KDebug>
#include <KPageDialog>

#include <QWidget>

namespace KI3U
{

namespace Plugins
{

class TaskConfigDialog::TaskConfigDialogPrivate
{
    public:

        TaskConfigDialogPrivate( TaskConfigDialog *dialog )
            : m_dialog( dialog )
        {
        }

        virtual ~TaskConfigDialogPrivate()
        {
        }

        Ui::AccountPage *accountPage;
        Ui::ModePage *modePage;
        Ui::TransferPage *transferPage;

    private:

        TaskConfigDialog *m_dialog;
};


TaskConfigDialog::TaskConfigDialog( KI3U::Plugins::RsyncTask *task, QWidget* parent, Qt::WFlags flags )
    : KPageDialog(parent, flags),
      d( new TaskConfigDialogPrivate( this ) ),
      m_task( task )
{
    setupGui();
    applyTaskSettings();
}

TaskConfigDialog::~TaskConfigDialog()
{
    delete d;
}

int
TaskConfigDialog::transferMode() const
{
    return d->accountPage->transferType->itemData( d->accountPage->transferType->currentIndex(), Qt::UserRole ).toInt();
}

const QString
TaskConfigDialog::protocol() const
{
    if ( d->accountPage->protocolSsh->isChecked() )
    {
        return "ssh";
    }
    if ( d->accountPage->protocolRsync->isChecked() )
    {
        return "rsync";
    }
    return "";
}

const QString
TaskConfigDialog::host() const
{
    return d->accountPage->host->text();
}

quint16
TaskConfigDialog::port() const
{
    return d->accountPage->port->value();
}

const QString
TaskConfigDialog::user() const
{
    return d->accountPage->user->text();
}

const QString
TaskConfigDialog::pass() const
{
    return d->accountPage->pass->text();
}

bool
TaskConfigDialog::keepPassword() const
{
    return d->accountPage->storePassword->isChecked();
}

const QString
TaskConfigDialog::source() const
{
    return d->accountPage->source->text();
}

const QString
TaskConfigDialog::destination() const
{
    return d->accountPage->destination->text();
}

const QString
TaskConfigDialog::sshAskPass() const
{
    return d->accountPage->sshAskPass->text();
}

const QString
TaskConfigDialog::sshUser() const
{
    return d->accountPage->sshUser->text();
}

const QString
TaskConfigDialog::rsyncModule() const
{
    return d->accountPage->module->text();
}

bool
TaskConfigDialog::dryRun() const
{
    return d->accountPage->dryRun->isChecked();
}

bool
TaskConfigDialog::archiveMode() const
{
    return d->modePage->archiveMode->isChecked();
}

bool
TaskConfigDialog::recurse() const
{
    return d->modePage->recurse->isChecked();
}

bool
TaskConfigDialog::directories() const
{
    return d->modePage->directories->isChecked();
}

bool
TaskConfigDialog::skipBasedOnChecksums() const
{
    return d->modePage->skipBasedOnChecksums->isChecked();
}

bool
TaskConfigDialog::skipOnFileSize() const
{
    return d->modePage->skipBasedOnFileSize->isChecked();
}

bool
TaskConfigDialog::update() const
{
    return d->modePage->update->isChecked();
}

bool
TaskConfigDialog::updateInPlace() const
{
    return d->modePage->updateInPlace->isChecked();
}

bool
TaskConfigDialog::delayUpdates() const
{
    return d->modePage->delayUpdates->isChecked();
}

bool
TaskConfigDialog::appendData() const
{
    return d->modePage->append->isChecked();
}

bool
TaskConfigDialog::appendVerify() const
{
    return d->modePage->verifyAppend->isChecked();
}

bool
TaskConfigDialog::dontCrossFileSystems() const
{
    return d->modePage->dontCrossFileSystem->isChecked();
}

bool
TaskConfigDialog::dontCreateNewFiles() const
{
    return d->modePage->dontCreateNewFiles->isChecked();
}

bool
TaskConfigDialog::skipExistingFiles() const
{
    return d->modePage->skipExisting->isChecked();
}

bool
TaskConfigDialog::removeExtraneousFiles() const
{
    return d->modePage->deleteExtraneous->isChecked();
}

int
TaskConfigDialog::deleteMode() const
{
    return d->modePage->deleteMode->itemData( d->modePage->deleteMode->currentIndex(), Qt::UserRole ).toInt();
}

bool
TaskConfigDialog::deleteExcluded() const
{
    return d->modePage->deleteExcluded->isChecked();
}

bool
TaskConfigDialog::deleteIgnoreErrors() const
{
    return d->modePage->deleteIgnoreErrors->isChecked();
}

bool
TaskConfigDialog::forceDeleteDirectories() const
{
    return d->modePage->forceDelete->isChecked();
}

quint32
TaskConfigDialog::maxDelete() const
{
    return d->modePage->maxDelete->value();
}

quint32
TaskConfigDialog::modificationTimeWindow() const
{
    return d->modePage->modificationTimeWindow->value();
}

bool
TaskConfigDialog::ignoreTimes() const
{
    return d->modePage->ignoreTimes->isChecked();
}

bool
TaskConfigDialog::backup() const
{
    return d->modePage->backup->isChecked();
}

const QString
TaskConfigDialog::backupDir() const
{
    return d->modePage->backupDirectory->text();
}

const QString
TaskConfigDialog::backupSuffix() const
{
    return d->modePage->backupSuffix->text();
}

bool
TaskConfigDialog::copySymLinksAsIs() const
{
    return d->modePage->copySymLinksAsIs->isChecked();
}

bool
TaskConfigDialog::resolveSymLinks() const
{
    return d->modePage->resolveSymLinks->isChecked();
}

bool
TaskConfigDialog::resolveUnsafeSymLinks() const
{
    return d->modePage->resolveUnsafeSymLinks->isChecked();
}

bool
TaskConfigDialog::ignoreOuterSymLinks() const
{
    return d->modePage->ignoreOuterSymLinks->isChecked();
}

bool
TaskConfigDialog::resolveDirSymLinks() const
{
    return d->modePage->resolveDirSymLinks->isChecked();
}

bool
TaskConfigDialog::treatDirSymLinksAsDir() const
{
    return d->modePage->treatDirSymLinksAsDir->isChecked();
}

bool
TaskConfigDialog::preserveHardLinks() const
{
    return d->modePage->preserveHardlinks->isChecked();
}

bool
TaskConfigDialog::preservePermissions() const
{
    return d->modePage->preservePermissions->isChecked();
}

bool
TaskConfigDialog::preserveExecutability() const
{
    return d->modePage->preserveExecutability->isChecked();
}

bool
TaskConfigDialog::preserveACLs() const
{
    return d->modePage->preserveACLs->isChecked();
}

bool
TaskConfigDialog::preserveExtendedAttributes() const
{
    return d->modePage->preserveExtendedAttributes->isChecked();
}

bool
TaskConfigDialog::preserveOwner() const
{
    return d->modePage->preserveOwner->isChecked();
}

bool
TaskConfigDialog::preserveGroup() const
{
    return d->modePage->preserveGroup->isChecked();
}

bool
TaskConfigDialog::preserveDevices() const
{
    return d->modePage->preserveDevices->isChecked();
}

bool
TaskConfigDialog::preserveSpecialFiles() const
{
    return d->modePage->preserveSpecialFiles->isChecked();
}

bool
TaskConfigDialog::preserveModificationTimes() const
{
    return d->modePage->preserveModificationTimes->isChecked();
}

bool
TaskConfigDialog::dontPreserveDirectoryModTimes() const
{
    return d->modePage->dontPreserveDirectoryModificationTimes->isChecked();
}

bool
TaskConfigDialog::numericIds() const
{
    return d->modePage->numericIDs->isChecked();
}

bool
TaskConfigDialog::autoIgnoreCVSStyle() const
{
    return d->modePage->autoIgnoreCVSStyle->isChecked();
}

const QStringList
TaskConfigDialog::excludePattern() const
{
    return d->modePage->exludePattern->toPlainText().split( "\n" );
}

const QString
TaskConfigDialog::excludePatternFile() const
{
    return d->modePage->excludePatternFile->text();
}

const QStringList
TaskConfigDialog::includePattern() const
{
    return d->modePage->includePattern->toPlainText().split( "\n" );
}

const QString
TaskConfigDialog::includePatternFile() const
{
    return d->modePage->includePatternFile->text();
}

const QString
TaskConfigDialog::tempDirectory() const
{
    return d->modePage->temporaryDirectory->text();
}

const QString
TaskConfigDialog::logFile() const
{
    return d->modePage->logFile->text();
}

const QString
TaskConfigDialog::sourceListFile() const
{
    return d->modePage->sourceListFile->text();
}

bool
TaskConfigDialog::copyWholeFiles() const
{
    return d->transferPage->copyWholeFiles->isChecked();
}

quint32
TaskConfigDialog::blockSize() const
{
    return d->transferPage->blockSize->value();
}

quint64
TaskConfigDialog::maxTransferSize() const
{
    return d->transferPage->maxTransferSize->value();
}

quint64
TaskConfigDialog::minTransferSize() const
{
    return d->transferPage->minTransferSize->value();
}

bool
TaskConfigDialog::keepPartiallyTransferredFiles() const
{
    return d->transferPage->keepPartialFiles->isChecked();
}

const QString
TaskConfigDialog::partialDir() const
{
    return d->transferPage->partialDir->text();
}

int
TaskConfigDialog::ipProtocol() const
{
    return d->transferPage->ipProtocol->itemData( d->transferPage->ipProtocol->currentIndex(), Qt::UserRole ).toInt();
}

bool
TaskConfigDialog::compress() const
{
    return d->transferPage->compress->isChecked();
}

quint8
TaskConfigDialog::compressLevel() const
{
    return d->transferPage->compressLevel->value();
}

const QString
TaskConfigDialog::skipCompressSuffixes() const
{
    return d->transferPage->excludeCompressSuffixes->toPlainText().split( "\n" ).join( "/" );
}

quint32
TaskConfigDialog::bandwidthLimitInKBPS() const
{
    return d->transferPage->limitBandwidth->value();
}

quint32
TaskConfigDialog::timeout() const
{
    return d->transferPage->timeout->value();
}

quint32
TaskConfigDialog::daemonConnectionTimeout() const
{
    return d->transferPage->daemonConnectionTimeout->value();
}

void
TaskConfigDialog::setupGui()
{
    d->accountPage = new Ui::AccountPage();
    QWidget *accountPageWidget = new QWidget( this );
    d->accountPage->setupUi( accountPageWidget );
    connect( d->accountPage->transferType, SIGNAL(currentIndexChanged(int)), this, SLOT(transferTypeChanged(int)) );
    connect( d->accountPage->protocolSsh, SIGNAL(toggled(bool)), this, SLOT(sshSelected(bool)) );
    connect( d->accountPage->protocolRsync, SIGNAL(toggled(bool)), this, SLOT(rsyncSelected(bool)) );
    d->accountPage->transferType->addItem( i18n( "Local to Local" ), (int) RsyncTask::LocalOnly );
    d->accountPage->transferType->addItem( i18n( "Copy from Remote Host" ), (int) RsyncTask::SourceRemote );
    d->accountPage->transferType->addItem( i18n( "Safe to Remote Host" ), (int) RsyncTask::DestinationRemote );
    d->accountPage->transferType->setCurrentIndex( 0 );
    d->accountPage->protocolSsh->setChecked( true );
    d->accountPage->port->setValue( 22 );
    accountPageWidget->setWindowIcon( KIcon( "computer" ) );
    addPage( accountPageWidget, i18n( "Account" ) )->setIcon( KIcon( "computer" ) );

    d->modePage = new Ui::ModePage();
    QWidget *modePageWidget = new QWidget( this );
    d->modePage->setupUi( modePageWidget );
    //TODO: Connect anything here?
    d->modePage->deleteMode->addItem( i18n( "Delete Before" ), RsyncTask::DeleteBefore );
    d->modePage->deleteMode->addItem( i18n( "Delete During Transfer" ), RsyncTask::DeleteDuring );
    d->modePage->deleteMode->addItem( i18n( "Delay Delete" ), RsyncTask::DeleteDelay );
    d->modePage->deleteMode->addItem( i18n( "Delete After" ), RsyncTask::DeleteAfter );
    addPage( modePageWidget, i18n( "Mode" ) )->setIcon( KIcon( "preferences-other" ) );

    d->transferPage = new Ui::TransferPage();
    QWidget *transferPageWidget = new QWidget( this );
    d->transferPage->setupUi( transferPageWidget );
    d->transferPage->ipProtocol->addItem( i18n( "No Preference" ), RsyncTask::NoPreference );
    d->transferPage->ipProtocol->addItem( i18n( "Prefer IPv4" ), RsyncTask::PreferIPv4 );
    d->transferPage->ipProtocol->addItem( i18n( "Prefer IPv6" ), RsyncTask::PreferIPv6 );
    addPage( transferPageWidget, i18n( "Transfer" ) )->setIcon( KIcon( "preferences-system-network" ) );
    
    //         quint32 m_protocolVersion; //    --protocol=NUM          force an older protocol version to be used
    //         QString m_charset; //    --iconv=CONVERT_SPEC    request charset conversion of filenames
    //         bool m_attemtSuperUser; //    --super                 receiver attempts super-user activities
    //         bool m_fakeSuperUser; //    --fake-super            store/recover privileged attrs using xattrs
    //         bool m_effectiveSparseFiles; //-S, --sparse                handle sparse files efficiently
    //         bool m_dryRun; // -n, --dry-run               perform a trial run with no changes made
    //         bool m_removeSourceAfterSync; //    --remove-source-files   sender removes synchronized files (non-dir)
    //         bool m_protectArgs; //-s, --protect-args          no space-splitting; wildcard chars only
    //         QString m_remoteShell; // -e, --rsh=COMMAND           specify the remote shell to use
    //         QString m_remoteRsyncPath; //    --rsync-path=PROGRAM    specify the rsync to run on remote machine
}

void
TaskConfigDialog::applyTaskSettings()
{
    d->accountPage->transferType->setCurrentIndex( ( int ) m_task->transferMode() );
    ( m_task->protocol() == "ssh" ? d->accountPage->protocolSsh : d->accountPage->protocolRsync )->setChecked( true );
    d->accountPage->host->setText( m_task->host() );
    d->accountPage->port->setValue( m_task->port() );
    d->accountPage->user->setText( m_task->user() );
    d->accountPage->pass->setText( m_task->pass() );
    d->accountPage->storePassword->setChecked( m_task->storePassword() );
    d->accountPage->source->setText( m_task->source() );
    d->accountPage->destination->setText( m_task->destination() );
    d->accountPage->sshAskPass->setText( m_task->sshAskPass() );
    d->accountPage->sshUser->setText( m_task->sshUser() );
    d->accountPage->module->setText( m_task->rsyncModule() );
    d->accountPage->dryRun->setChecked( m_task->dryRun() );

    d->modePage->archiveMode->setChecked( m_task->archiveMode() );
    d->modePage->recurse->setChecked( m_task->recurse() );
    d->modePage->directories->setChecked( m_task->directories() );
    d->modePage->skipBasedOnChecksums->setChecked( m_task->skipBasedOnChecksums() );
    d->modePage->skipBasedOnFileSize->setChecked( m_task->skipOnFileSize() );
    d->modePage->dontCreateNewFiles->setChecked( m_task->dontCreateNewFiles() );
    d->modePage->append->setChecked( m_task->appendData() );
    d->modePage->verifyAppend->setChecked( m_task->appendVerify() );
    d->modePage->dontCrossFileSystem->setChecked( m_task->dontCrossFileSystems() );
    d->modePage->skipExisting->setChecked( m_task->skipExistingFiles() );
    d->modePage->ignoreTimes->setChecked( m_task->ignoreTimes() );
    d->modePage->modificationTimeWindow->setValue( m_task->modificationTimeWindow() );
    d->modePage->update->setChecked( m_task->update() );
    d->modePage->updateInPlace->setChecked( m_task->updateInPlace() );
    d->modePage->delayUpdates->setChecked( m_task->delayUpdates() );
    d->modePage->deleteExtraneous->setChecked( m_task->removeExtraneousFiles() );
    d->modePage->deleteMode->setCurrentIndex( ( int ) m_task->deleteMode() );
    d->modePage->deleteExcluded->setChecked( m_task->deleteExcluded() );
    d->modePage->deleteIgnoreErrors->setChecked( m_task->deleteIgnoreErrors() );
    d->modePage->forceDelete->setChecked( m_task->forceDeleteDirectories() );
    d->modePage->maxDelete->setValue( m_task->maxDelete() );
    d->modePage->backup->setChecked( m_task->backup() );
    d->modePage->backupDirectory->setText( m_task->backupDir() );
    d->modePage->backupSuffix->setText( m_task->backupSuffix() );
    d->modePage->copySymLinksAsIs->setChecked( m_task->copySymLinksAsIs() );
    d->modePage->resolveSymLinks->setChecked( m_task->resolveSymLinks() );
    d->modePage->resolveUnsafeSymLinks->setChecked( m_task->resolveUnsafeLinks() );
    d->modePage->ignoreOuterSymLinks->setChecked( m_task->ignoreOuterSymLinks() );
    d->modePage->resolveDirSymLinks->setChecked( m_task->resolveDirSymLinks() );
    d->modePage->treatDirSymLinksAsDir->setChecked( m_task->treatDirSymLinkAsDir() );
    d->modePage->preserveHardlinks->setChecked( m_task->preserveHardLinks() );
    d->modePage->preservePermissions->setChecked( m_task->preservePermissions() );
    d->modePage->preserveExecutability->setChecked( m_task->preserveExecutability() );
    d->modePage->preserveACLs->setChecked( m_task->preserveACLs() );
    d->modePage->preserveExtendedAttributes->setChecked( m_task->preserveExtendedAttributes() );
    d->modePage->preserveOwner->setChecked( m_task->preserveOwner() );
    d->modePage->preserveGroup->setChecked( m_task->preserveGroup() );
    d->modePage->preserveDevices->setChecked( m_task->preserveDevices() );
    d->modePage->preserveSpecialFiles->setChecked( m_task->preserveSpecialFiles() );
    d->modePage->preserveModificationTimes->setChecked( m_task->preserveModificationTimes() );
    d->modePage->dontPreserveDirectoryModificationTimes->setChecked( m_task->dontPreserveDirectoryModTimes() );
    d->modePage->numericIDs->setChecked( m_task->numericIds() );
    d->modePage->autoIgnoreCVSStyle->setChecked( m_task->autoIgnoreCVSStyle() );
    d->modePage->exludePattern->setText( m_task->excludePattern().join( "\n" ) );
    d->modePage->excludePatternFile->setText( m_task->excludePatternFile() );
    d->modePage->includePattern->setText( m_task->includePattern().join( "\n" ) );
    d->modePage->includePatternFile->setText( m_task->includePatternFile() );
    d->modePage->temporaryDirectory->setText( m_task->tempDirectory() );
    d->modePage->logFile->setText( m_task->logFile() );
    d->modePage->sourceListFile->setText( m_task->sourceListFile() );

    d->transferPage->copyWholeFiles->setChecked( m_task->copyWholeFiles() );
    d->transferPage->blockSize->setValue( m_task->blockSize() );
    d->transferPage->maxTransferSize->setValue( m_task->maxTransferSize() );
    d->transferPage->minTransferSize->setValue( m_task->minTransferSize() );
    d->transferPage->keepPartialFiles->setChecked( m_task->keepPartiallyTransferredFiles() );
    d->transferPage->partialDir->setText( m_task->partialDir() );
    d->transferPage->ipProtocol->setCurrentIndex( ( int ) m_task->ipProtocol() );
    d->transferPage->compress->setChecked( m_task->compress() );
    d->transferPage->compressLevel->setValue( m_task->compressLevel() );
    d->transferPage->excludeCompressSuffixes->setText( m_task->skipCompressSuffixes().split( "/" ).join( "\n" ) );
    d->transferPage->limitBandwidth->setValue( m_task->bandwidthLimitInKBPS() );
    d->transferPage->timeout->setValue( m_task->timeout() );
    d->transferPage->daemonConnectionTimeout->setValue( m_task->daemonConnectionTimeout() );
}

void
TaskConfigDialog::setAccountEnabledStates()
{
    d->accountPage->protocolWidget->setEnabled( transferMode() != RsyncTask::LocalOnly );
    d->accountPage->host->setEnabled( transferMode() != RsyncTask::LocalOnly );
    d->accountPage->port->setEnabled( transferMode() != RsyncTask::LocalOnly );

    d->accountPage->user->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "rsync" );
    d->accountPage->pass->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "rsync" );
    d->accountPage->storePassword->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "rsync" );
    d->accountPage->module->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "rsync" );

    d->accountPage->sshAskPass->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "ssh" );
    d->accountPage->sshUser->setEnabled( transferMode() != RsyncTask::LocalOnly && protocol() == "ssh" );
}

void
TaskConfigDialog::transferTypeChanged( int )
{
    setAccountEnabledStates();
}

void
TaskConfigDialog::sshSelected( bool )
{
    d->accountPage->port->setValue( 22 );
    setAccountEnabledStates();
}

void
TaskConfigDialog::rsyncSelected( bool )
{
    d->accountPage->port->setValue( 873 );
    setAccountEnabledStates();
}



}

}